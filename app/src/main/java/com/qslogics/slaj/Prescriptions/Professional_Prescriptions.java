package com.qslogics.slaj.Prescriptions;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Prescriptions.Medicine.Prescription;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class Professional_Prescriptions extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Prescription> list=new ArrayList<Prescription>();
    private DatabaseReference mDatabase;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //this is for db reference
        SharedPreferences settings = getActivity().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        String key = settings.getString("key", "key");
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Prescription");
        Query query = mDatabase.orderByChild("doctorKey").equalTo(key);

        //this is for rootview which will later used to return the view context
        final View rootView = inflater.inflate(R.layout.fragment_patient_prescriptions,container,false);

        //this is to get search value from the search
        final SearchView searchValue = (SearchView)rootView.findViewById(R.id.search_prescription);

        //db listener to track data changes
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                //to get database data and store it as a snapshot
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //loop through the snapshot data and store them in arraylist one after another
                    Prescription prescription_Entity = postSnapshot.getValue(Prescription.class);
                    list.add(prescription_Entity);
                }

                //get recyclerview by id
                recyclerView = rootView.findViewById(R.id.prescription_recyclerView);

                //declare adapter and connect the adapter to the recycle view while passing the data to the adapter
                final Prescriptions_Adapter prescriptions_Adapter = new Prescriptions_Adapter(getContext(),list);
                recyclerView.setAdapter(prescriptions_Adapter);
                recyclerView.setFocusable(false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                //pass the search text value to the adapter in order to know which data the user is searching for (default value is "" in order to show all the data in recycle view at first)
                prescriptions_Adapter.filter("");

                //onchange search textfield method
                searchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        String text = searchValue.getQuery().toString().toLowerCase(Locale.getDefault());
                        prescriptions_Adapter.filter(text);
                        return false;
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }
}
