package com.qslogics.slaj.Prescriptions.Medicine;

import android.os.Parcelable;

import java.io.Serializable;

public class Medicine implements Serializable {
    private String name;
    private String unit;
    private String doseamount;
    private String dosetype;
    private int days;
    private String medicineDetail;
    private String morningDose;
    private String afternoonDose;
    private String dinnerDose;
    private String bedtimeDose;
    private String othersDose;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDoseamount() {
        return doseamount;
    }

    public void setDoseamount(String doseamount) {
        this.doseamount = doseamount;
    }

    public String getDosetype() {
        return dosetype;
    }

    public void setDosetype(String dosetype) {
        this.dosetype = dosetype;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getMedicineDetail() {
        return medicineDetail;
    }

    public void setMedicineDetail(String medicineDetail) {
        this.medicineDetail = medicineDetail;
    }

    public String getMorningDose() {
        return morningDose;
    }

    public void setMorningDose(String morningDose) {
        this.morningDose = morningDose;
    }

    public String getAfternoonDose() {
        return afternoonDose;
    }

    public void setAfternoonDose(String afternoonDose) {
        this.afternoonDose = afternoonDose;
    }

    public String getDinnerDose() {
        return dinnerDose;
    }

    public void setDinnerDose(String dinnerDose) {
        this.dinnerDose = dinnerDose;
    }

    public String getBedtimeDose() {
        return bedtimeDose;
    }

    public void setBedtimeDose(String bedtimeDose) {
        this.bedtimeDose = bedtimeDose;
    }

    public String getOthersDose() {
        return othersDose;
    }

    public void setOthersDose(String othersDose) {
        this.othersDose = othersDose;
    }
}
