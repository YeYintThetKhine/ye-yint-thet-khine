package com.qslogics.slaj.Prescriptions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qslogics.slaj.Entity.Medicals_Entity;
import com.qslogics.slaj.Prescriptions.Medicine.Prescription;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class Prescriptions_Adapter extends RecyclerView.Adapter<Prescriptions_Adapter.PrescriptionsValue> {
    //declare array lists
    ArrayList<Prescription> before_filtered_list=new ArrayList<Prescription>();
    ArrayList<Prescription> mainlist=new ArrayList<Prescription>();
    Context context;

    public Prescriptions_Adapter(Context ct, ArrayList<Prescription> list){
        //first initialize the value to their respective variables
        context = ct;
        before_filtered_list = list;
    }
    public Prescriptions_Adapter() {

    }

    @NonNull
    @Override
    public PrescriptionsValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //initialize layout
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_prescriptions__adapter,parent,false);
        return new PrescriptionsValue(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PrescriptionsValue holder, final int position) {
        //bind the data with the texts and views

        holder.date.setText(mainlist.get(position).getDate());
        holder.doctor_name.setText(mainlist.get(position).getDoctorName());
        holder.patient_name.setText(mainlist.get(position).getPatientName());
        holder.time.setText(mainlist.get(position).getTime());

        holder.card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Prescription_Detail.class);
                intent.putExtra("child_key",mainlist.get(position).getKey());
//                    intent.putExtra("experience",mainlist.get(position).getExperience());
//                    intent.putExtra("email",mainlist.get(position).getEmail());
//                    intent.putExtra("pro_phone_numbers",(Serializable)mainlist.get(position).getPro_phone_numbers());
//                    intent.putExtra("available_time",(Serializable)mainlist.get(position).getAvailable_time());
//                    intent.putExtra("qualifications",mainlist.get(position).getQualifications());
//                    intent.putExtra("specializations",(Serializable)mainlist.get(position).getSpecializations());
                context.startActivity(intent);
            }
        });

    }

    public void filter(String searchText) {
        //filter search data method
        searchText = searchText.toLowerCase(Locale.getDefault());
        mainlist.clear();
        Log.d("result", String.valueOf(searchText.length()));
        if (searchText.length() == 0) {
            mainlist.addAll(before_filtered_list);
        }
        else
        {
            for (Prescription p : before_filtered_list) {
                if (p.getDoctorName().toLowerCase(Locale.getDefault()).contains(searchText)) {
                    mainlist.add(p);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class PrescriptionsValue extends RecyclerView.ViewHolder {

        //declare respective values to match with class value
        TextView date;
        TextView doctor_name;
        TextView patient_name;
        TextView prescription;
        TextView time;
        RelativeLayout card;

        public PrescriptionsValue(@NonNull View itemView) {
            //initialize view id
            super(itemView);
            date = itemView.findViewById(R.id.created_date);
            doctor_name = itemView.findViewById(R.id.doctor_name);
            patient_name = itemView.findViewById(R.id.patient_name);
            time = itemView.findViewById(R.id.created_time);
            card= itemView.findViewById(R.id.prescription_card_layout);

        }
    }

}
