package com.qslogics.slaj.Prescriptions.Medicine;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Prescriptions.EditMedicineActivity;
import com.qslogics.slaj.R;

import java.util.ArrayList;

public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.MedicineValue> {

    Context ct;
    ArrayList<Medicine> list = new ArrayList<>();

    public MedicineAdapter(Context ct, ArrayList<Medicine> medicineList) {
        this.ct = ct;
        list = medicineList;
    }

    @NonNull
    @Override
    public MedicineAdapter.MedicineValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ct);
        View view = inflater.inflate(R.layout.ps_medicine_row, parent, false);
        return new MedicineValue(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MedicineAdapter.MedicineValue holder, final int position) {

        if(list.get(position).getName() != null) {
            holder.medicineName.setText(list.get(position).getName());
        }

        holder.medicineNo.setText(position + 1 + " .");

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Medicine medicine = list.get(position);
                Intent intent = new Intent(v.getContext(), EditMedicineActivity.class);
                intent.putExtra("medicine", medicine);
                intent.putExtra("position", position);
                ct.startActivity(intent);

            }
        });

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MedicineValue extends RecyclerView.ViewHolder {

        TextView medicineNo;
        TextView medicineName;
        Button editButton;
        Button removeButton;

        public MedicineValue(View itemView) {
            super(itemView);
            medicineNo = itemView.findViewById(R.id.medicine_no);
            medicineName = itemView.findViewById(R.id.medicine_name);
            editButton = itemView.findViewById(R.id.edit_btn);
            removeButton = itemView.findViewById(R.id.remove_btn);
        }
    }
}
