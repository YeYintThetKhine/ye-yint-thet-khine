package com.qslogics.slaj.Prescriptions;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qslogics.slaj.Entity.Prescription_Detail_Entity;
import com.qslogics.slaj.Prescriptions.Medicine.Medicine;
import com.qslogics.slaj.Prescriptions.Medicine.Prescription;
import com.qslogics.slaj.R;

import java.util.ArrayList;

public class Prescription_Detail_Adapter extends RecyclerView.Adapter<Prescription_Detail_Adapter.Prescription_Detail_Value> {
    ArrayList<Medicine> mainlist=new ArrayList<Medicine>();
    Context context;

    public Prescription_Detail_Adapter(Context ct, ArrayList<Medicine> list){
        context = ct;
        mainlist = list;
        if(mainlist.size() == 0){
            Medicine med = new Medicine();
            med.setName("no_med");
            mainlist.add(med);
        }
    }

    @NonNull
    @Override
    public Prescription_Detail_Value onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_prescription__detail__adapter,parent,false);
        return new Prescription_Detail_Value(view);
    }

    public void onBindViewHolder(@NonNull Prescription_Detail_Value holder, final int position) {
        if(mainlist.get(0).getName().equals("no_med")){
            holder.no_med.setText(R.string.no_medicine_value);
            holder.have_med_data_layout.setVisibility(View.GONE);
        }
        else{
            holder.no_med.setVisibility(View.GONE);
            holder.no_med_data_layout.setVisibility(View.VISIBLE);
            holder.medicine_name_text.setText(mainlist.get(position).getName());
            holder.unit_text.setText(mainlist.get(position).getUnit());
            //holder.dose_amount_value.setText(String.valueOf(mainlist.get(position).getDays()));
            holder.morning_time.setText(mainlist.get(position).getMorningDose());
            holder.afternoon_time.setText(mainlist.get(position).getAfternoonDose());
            holder.bedtime_time.setText(mainlist.get(position).getBedtimeDose());
            holder.days_text.setText(String.valueOf(mainlist.get(position).getDays()));
            holder.remark_box_message.setText(mainlist.get(position).getMedicineDetail());
        }
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class Prescription_Detail_Value extends RecyclerView.ViewHolder {

        ConstraintLayout no_med_data_layout;
        ConstraintLayout have_med_data_layout;
        TextView no_med;
        TextView medicine_name_text;
        TextView unit_text;
        //TextView dose_amount_value;
        TextView morning_time;
        TextView afternoon_time;
        TextView bedtime_time;
        TextView days_text;
        TextView remark_box_message;

        public Prescription_Detail_Value(@NonNull View itemView) {
            super(itemView);
            no_med_data_layout = itemView.findViewById(R.id.no_med_data_layout);
            have_med_data_layout = itemView.findViewById(R.id.have_med_data_layout);
            no_med = itemView.findViewById(R.id.no_med);
            medicine_name_text= itemView.findViewById(R.id.medicine_name_text);
            unit_text = itemView.findViewById(R.id.unit_text);
            //dose_amount_value = itemView.findViewById(R.id.dose_amount_value);
            morning_time = itemView.findViewById(R.id.morning_time);
            afternoon_time = itemView.findViewById(R.id.afternoon_time);
            bedtime_time = itemView.findViewById(R.id.bedtime_time);
            days_text = itemView.findViewById(R.id.days_text);
            remark_box_message = itemView.findViewById(R.id.remark_box_message);
        }
    }

}
