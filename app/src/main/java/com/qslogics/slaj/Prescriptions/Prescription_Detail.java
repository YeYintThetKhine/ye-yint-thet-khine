package com.qslogics.slaj.Prescriptions;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Prescription_Detail_Entity;
import com.qslogics.slaj.Prescriptions.Medicine.Medicine;
import com.qslogics.slaj.Prescriptions.Medicine.Prescription;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Prescription_Detail extends AppCompatActivity {

    private DatabaseReference mDatabase;
    String child_key;
    RecyclerView recyclerView;
    ArrayList<Medicine> list=new ArrayList<Medicine>();
    TextView name;
    TextView date;
    TextView time;
    TextView patientName;
    ImageView imgBackArrow;
    TextView doctorNote;
    TextView doc_adv;
    TextView next_appointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription__detail);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onStart() {
        super.onStart();
        if(getIntent().hasExtra("child_key")){
            child_key = getIntent().getStringExtra("child_key");
            mDatabase = FirebaseDatabase.getInstance().getReference().child("Prescription");
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){

                        name = findViewById(R.id.doctor_p_name);
                        date = findViewById(R.id.date_text);
                        time = findViewById(R.id.time_text);
                        patientName = findViewById(R.id.patient_name);
                        doctorNote = findViewById(R.id.doctor_note);
                        doc_adv = findViewById(R.id.doc_adv);
                        next_appointment = findViewById(R.id.next_appointment);

                        name.setText(dataSnapshot.child(child_key).child("doctorName").getValue().toString());
                        //phone1.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("data").getValue().toString());
//                        if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("data2").exists()){
//                            phone2.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("data2").getValue().toString());
//                        }
//                        else{
//                            phone2.setText("");
//                        }
//                        if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("data3").exists()){
//                            phone3.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("data3").getValue().toString());
//                        }
//                        else{
//                            phone3.setText("");
//                        }
                        date.setText(dataSnapshot.child(child_key).child("date").getValue().toString());
                        time.setText(dataSnapshot.child(child_key).child("time").getValue().toString());
                        patientName.setText(dataSnapshot.child(child_key).child("patientName").getValue().toString());

                        if(dataSnapshot.child(child_key).child("prescription").exists()){
                            if(dataSnapshot.child(child_key).child("prescription").getValue().toString().equals("")){
                                doctorNote.setText(R.string.doctor_prescription_value);
                            }
                            else{
                                doctorNote.setText(dataSnapshot.child(child_key).child("prescription").getValue().toString());
                            }
                        }
                        else{
                            doctorNote.setText(R.string.doctor_prescription_value);
                        }
                        if(dataSnapshot.child(child_key).child("advice").exists()){
                            if(dataSnapshot.child(child_key).child("advice").getValue().toString().equals("")){
                                doc_adv.setText(R.string.doctor_advice_value);
                            }
                            else{
                                doc_adv.setText(dataSnapshot.child(child_key).child("advice").getValue().toString());
                            }
                        }
                        else{
                            doc_adv.setText(R.string.doctor_advice_value);
                        }
                        if(dataSnapshot.child(child_key).child("next_appointment_time").exists()){
                            if(dataSnapshot.child(child_key).child("next_appointment_time").getValue().toString().equals("")){
                                next_appointment.setText(R.string.doctor_next_appointment_value);
                            }
                            else{
                                next_appointment.setText(dataSnapshot.child(child_key).child("next_appointment_time").getValue().toString());
                            }
                        }
                        else{
                            next_appointment.setText(R.string.doctor_next_appointment_value);
                        }

                        list.clear();
                        for (DataSnapshot postSnapshot : dataSnapshot.child(child_key).child("medicines").getChildren()) {
                            Medicine medicine = postSnapshot.getValue(Medicine.class);
                            list.add(medicine); 
                        }

                        recyclerView = findViewById(R.id.prescriptions_recycler);
                        final Prescription_Detail_Adapter prescription_Detail_Adapter = new Prescription_Detail_Adapter(Prescription_Detail.this,list);
                        recyclerView.setAdapter(prescription_Detail_Adapter);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(Prescription_Detail.this, LinearLayoutManager.HORIZONTAL,false));
                        recyclerView.setFocusable(false);

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
