package com.qslogics.slaj.Prescriptions;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.qslogics.slaj.Prescriptions.Medicine.Medicine;
import com.qslogics.slaj.R;
import com.qslogics.slaj.VideoCall.VideoActivity;

public class EditMedicineActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private EditText medicineName, doseamount, no_of_days, doctor_note;
    private RadioButton morning_bm, morning_am, afternoon_bm, afternoon_am, dinner_bm, dinner_am, bedtime, no_bedtime, other_radiobtn;
    private RadioGroup morning_group, afternoon_group, dinner_group ,bedtime_group, other_group;
    private Button save_edit_medicine;
    private Button cancel;
    int position;
    private Spinner unitSpinner, doseSpinner;
    String[] unitItems = { "Capsule", "Tablet", "Syrup", "Drop", "Inhaler", "Suppository", "Injection", "Topical", "Patch" };
    String selectedUnit;
    String[] doseItems = {"mg","gm","ml","tbsp"};
    String selectedDose;
    boolean morning_bm_check,morning_am_check,afternoon_bm_check,afternoon_am_check,dinner_bm_check,dinner_am_check,bedtime_check,nobedtime_check,other_check;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ps_take_medicine_page);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        System.out.println(extras);

        doctor_note = findViewById(R.id.note);
        save_edit_medicine = findViewById(R.id.save_edit_btn);

        medicineName = findViewById(R.id.medicine_name);
        no_of_days = findViewById(R.id.taken_day);
        morning_bm = findViewById(R.id.radio1);
        morning_am = findViewById(R.id.radio2);
        morning_group = findViewById(R.id.morning_radio);
        afternoon_group = findViewById(R.id.afternoon_radio);
        afternoon_bm = findViewById( R.id.radio11);
        afternoon_am = findViewById(R.id.radio22);
        bedtime = findViewById(R.id.bt_radio);
        no_bedtime = findViewById(R.id.bt_radio1);
        dinner_group = findViewById(R.id.dinner_radio);
        dinner_bm = findViewById(R.id.bt_radio2);
        dinner_am = findViewById(R.id.bt_radio3);
        bedtime_group = findViewById(R.id.bedtime_radio);
        other_radiobtn = findViewById(R.id.bt_radio4);
        other_group = findViewById(R.id.other_radio);
        cancel = findViewById(R.id.cancel_button);
        doseamount = findViewById(R.id.etDose);

        morning_bm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                morning_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        morning_bm_check = true;
                    }
                });
                if (morning_bm.isChecked() && morning_bm_check == false) {
                    morning_bm_check = true;
                    morning_group.clearCheck();
                }
                if(morning_bm.isChecked() && morning_bm_check == true){
                    morning_bm_check = false;
                    morning_bm.setChecked(true);
                }
            }
        });

        morning_am.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                morning_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        morning_am_check = true;
                    }
                });
                if (morning_am.isChecked() && morning_am_check == false) {
                    morning_am_check = true;
                    morning_group.clearCheck();
                    System.out.println(morning_am_check);
                }
                if(morning_am.isChecked() && morning_am_check == true){
                    morning_am_check = false;
                    morning_am.setChecked(true);
                    System.out.println(morning_am_check);
                }
            }
        });

        afternoon_bm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                afternoon_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        afternoon_bm_check = true;
                    }
                });
                if (afternoon_bm.isChecked() && afternoon_bm_check == false) {
                    afternoon_bm_check = true;
                    afternoon_group.clearCheck();
                    System.out.println(afternoon_bm_check);
                }
                if(afternoon_bm.isChecked() && afternoon_bm_check == true){
                    afternoon_bm_check = false;
                    afternoon_bm.setChecked(true);
                    System.out.println(afternoon_bm_check);
                }
            }
        });

        afternoon_am.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                afternoon_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        afternoon_am_check = true;
                    }
                });
                if (afternoon_am.isChecked() && afternoon_am_check == false) {
                    afternoon_am_check = true;
                    afternoon_group.clearCheck();
                    System.out.println(afternoon_am_check);
                }
                if(afternoon_am.isChecked() && afternoon_am_check == true){
                    afternoon_am_check = false;
                    afternoon_am.setChecked(true);
                    System.out.println(afternoon_am_check);
                }
            }
        });


        dinner_bm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dinner_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        dinner_bm_check = true;
                    }
                });
                if (dinner_bm.isChecked() && dinner_bm_check == false) {
                    dinner_bm_check = true;
                    dinner_group.clearCheck();
                    System.out.println(dinner_bm_check);
                }
                if(dinner_bm.isChecked() && dinner_bm_check == true){
                    dinner_bm_check = false;
                    dinner_bm.setChecked(true);
                    System.out.println(dinner_bm_check);
                }
            }
        });

        dinner_am.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dinner_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        dinner_am_check = true;
                    }
                });
                if (dinner_am.isChecked() && dinner_am_check == false) {
                    dinner_am_check = true;
                    dinner_group.clearCheck();
                    System.out.println(dinner_am_check);
                }
                if(dinner_am.isChecked() && dinner_am_check == true){
                    dinner_am_check = false;
                    dinner_am.setChecked(true);
                    System.out.println(dinner_am_check);
                }
            }
        });

        bedtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bedtime_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        bedtime_check = true;
                    }
                });
                if (bedtime.isChecked() && bedtime_check == false) {
                    bedtime_check = true;
                    bedtime_group.clearCheck();
                }
                if(bedtime.isChecked() && bedtime_check == true){
                    bedtime_check = false;
                    bedtime.setChecked(true);
                }
            }
        });

        no_bedtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bedtime_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        nobedtime_check = true;
                    }
                });
                if (no_bedtime.isChecked() && nobedtime_check == false) {
                    nobedtime_check = true;
                    bedtime_group.clearCheck();
                }
                if(no_bedtime.isChecked() && nobedtime_check == true){
                    nobedtime_check = false;
                    no_bedtime.setChecked(true);
                }
            }
        });

        other_radiobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                other_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        other_check = true;
                    }
                });
                if (other_radiobtn.isChecked() && other_check == false) {
                    other_check = true;
                    other_group.clearCheck();
                }
                if(other_radiobtn.isChecked() && other_check == true){
                    other_check = false;
                    other_radiobtn.setChecked(true);
                }
            }
        });

        if(extras != null) {
            Medicine medicine = (Medicine) extras.getSerializable("medicine");
            position = extras.getInt("position");

            //yyp
            unitSpinner = findViewById(R.id.unitSpinner);
            unitSpinner.setOnItemSelectedListener(this);
            ArrayAdapter adapter = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, unitItems);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            unitSpinner.setAdapter(adapter);
            if(medicine.getUnit().equals("Capsule")){
                unitSpinner.setSelection(0);
            }
            else if(medicine.getUnit().equals("Tablet")){
                unitSpinner.setSelection(1);
            }
            else if(medicine.getUnit().equals("Syrup")){
                unitSpinner.setSelection(2);
            }
            else if(medicine.getUnit().equals("Drop")){
                unitSpinner.setSelection(3);
            }
            else if(medicine.getUnit().equals("Inhaler")){
                unitSpinner.setSelection(4);
            }
            else if(medicine.getUnit().equals("Suppository")){
                unitSpinner.setSelection(5);
            }
            else if(medicine.getUnit().equals("Injection")){
                unitSpinner.setSelection(6);
            }
            else if(medicine.getUnit().equals("Topical")){
                unitSpinner.setSelection(7);
            }
            else if(medicine.getUnit().equals("Patch")){
                unitSpinner.setSelection(8);
            }
            unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedUnit = parent.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            doseSpinner = findViewById(R.id.doseSpinner);

            doseSpinner.setOnItemSelectedListener(this);
            ArrayAdapter adapter1 = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, doseItems);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            doseSpinner.setAdapter(adapter1);
            Log.d("amount",medicine.getDosetype());
            if(medicine.getDosetype().equals("mg")){
                doseSpinner.setSelection(0);
            }
            else if(medicine.getDosetype().equals("gm")){
                doseSpinner.setSelection(1);
            }
            else if(medicine.getDosetype().equals("ml")){
                doseSpinner.setSelection(2);
            }
            else if(medicine.getDosetype().equals("tbsp")){
                doseSpinner.setSelection(3);
            }
            doseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDose = parent.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            doctor_note.setText(medicine.getMedicineDetail());
//            unit.setText(String.valueOf(medicine.getUnit()));
            doseamount.setText(medicine.getDoseamount());
            medicineName.setText(medicine.getName());
            no_of_days.setText(String.valueOf(medicine.getDays()));

            if(medicine.getMorningDose().equals("Before Meal")) {
                morning_bm.setChecked(true);
                morning_bm_check = false;
            }
            else{
                morning_bm_check = true;
            }
            if(medicine.getMorningDose().equals("After Meal")) {
                morning_am.setChecked(true);
                morning_am_check = false;
            }
            else{
                morning_am_check = true;
            }
            if(medicine.getAfternoonDose().equals("Before Meal")) {
                afternoon_bm.setChecked(true);
                afternoon_bm_check = false;
            }
            else{
                afternoon_bm_check = true;
            }
            if(medicine.getAfternoonDose().equals("After Meal")) {
                afternoon_am.setChecked(true);
                afternoon_am_check = false;
            }
            else{
                afternoon_am_check = true;
            }
            if(medicine.getDinnerDose().equals("Before Meal")) {
                dinner_bm.setChecked(true);
                dinner_bm_check = false;
            }
            else{
                dinner_bm_check = true;
            }
            if(medicine.getDinnerDose().equals("After Meal")) {
                dinner_am.setChecked(true);
                dinner_am_check = false;
            }
            else{
                dinner_am_check = true;
            }
            if(medicine.getBedtimeDose().equals("Before Meal")) {
                bedtime.setChecked(true);
                bedtime_check = false;
            }
            else{
                bedtime_check = true;
            }
            if(medicine.getBedtimeDose().equals("After Meal")) {
                no_bedtime.setChecked(true);
                nobedtime_check = false;
            }
            else{
                nobedtime_check = true;
            }
            if(medicine.getOthersDose().equals("Need")) {
                other_radiobtn.setChecked(true);
                other_check = false;
            }
            else{
                other_check = true;
            }
        }

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        save_edit_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(medicineName.getText().toString())) {
                    medicineName.setError("Medicine Name field can not be empty");
                    return;
                }

                if(TextUtils.isEmpty(doseamount.getText().toString())) {
                    doseamount.setError("Dose amount can not be empty");
                    return;
                }
                if(TextUtils.isEmpty(no_of_days.getText().toString())) {
                    no_of_days.setError("Number of days field can not be empty");
                    return;
                }
                if(morning_group.getCheckedRadioButtonId() == -1 && afternoon_group.getCheckedRadioButtonId() == -1 && dinner_group.getCheckedRadioButtonId() == -1
                && bedtime_group.getCheckedRadioButtonId() == -1 && other_group.getCheckedRadioButtonId() == -1){
                    Toast.makeText(EditMedicineActivity.this, "Please choose at least one of the times", Toast.LENGTH_LONG).show();
                }
                else{
                    String medicine_value = medicineName.getText().toString();
//                String unit_value = unit.getText().toString();
                    String taken_days_value = no_of_days.getText().toString();

                    Medicine editedMedicine = new Medicine();
                    editedMedicine.setName(medicineName.getText().toString().trim());
                    editedMedicine.setUnit(selectedUnit);
                    editedMedicine.setDoseamount(doseamount.getText().toString().trim());
                    editedMedicine.setDosetype(selectedDose);
                    editedMedicine.setDays(Integer.parseInt(no_of_days.getText().toString().trim()));
                    editedMedicine.setMedicineDetail(doctor_note.getText().toString().trim());
                    if(morning_group.getCheckedRadioButtonId() == -1){
                        editedMedicine.setMorningDose("-");
                    }
                    else{
                        int selectedMorningId = morning_group.getCheckedRadioButtonId();
                        morning_bm = findViewById(selectedMorningId);
                        String morning_value = morning_bm.getText().toString();
                        editedMedicine.setMorningDose(morning_value);
                    }
                    if(afternoon_group.getCheckedRadioButtonId() == -1){
                        editedMedicine.setAfternoonDose("-");
                    }
                    else{
                        int selectedAfternoonId = afternoon_group.getCheckedRadioButtonId();
                        afternoon_bm = findViewById(selectedAfternoonId);
                        String afternoon_value = afternoon_bm.getText().toString();
                        editedMedicine.setAfternoonDose(afternoon_value);
                    }
                    if(bedtime_group.getCheckedRadioButtonId() == -1){
                        editedMedicine.setBedtimeDose("-");
                    }
                    else{
                        int selectedBedtimeId = bedtime_group.getCheckedRadioButtonId();
                        bedtime = findViewById(selectedBedtimeId);
                        String bedtime_value = bedtime.getText().toString();
                        editedMedicine.setBedtimeDose(bedtime_value);
                    }
                    if(dinner_group.getCheckedRadioButtonId() == -1){
                        editedMedicine.setDinnerDose("-");
                    }
                    else{
                        int selectedDinnertimeId = dinner_group.getCheckedRadioButtonId();
                        dinner_bm = findViewById(selectedDinnertimeId);
                        String dinnertime_value = dinner_bm.getText().toString();
                        editedMedicine.setDinnerDose(dinnertime_value);
                    }
                    if(other_group.getCheckedRadioButtonId() == -1){
                        editedMedicine.setOthersDose("-");
                    }
                    else{
                        int selectedOthertimeId = other_group.getCheckedRadioButtonId();
                        other_radiobtn = findViewById(selectedOthertimeId);
                        String other_value = other_radiobtn.getText().toString();
                        editedMedicine.setOthersDose(other_value);
                    }

                    Intent intent = new Intent(EditMedicineActivity.this, VideoActivity.class);
                    intent.putExtra("editedMedicine", editedMedicine);
                    intent.putExtra("functionType", "edit");
                    intent.putExtra("position", position);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |  Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    EditMedicineActivity.this.finish();
                    startActivity(intent);
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String unit_value = unitItems[i];
        System.out.println(unit_value);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
