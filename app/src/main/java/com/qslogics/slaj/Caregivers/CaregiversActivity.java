package com.qslogics.slaj.Caregivers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class CaregiversActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private DatabaseReference mDatabase;
    ArrayList<Caregiver> list_caregiver=new ArrayList<Caregiver>();
    Query query;
    ImageView imgBackArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_caregivers);
        Intent i = getIntent();

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Caregivers");
        query = mDatabase.orderByChild("support_role").equalTo(i.getStringExtra("query"));

        final SearchView searchValue = findViewById(R.id.search_caregivers);
        searchValue.setFocusable(false);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_caregiver.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Caregiver caregiver_Entity = postSnapshot.getValue(Caregiver.class);
                    list_caregiver.add(caregiver_Entity);
                }
                recyclerView = findViewById(R.id._caregiversRecyclerView);

                final Caregivers_Adapter caregivers_Adapter = new Caregivers_Adapter(CaregiversActivity.this,list_caregiver);
                recyclerView.setAdapter(caregivers_Adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(CaregiversActivity.this));
                caregivers_Adapter.filter("");

                searchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        String text = searchValue.getQuery().toString().toLowerCase(Locale.getDefault());
                        caregivers_Adapter.filter(text);
                        return false;
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
