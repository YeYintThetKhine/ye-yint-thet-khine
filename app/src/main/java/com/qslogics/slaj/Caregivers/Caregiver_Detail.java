package com.qslogics.slaj.Caregivers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

public class Caregiver_Detail extends AppCompatActivity {

    private DatabaseReference mDatabase;
    String child_key;
    ImageView imgBackArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caregiver__detail);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onStart() {
        super.onStart();

        if(getIntent().hasExtra("name")){
            child_key = getIntent().getStringExtra("child_key");


            mDatabase = FirebaseDatabase.getInstance().getReference().child("Caregivers");

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        final ImageView caregiver_image_url;
                        TextView name;
                        TextView email;
                        TextView phone1;
                        TextView phone2;
                        TextView phone3;
                        TextView support_role;


                        caregiver_image_url = findViewById(R.id.caregiver_profile_image);
                        name = findViewById(R.id.caregiver_name);
                        email = findViewById(R.id.email_text);
                        phone1 = findViewById(R.id.phoneno_1);
                        phone2 = findViewById(R.id.phoneno_2);
                        phone3 = findViewById(R.id.phoneno_3);
                        support_role = findViewById(R.id.support_role_text);

                        name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                        Picasso.get().load(dataSnapshot.child(child_key).child("caregiver_image_url").getValue().toString()).into(caregiver_image_url);

                        email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());

                        phone1.setText(dataSnapshot.child(child_key).child("caregiver_phone_numbers").child("ph_1").getValue().toString());
                        if(dataSnapshot.child(child_key).child("caregiver_phone_numbers").child("ph_2").exists()){
                            phone2.setText(dataSnapshot.child(child_key).child("caregiver_phone_numbers").child("ph_2").getValue().toString());
                        }
                        else{
                            phone2.setText("");
                        }
                        if(dataSnapshot.child(child_key).child("caregiver_phone_numbers").child("ph_3").exists()){
                            phone3.setText(dataSnapshot.child(child_key).child("caregiver_phone_numbers").child("ph_3").getValue().toString());
                        }
                        else{
                            phone3.setText("");
                        }

                        support_role.setText(dataSnapshot.child(child_key).child("support_role").getValue().toString());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }

}
