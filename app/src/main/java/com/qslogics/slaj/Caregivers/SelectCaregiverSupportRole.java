package com.qslogics.slaj.Caregivers;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.qslogics.slaj.R;

public class SelectCaregiverSupportRole extends Fragment {

    Button supportRole1;
    Button supportRole2;
    Button supportRole3;
    Button supportRole4;
    Button supportRole5;
    Button supportRole6;
    Button supportRole7;
    Button supportRole8;
    Button supportRole9;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_select_caregiver_support_role, container, false);

        supportRole1 = rootView.findViewById(R.id.supportRole1);
        supportRole2 = rootView.findViewById(R.id.supportRole2);
        supportRole3 = rootView.findViewById(R.id.supportRole3);
        supportRole4 = rootView.findViewById(R.id.supportRole4);
        supportRole5 = rootView.findViewById(R.id.supportRole5);
        supportRole6 = rootView.findViewById(R.id.supportRole6);
        supportRole7 = rootView.findViewById(R.id.supportRole7);
        supportRole8 = rootView.findViewById(R.id.supportRole8);
        supportRole9 = rootView.findViewById(R.id.supportRole9);

        supportRole1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportOne));
                startActivity(intent);
            }
        });

        supportRole2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportTwo));
                startActivity(intent);
            }
        });

        supportRole3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportThree));
                startActivity(intent);
            }
        });

        supportRole4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportFour));
                startActivity(intent);
            }
        });

        supportRole5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportFive));
                startActivity(intent);
            }
        });

        supportRole6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportSix));
                startActivity(intent);
            }
        });

        supportRole7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportSeven));
                startActivity(intent);
            }
        });

        supportRole8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportEight));
                startActivity(intent);
            }
        });

        supportRole9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.supportNine));
                startActivity(intent);
            }
        });


        return rootView;
    }
}
