package com.qslogics.slaj.Caregivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class Caregivers_Adapter extends RecyclerView.Adapter<Caregivers_Adapter.Caregivers_Value> {

    String data1[], data2[];
    int images[];
    ArrayList<Caregiver> mainlist=new ArrayList<>();
    ArrayList<Caregiver> before_filtered_list=new ArrayList<>();
    Context context;

    public Caregivers_Adapter(Context ct, ArrayList<Caregiver> list){
        context = ct;
        before_filtered_list = list;
    }

    @Override
    public Caregivers_Adapter.Caregivers_Value onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_caregivers__adapter,parent,false);
        return new Caregivers_Adapter.Caregivers_Value(view);
    }

    public Caregivers_Adapter () {

    }

    public void filter(String searchText) {
        searchText = searchText.toLowerCase(Locale.getDefault());
        mainlist.clear();
        Log.d("result", String.valueOf(searchText.length()));
        if (searchText.length() == 0) {
            mainlist.addAll(before_filtered_list);
        }
        else
        {
            for (Caregiver OPE : before_filtered_list) {
                if (OPE.getName().toLowerCase(Locale.getDefault()).contains(searchText)) {
                    mainlist.add(OPE);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull Caregivers_Value holder, final int position) {
        holder.card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Caregiver_Detail.class);
                intent.putExtra("child_key",mainlist.get(position).getChild_key());
                intent.putExtra("caregiver_image_url",mainlist.get(position).getCaregiver_image_url());
                intent.putExtra("name",mainlist.get(position).getName());
//                    intent.putExtra("experience",mainlist.get(position).getExperience());
//                    intent.putExtra("email",mainlist.get(position).getEmail());
//                    intent.putExtra("pro_phone_numbers",(Serializable)mainlist.get(position).getPro_phone_numbers());
//                    intent.putExtra("available_time",(Serializable)mainlist.get(position).getAvailable_time());
//                    intent.putExtra("qualifications",mainlist.get(position).getQualifications());
//                    intent.putExtra("specializations",(Serializable)mainlist.get(position).getSpecializations());
                context.startActivity(intent);
            }
        });

        holder.viewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Caregiver_Detail.class);
                intent.putExtra("child_key",mainlist.get(position).getChild_key());
                intent.putExtra("caregiver_image_url",mainlist.get(position).getCaregiver_image_url());
                intent.putExtra("name",mainlist.get(position).getName());
                context.startActivity(intent);
            }
        });

        holder.fullname.setText(mainlist.get(position).getName());
        holder.phnumber.setText(mainlist.get(position).getCaregiver_phone_numbers().get("ph_1"));
        /*if(mainlist.get(position).getEmail().length() <= 4){
            holder.gmail.setText("Not provided");
        }
        else{
            holder.gmail.setText(mainlist.get(position).getEmail());
        }*/
        Picasso.get().load(mainlist.get(position).getCaregiver_image_url()).into(holder.img_url);
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class Caregivers_Value extends RecyclerView.ViewHolder {

        ImageView img_url;
        TextView fullname;
        TextView phnumber;
        TextView gmail;
        RelativeLayout card;
        Button viewDetail;

        public Caregivers_Value(@NonNull View itemView) {
            super(itemView);
            card= itemView.findViewById(R.id.caregiver_card_layout);
            fullname = itemView.findViewById(R.id._title);
            phnumber = itemView.findViewById(R.id._phone);
            //gmail = itemView.findViewById(R.id._email);
            img_url = itemView.findViewById(R.id._image);
            viewDetail = itemView.findViewById(R.id.btn_detail);
        }
    }
}
