package com.qslogics.slaj.Caregivers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class Caregivers extends Fragment {

    RecyclerView recyclerView;
    private DatabaseReference mDatabase;
    ArrayList<Caregiver> list_caregiver=new ArrayList<Caregiver>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Caregivers");
        final View rootView = inflater.inflate(R.layout.fragment_caregivers,container,false);
        final SearchView searchValue = rootView.findViewById(R.id.search_caregivers);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_caregiver.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Caregiver caregiver_Entity = postSnapshot.getValue(Caregiver.class);
                    list_caregiver.add(caregiver_Entity);
                }
                recyclerView = rootView.findViewById(R.id._caregiversRecyclerView);

                final Caregivers_Adapter caregivers_Adapter = new Caregivers_Adapter(getContext(),list_caregiver);
                recyclerView.setAdapter(caregivers_Adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                caregivers_Adapter.filter("");

                searchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        String text = searchValue.getQuery().toString().toLowerCase(Locale.getDefault());
                        caregivers_Adapter.filter(text);
                        return false;
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }

}
