package com.qslogics.slaj.Settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.qslogics.slaj.R;

import java.util.Locale;

public class Help extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate ( R.layout.fragment_help , container , false );
        TextView Ph_num_one = v.findViewById ( R.id.help );
        TextView Ph_num_two = v.findViewById ( R.id.help_two );

        Ph_num_one.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String phone="+959421179663";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+phone));
                startActivity(intent);
            }
        } );

        Ph_num_two.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String phone="+959960177706";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+phone));
                startActivity(intent);
            }
        } );


        return v;
    }
}