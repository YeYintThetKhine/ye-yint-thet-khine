package com.qslogics.slaj.Settings;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.qslogics.slaj.Home.MainActivity;
import com.qslogics.slaj.Home.Professional_Home;
import com.qslogics.slaj.Login.Login;
import com.qslogics.slaj.R;

import java.util.Locale;
import java.util.Set;

public class Settings extends Fragment {

    Locale myLocale;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        ///String [] values =
                /// {"Please Select Language","Myanmar","English",};
        Spinner spinner = (Spinner) v.findViewById(R.id.spinner1);
        /// ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        /// adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        /// spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener () {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                if (pos == 1) {

                   /* Toast.makeText(parent.getContext(),
                            "You have selected Myanmar", Toast.LENGTH_SHORT)
                            .show(); */
                    saveLocale("my");
                } else if (pos == 2) {

                    /* Toast.makeText(parent.getContext(),
                            "You have selected English", Toast.LENGTH_SHORT)
                            .show(); */
                    saveLocale("en");
                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });

        return v;

    }

    public void setLocale(String lang) {


        // Create a new Locale object
        myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        // Create a new configuration object
        Configuration config = new Configuration();
        // Set the locale of the new configuration
        config.locale = myLocale;
        // Update the configuration of the Accplication context
        getResources().updateConfiguration(
                config,
                getResources().getDisplayMetrics()
        );


    }
    public void saveLocale(String language) {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("USER_LANGUAGE", language);
        editor.apply();
        LoadLanguage ();
        FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
        t.setReorderingAllowed (false);
        t.detach(this).attach(this).commitAllowingStateLoss();
    }
    public void LoadLanguage(){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String language = sharedPreferences.getString("USER_LANGUAGE","");
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

}