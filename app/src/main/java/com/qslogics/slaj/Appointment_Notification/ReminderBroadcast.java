package com.qslogics.slaj.Appointment_Notification;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;
import com.qslogics.slaj.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReminderBroadcast extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {

//        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,"notifyLemubit")
//                .setSmallIcon(R.drawable.ic_launcher_background)
//                .setContentTitle("Remind me")
//                .setContentText("Hey hello")
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
//
//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
//
//        notificationManager.notify(200, builder.build());


        SharedPreferences settings = context.getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        String key = settings.getString("key", "key");

        //for volley http library
        RequestQueue mRequestQue;

        String URL = "https://fcm.googleapis.com/fcm/send";

        //for volley http library
        mRequestQue = Volley.newRequestQueue(context);

        //for push notification
        FirebaseMessaging.getInstance().subscribeToTopic(key);

        //json object
        JSONObject mainObj = new JSONObject();

        JSONObject mainObj_doc = new JSONObject();
        try {
            mainObj.put("to","/topics/"+key);
            JSONObject notificationObj = new JSONObject();
            notificationObj.put("title","Appointment Time");
            notificationObj.put("body","Its time for your appointment. Please join your video chat room and wait for the doctor arrival");

            JSONObject extraData = new JSONObject();
            extraData.put("brandId","puma");
            extraData.put("category","Shoes");

            mainObj.put("notification",notificationObj);
            mainObj.put("data",extraData);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, mainObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAbhYK_dE:APA91bEV8F2sHqIcZnW1xtdGjy5fpaVeqQQVctSDrzgl6SPaaUGnIUxBiDW2tJB1MryeJXy2Qb_4k6UUnNztLvptcAxVF3up_8lShS2KPWbV9ulgaBZpdLNc9Xc4UH7LRGHy0S4l5E8n");
                    System.out.println("passed here");
                    return header;
                }
            };
            mRequestQue.add(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
