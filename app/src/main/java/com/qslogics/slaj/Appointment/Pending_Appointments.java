package com.qslogics.slaj.Appointment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Article.Image;
import com.qslogics.slaj.Entity.Appointments_Entity;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Pending_Appointments extends Fragment {

    RecyclerView recyclerView;
    private DatabaseReference mDatabase;
    List<MedicalScreeningData> list_pending_appointments;
    List<String> completed_appointment_childkeys;
    String usertype;
    String Professional_child_key;
    Query query;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_pending__appointments, container, false);

        list_pending_appointments=new ArrayList<>();
        completed_appointment_childkeys=new ArrayList<>();
        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        usertype =settings.getString("type", "usertype");
        Professional_child_key =settings.getString("key", "child_key");
        Log.d("pro_key",Professional_child_key);
        if(usertype.equals("Professional")){

            mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");
            query = mDatabase.orderByChild("professionalKey").equalTo(Professional_child_key);

            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    list_pending_appointments.clear();
                    completed_appointment_childkeys.clear();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        MedicalScreeningData appointments_entity = postSnapshot.getValue(MedicalScreeningData.class);
                        if(postSnapshot.child("checked").getValue().equals(false)){
                            list_pending_appointments.add(appointments_entity);
                            completed_appointment_childkeys.add(postSnapshot.getKey());
                        }
                    }

                    recyclerView = rootView.findViewById(R.id.appointment_pending_recycleview);

                    final Continue_Appointments_Adapter continue_Appointments_Adapter = new Continue_Appointments_Adapter(getContext(),list_pending_appointments,completed_appointment_childkeys,"Professional");
                    recyclerView.setAdapter(continue_Appointments_Adapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

//                    recyclerView = rootView.findViewById(R.id.appointment_pending_recycleview);
//
//                    final Pending_Appointment_Adapter pending_Appointment_Adapter = new Pending_Appointment_Adapter(getContext(),list_pending_appointments);
//                    recyclerView.setAdapter(pending_Appointment_Adapter);
//                    recyclerView.setHasFixedSize(true);
//                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        if(usertype.equals("MO")){
            mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords");

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    list_pending_appointments.clear();
                    completed_appointment_childkeys.clear();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        MedicalScreeningData appointments_entity = postSnapshot.getValue(MedicalScreeningData.class);
                        list_pending_appointments.add(appointments_entity);
                        completed_appointment_childkeys.add(postSnapshot.getKey());
                    }

                    recyclerView = rootView.findViewById(R.id.appointment_pending_recycleview);

                    final Pending_Appointment_Adapter pending_Appointment_Adapter = new Pending_Appointment_Adapter(getContext(),list_pending_appointments,completed_appointment_childkeys);
                    recyclerView.setAdapter(pending_Appointment_Adapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        // Inflate the layout for this fragment
        return rootView;
    }
}
