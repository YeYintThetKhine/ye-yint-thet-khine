package com.qslogics.slaj.Appointment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.qslogics.slaj.MedicalScreening.ContactMo;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.List;

public class Patient_Appointments extends Fragment {

    FloatingActionButton floatingBtn;

    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_patient_appointments, container, false);

        TabItem tab1 = rootView.findViewById(R.id.tab1);
        TabItem tab2 = rootView.findViewById(R.id.tab2);
        TabLayout tabLayout = rootView.findViewById(R.id.tab_layout);

        final ViewPager viewPager = rootView.findViewById(R.id.view_pager);
        PatientAppointmentTabsAdapter tabsAdapter = new PatientAppointmentTabsAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        floatingBtn = rootView.findViewById(R.id.floating_button);
        floatingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(rootView.getContext(), ContactMo.class);
                i.putExtra("fromPatientAppointment", true);
                startActivity(i);
            }
        });



        return rootView;
    }
}
