package com.qslogics.slaj.Appointment;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ortiz.touchview.TouchImageView;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DisplayDoctorAppointmentsAdapter extends RecyclerView.Adapter<DisplayDoctorAppointmentsAdapter.displaydoctorValue> {
    ArrayList<String> mainlist =  new ArrayList<String>();
    Context context;

    public DisplayDoctorAppointmentsAdapter (Context ct, ArrayList<String> list) {
        context = ct;
        mainlist = list;
        if(mainlist.size() == 0){
            mainlist.add("No assigned appointment!");
        }
    }


    @NonNull
    @Override
    public DisplayDoctorAppointmentsAdapter.displaydoctorValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_displaydoctorappointment_adapter,parent,false);

        return new DisplayDoctorAppointmentsAdapter.displaydoctorValue(view);
    }

    public DisplayDoctorAppointmentsAdapter() {

    }

    @Override
    public void onBindViewHolder(@NonNull DisplayDoctorAppointmentsAdapter.displaydoctorValue holder, final int position) {
        Log.d("adapter_doc_appointment",String.valueOf(mainlist.size()));
        if(mainlist.get(0) == "No assigned appointment!"){
            holder.doc_appointment_time.setText("No assigned appointment!");
        }
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            try {
                Date picked_time_parse = sdf.parse(mainlist.get(position));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(picked_time_parse);
                calendar.add(Calendar.MINUTE, 16);
                String appointment_time_no_sec = sdf.format(picked_time_parse);
                String appointment_time_parse_aftertime = sdf.format(calendar.getTime());
                holder.doc_appointment_time.setText(appointment_time_no_sec + " - " + appointment_time_parse_aftertime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class displaydoctorValue extends RecyclerView.ViewHolder {


        TextView doc_appointment_time;


        public displaydoctorValue(@NonNull View itemView) {
            super(itemView);
            doc_appointment_time = itemView.findViewById(R.id.doc_appointment_time);
        }
    }
}
