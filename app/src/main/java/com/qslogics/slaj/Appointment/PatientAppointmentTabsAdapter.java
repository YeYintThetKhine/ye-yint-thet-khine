package com.qslogics.slaj.Appointment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PatientAppointmentTabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public PatientAppointmentTabsAdapter(FragmentManager fm, int NoofTabs){
        super(fm);
        this.mNumOfTabs = NoofTabs;

    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Patient_Pending_Appointments ppa = new Patient_Pending_Appointments();
                return ppa;
            case 1:
                Patient_Completed_Appointments pca = new Patient_Completed_Appointments();
                return pca;
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
