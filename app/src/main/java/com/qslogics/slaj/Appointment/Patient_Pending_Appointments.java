package com.qslogics.slaj.Appointment;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Appointment_Notification.ReminderBroadcast;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Patient_Pending_Appointments extends Fragment {

    RecyclerView recyclerView;
    DatabaseReference mDatabase;
    List<MedicalScreeningData> list_patient_pending_appointments;
    List<String> pendingAppointmentKeys;
    Query query1;
    String key;
    long final_final_time_diff;
    long current_time_parse_time;
    long appointment_time_parse_time;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_patient__pending__appointments, container, false);
        list_patient_pending_appointments = new ArrayList<>();
        pendingAppointmentKeys = new ArrayList<>();

        createNotificationChannel();
        Intent intent = new Intent(getActivity(),ReminderBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(),0,intent,0);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");

        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        key = settings.getString("key", "key");

        query1 = mDatabase.orderByChild("patientKey").equalTo(key);

        query1.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_patient_pending_appointments.clear();
                pendingAppointmentKeys.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    MedicalScreeningData medicalScreeningData = postSnapshot.getValue(MedicalScreeningData.class);
                    if(postSnapshot.child("checked").getValue().equals(false)){
                        list_patient_pending_appointments.add(medicalScreeningData);
                        pendingAppointmentKeys.add(postSnapshot.getKey());


                        try {
                            DateFormat df = new SimpleDateFormat("hh:mm:ss_yyyy.MM.dd");
                            DateFormat df_two = new SimpleDateFormat("MM/dd/yy");

                            String now = df.format(new Date());
                            Date today_date = new java.util.Date();
//                            Log.d("hi1",today_date.toString());
                            String today_date_string = df.format(today_date);
                            Date today_parse = df.parse(today_date_string);
                            Log.d("today_date_string",today_date_string);
                            Log.d("today_parse",today_parse.toString());
                            String appointment_date_string = postSnapshot.child("appointmentDate").getValue().toString();
                            System.out.println(appointment_date_string);
                            Date appointment_date_parse = df_two.parse(appointment_date_string);
                            String appointment_date_parse_string = df.format(appointment_date_parse);
                            if(now.equals(appointment_date_parse_string)){
                                Date appointment_date_parse_new = df.parse(appointment_date_parse_string);
                                long today_parse_time = today_parse.getTime();
                                long appointment_date_parse_new_time = appointment_date_parse_new.getTime();
                                long time_diff = appointment_date_parse_new_time - today_parse_time;
                                System.out.println(appointment_date_parse_new);
                                System.out.println(today_parse_time);
                                System.out.println(appointment_date_parse_new_time);
                                Log.d("appointment_date_parse",String.valueOf(time_diff));

                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                String current_time = sdf.format(new Date());
                                Date current_time_parse = sdf.parse(current_time);
                                String appointment_time = postSnapshot.child("appointmentTime").getValue().toString();
                                Date appointment_time_parse = sdf.parse(appointment_time);
                                current_time_parse_time = current_time_parse.getTime();
                                appointment_time_parse_time = appointment_time_parse.getTime();
                                long final_time_diff = appointment_time_parse_time - current_time_parse_time;

                                final_final_time_diff = time_diff + final_time_diff;
                            }
                            else{
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                String current_time = sdf.format(new Date());
                                Date current_time_parse = sdf.parse(current_time);
                                String appointment_time = postSnapshot.child("appointmentTime").getValue().toString();
                                Date appointment_time_parse = sdf.parse(appointment_time);
                                current_time_parse_time = current_time_parse.getTime();
                                appointment_time_parse_time = appointment_time_parse.getTime();
                                final_final_time_diff = appointment_time_parse_time - current_time_parse_time;
                                Log.d("final_time_diff",String.valueOf(final_final_time_diff));
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

//                        long diff = date2.getTime() - date1.getTime();
//
//                        System.out.println(diff);

                        if(appointment_time_parse_time > current_time_parse_time){
                            AlarmManager alarmManager =(AlarmManager) getActivity().getSystemService(getActivity().ALARM_SERVICE);

                            long timeAtButtonClick = System.currentTimeMillis();

                            long tenSecondsInMilis = 1000 * 10;

                            alarmManager.set(AlarmManager.RTC_WAKEUP,timeAtButtonClick + final_final_time_diff,pendingIntent);

                            Log.d("alarm_manager_time",String.valueOf(final_final_time_diff));
                            Toast.makeText(getContext(), "Reminder Set!" + String.valueOf(final_final_time_diff), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                recyclerView = rootView.findViewById(R.id.appointment_patient_pending_recycleview);

                final Patient_Pending_Appointment_Adapter patient_pending_appointment_adapter = new Patient_Pending_Appointment_Adapter(getContext(),list_patient_pending_appointments, pendingAppointmentKeys);
                recyclerView.setAdapter(patient_pending_appointment_adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "LemubitReminderChannel";
            String description = "Channel for Lemubit Reminder";
            int important = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("notifyLemubit",name,important);
            channel.setDescription(description);

            NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
