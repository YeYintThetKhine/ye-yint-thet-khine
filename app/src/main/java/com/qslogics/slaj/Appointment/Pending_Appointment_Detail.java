package com.qslogics.slaj.Appointment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.qslogics.slaj.Appointment_Notification.ReceiveNotificationActivity;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.R;
import com.qslogics.slaj.VideoCall.VideoActivity;
import com.twilio.video.Room;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pending_Appointment_Detail extends AppCompatActivity {


    String address;
    Boolean checked;
    String checkedDate;
    Boolean haveCough;
    Boolean haveTravelHistory;
    Boolean haveTroubleBreathing;
    String maritalStatus;
    String medicalNotes;
    String screeningDate;
    String temperature;
    String type_value;
    String appointment_date;
    String appointment_time;

    String patient_name;
    String patient_key;
    String completed_appointment_childkeys;
    String appointment_key;
    RecyclerView recyclerView;
    RecyclerView recyclerView_med_report;
    RecyclerView recyclerView_doc_appointment_time;
    ImageView imgBackArrow;
    TextView edit_id;
    TextView date_picker;
    TextView p_appointment_Time;
    Button med_report_uploadbtn;
    Button edit_btn_appointment;
    TextView expandable_btn;
    private Uri ImageUri;
    private static final int PICK_IMAGE = 1;
    private DatabaseReference mDatabase;
    private DatabaseReference pending_list_rm_Database;
    private DatabaseReference patient_hasappointment_true_db_ref;
    private Spinner spinner;
    ArrayList<MedicalRecordImage> list = new ArrayList<>();
    ArrayList<MedicalRecordImage> final_med_report_list = new ArrayList<>();
    ArrayList<Uri> ImageList = new ArrayList<>();
    ArrayList<MedicalRecordImage> record_list = new ArrayList<>();
    ArrayList<String> doc_name_list = new ArrayList<String>();
    ArrayList<String> doc_childkey_list = new ArrayList<String>();
    ArrayList<String> doc_appointment_time_list = new ArrayList<String>();
    boolean time_overlapped_check = false;

    StorageReference mStorageRef;
    private DatabaseReference mStorageRefDocAva;
    private DatabaseReference mStorageRefDoc_appoint_Time;

    Date c = Calendar.getInstance().getTime();
    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
    String date = df.format(c);

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private int upload_count = 0;
    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    public Uri imguri;
    Uri downloadUri;

    //for volley http library
    private RequestQueue mRequestQue;
    private String URL = "https://fcm.googleapis.com/fcm/send";

    //for expandable layout
    ExpandableLayout expandableLayout0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending__appointment__detail);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);


        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        expandableLayout0 = findViewById(R.id.expandable_layout);
//
//        expandableLayout0.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
//            @Override
//            public void onExpansionUpdate(float expansionFraction, int state) {
//                Log.d("ExpandableLayout0", "State: " + state);
//            }
//        });
//
//        expandable_btn = findViewById(R.id.expand_btn);
//
//        expandable_btn.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                if (expandableLayout0.isExpanded()) {
//                    expandableLayout0.collapse();
//                } else {
//                    expandableLayout0.expand();
//                }
//            }
//        });

        //Get intent data from Pending Appointment adapter
        patient_name = getIntent().getStringExtra("patientName");
        patient_key = getIntent().getStringExtra("patientKey");
        completed_appointment_childkeys = getIntent().getStringExtra("completed_appointment_childkeys");
        Bundle bundle = new Bundle();
        String medical_RecordsAsString = getIntent().getStringExtra("medical_Records");
        Gson gson = new Gson();
        Type type = new TypeToken<List<MedicalRecordImage>>(){}.getType();
        record_list = gson.fromJson(medical_RecordsAsString, type);

        address = getIntent().getStringExtra("address");
        checked = bundle.getBoolean("checked");
        checkedDate = getIntent().getStringExtra("checkedDate");
        haveCough = bundle.getBoolean("haveCough");
        haveTravelHistory = bundle.getBoolean("haveTravelHistory");
        haveTroubleBreathing = bundle.getBoolean("haveTroubleBreathing");
        maritalStatus = getIntent().getStringExtra("maritalStatus");
        medicalNotes = getIntent().getStringExtra("medicalNotes");
        screeningDate = getIntent().getStringExtra("screeningDate");
        temperature = getIntent().getStringExtra("temperature");
        type_value = getIntent().getStringExtra("type");

        /////////////////////////////////////////////////////

//        record_list = (ArrayList<MedicalRecordImage>)getIntent().getSerializableExtra("medical_Records");

        edit_id = findViewById(R.id.edit_id);
        date_picker = findViewById(R.id.tvDate);
        p_appointment_Time = findViewById(R.id.p_appointment_Time);
        edit_btn_appointment = findViewById(R.id.edit_btn_appointment);
        edit_id.setText(patient_key);
        med_report_uploadbtn = findViewById(R.id.med_report_uploadbtn);

        med_report_uploadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent itImage = new Intent(Intent.ACTION_GET_CONTENT);
                Intent itImage = new Intent(Intent.ACTION_GET_CONTENT,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                itImage.setType("image/*");
                itImage.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(itImage, PICK_IMAGE);
            }
        });

        //recycleView for medical record
        recyclerView = findViewById(R.id.mrecords);

        final MedicalRecord_Appointment_Adapter medicalRecord_appointment_Adapter = new MedicalRecord_Appointment_Adapter(Pending_Appointment_Detail.this,record_list);
        recyclerView.setAdapter(medicalRecord_appointment_Adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(Pending_Appointment_Detail.this,LinearLayoutManager.HORIZONTAL,false));
        ///////////////////////////////

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Professionals");
        Query filter_pro_names = mDatabase.orderByChild("sm_verified").equalTo(true);
        filter_pro_names.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                doc_name_list.clear();
                doc_childkey_list.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String doc_name = postSnapshot.getValue(Professional.class).getName();
                    String doc_childkey = postSnapshot.getValue(Professional.class).getChild_key();
                    doc_name_list.add(doc_name);
                    doc_childkey_list.add(doc_childkey);
                }
                spinner = findViewById(R.id.doctors_spinner);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, doc_name_list);
                adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

                spinner.setAdapter(adapter);

                //call doctor appointed time as soon as the activity created
                callDocAppointTime();

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        callDocAppointTime();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //to set auto current date in calender as soon as the activity created
        appointment_date = Calendar.getInstance().get(Calendar.MONTH)+1 + "/" + Calendar.getInstance().get(Calendar.DATE) + "/" +Calendar.getInstance().get(Calendar.YEAR);
        date_picker.setText(appointment_date);

        date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                long now = cal.getTimeInMillis();

                DatePickerDialog dialog = new DatePickerDialog(
                        Pending_Appointment_Detail.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getDatePicker().setMinDate(now);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                appointment_date = month + "/" + day + "/" + year;
                date_picker.setText(appointment_date);
                date_picker.setTextColor(Color.parseColor("#72bb53"));
                Log.d("checking day",String.valueOf(Calendar.getInstance().get(Calendar.MONTH)));
                callDocAppointTime();
            }
        };

        p_appointment_Time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;

                mTimePicker = new TimePickerDialog(Pending_Appointment_Detail.this, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        try{

                            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                            String now_date = df.format(new Date());
                            Date d = df.parse(appointment_date);
                            String appointment_date_con = df.format(d);

                            if(now_date.equals(appointment_date_con)){
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                                String current_time = sdf.format(new Date());
                                Date current_time_parse = sdf.parse(current_time);
                                String seleted_time = selectedHour + ":" + selectedMinute;
                                Date seleted_time_parse = sdf.parse(seleted_time);
                                if(current_time_parse.compareTo(seleted_time_parse) == 0 || current_time_parse.compareTo(seleted_time_parse) == -1){
                                    p_appointment_Time.setText( selectedHour + ":" + selectedMinute + ":00");
                                    p_appointment_Time.setTextColor(Color.parseColor("#72bb53"));
                                    String convertString_hour = String.valueOf(selectedHour);
                                    String convertString_min = String.valueOf(selectedMinute);

                                    appointment_time = selectedHour + ":" + selectedMinute + ":00";
                                    Log.d("appointment_time",appointment_time);
                                }
                                else{
                                    Toast.makeText(Pending_Appointment_Detail.this, "You can't pick the passed time", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else{
                                p_appointment_Time.setText( selectedHour + ":" + selectedMinute + ":00");
                            }
                        }
                        catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

//        p_appointment_Time.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(Pending_Appointment_Detail.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        p_appointment_Time.setText( selectedHour + ":" + selectedMinute);
//                    }
//                }, hour, minute, false);//Yes 24 hour time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//
//            }
//        });

        if(getIntent().hasExtra("category")){
            Intent intent = new Intent(Pending_Appointment_Detail.this, ReceiveNotificationActivity.class);
            intent.putExtra("category",getIntent().getStringExtra("category"));
            intent.putExtra("brandId",getIntent().getStringExtra("brandId"));
            startActivity(intent);
        }

        //for volley http library
        mRequestQue = Volley.newRequestQueue(this);


        edit_btn_appointment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                time_overlapped_check = false;
//                Toast.makeText(Pending_Appointment_Detail.this,doc_childkey_list.get(spinner.getSelectedItemPosition()),Toast.LENGTH_SHORT).show();
                mStorageRefDocAva = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");
                System.out.println("hello");
                Query doc_ava_query = mStorageRefDocAva.orderByChild("professionalKey").equalTo(doc_childkey_list.get(spinner.getSelectedItemPosition()));
                doc_ava_query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            if (postSnapshot.child("appointmentDate").getValue().equals(appointment_date) && postSnapshot.child("checked").getValue().equals(false)) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                    Date picked_time_parse = sdf.parse(appointment_time);
                                    String picked_time_string = sdf.format(picked_time_parse);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(picked_time_parse);
                                    calendar.add(Calendar.MINUTE, 16);
                                    String picked_time_parse_aftertime = sdf.format(calendar.getTime());
                                    Date picked_time_aftertime_parse = sdf.parse(picked_time_parse_aftertime);

                                    String appointment_time_db = (String) postSnapshot.child("appointmentTime").getValue();
                                    Date appointment_time_parse = sdf.parse(appointment_time_db);
                                    String appointment_time_string = sdf.format(appointment_time_parse);
                                    Calendar calendar_appointment = Calendar.getInstance();
                                    calendar_appointment.setTime(appointment_time_parse);
                                    calendar_appointment.add(Calendar.MINUTE, 16);
                                    String appointment_time_aftertime = sdf.format(calendar_appointment.getTime());
                                    Date appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);
//                                    System.out.println(picked_time_aftertime_parse);
//                                    System.out.println(appointment_time_parse);
//                                    System.out.println(appointment_time_aftertime_parse);
//                                    System.out.println(picked_time_aftertime_parse.after(appointment_time_parse) && picked_time_aftertime_parse.before(appointment_time_aftertime_parse));

                                    boolean first_condition = picked_time_parse.after(appointment_time_parse) && picked_time_parse.before(appointment_time_aftertime_parse);
                                    boolean second_condition = picked_time_aftertime_parse.after(appointment_time_parse) && picked_time_aftertime_parse.before(appointment_time_aftertime_parse);
                                    boolean third_condition = picked_time_string.equals(appointment_time_string) || picked_time_string.equals(appointment_time_aftertime);

                                    if (third_condition || first_condition || second_condition) {
                                        new AlertDialog.Builder(Pending_Appointment_Detail.this, R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                                .setTitle("This assigned time is not available.")
                                                .setMessage(doc_name_list.get(spinner.getSelectedItemPosition()) + " already has appointment between " + appointment_time_db + " to " + appointment_time_aftertime)
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                }).show();
                                        time_overlapped_check = true;
                                        break;
                                    }
//                                    else {
//                                        System.out.println("yahalol");
//                                        postSnapshot.
//                                        edit_check();
//                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
//                            else {
//                                System.out.println("tuturu");
//                                edit_check();
//                            }
                        }
                        if(time_overlapped_check == false){
                            edit_check();
                        }
                    }
                        else{
                            edit_check();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    public void callDocAppointTime(){
        mStorageRefDoc_appoint_Time = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");
        Query doc_appointment_query = mStorageRefDoc_appoint_Time.orderByChild("professionalKey").equalTo(doc_childkey_list.get(spinner.getSelectedItemPosition()));
        doc_appointment_query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                doc_appointment_time_list.clear();
                if(dataSnapshot.exists()){
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot.child("appointmentDate").getValue().equals(appointment_date) && postSnapshot.child("checked").getValue().equals(false)) {
                            doc_appointment_time_list.add(postSnapshot.getValue(MedicalScreeningData.class).getAppointmentTime());

                        } else {
                            doc_appointment_time_list.clear();
                        }
                    }


                }
                else{
                    doc_appointment_time_list.clear();
                }
                recyclerView_doc_appointment_time = findViewById(R.id.doc_appointment_recyclerview);
                final DisplayDoctorAppointmentsAdapter displayDoctorAppointmentsAdapter = new DisplayDoctorAppointmentsAdapter(Pending_Appointment_Detail.this,doc_appointment_time_list);
                recyclerView_doc_appointment_time.setAdapter(displayDoctorAppointmentsAdapter);
                recyclerView_doc_appointment_time.setHasFixedSize(true);

                if(doc_appointment_time_list.size() == 1){
                    recyclerView_doc_appointment_time.setLayoutManager(new GridLayoutManager(Pending_Appointment_Detail.this,1));
                }
                else{
                    recyclerView_doc_appointment_time.setLayoutManager(new GridLayoutManager(Pending_Appointment_Detail.this,2));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    System.out.println("deleted_topics");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        super.onDestroy();
    }

    private void sendNotification(){

        //for push notification
        FirebaseMessaging.getInstance().subscribeToTopic(patient_key);
        String doctor_topic =doc_childkey_list.get(spinner.getSelectedItemPosition()).replace("+","");
        FirebaseMessaging.getInstance().subscribeToTopic(doc_childkey_list.get(spinner.getSelectedItemPosition()).replace("+",""));
        System.out.println(doctor_topic);

        //json object
        JSONObject mainObj = new JSONObject();

        JSONObject mainObj_doc = new JSONObject();
        try {
            mainObj.put("to","/topics/"+patient_key);
            JSONObject notificationObj = new JSONObject();
            notificationObj.put("title","Appointment has been assigned!");
            notificationObj.put("body","Your appointment has been assigned. Please check it's appointment date and time in your appointment list.");
            notificationObj.put("isScheduled","true");
            notificationObj.put("scheduledTime","2020-12-13 14:12:00");

            JSONObject extraData = new JSONObject();
            extraData.put("brandId","puma");
            extraData.put("category","Shoes");

            mainObj.put("notification",notificationObj);
            mainObj.put("data",extraData);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, mainObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError{
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAbhYK_dE:APA91bEV8F2sHqIcZnW1xtdGjy5fpaVeqQQVctSDrzgl6SPaaUGnIUxBiDW2tJB1MryeJXy2Qb_4k6UUnNztLvptcAxVF3up_8lShS2KPWbV9ulgaBZpdLNc9Xc4UH7LRGHy0S4l5E8n");
                    System.out.println("passed here");
                    return header;
                }
            };
            mRequestQue.add(request);



            mainObj_doc.put("to","/topics/"+doc_childkey_list.get(spinner.getSelectedItemPosition()).replace("+",""));
            JSONObject notificationObj_doc = new JSONObject();
            notificationObj_doc.put("title","Appointment has been assigned!");
            notificationObj_doc.put("body","Your appointment has been assigned. Please check it's appointment date and time in your appointment list.");

            JSONObject extraData_doc = new JSONObject();
            extraData_doc.put("brandId","puma");
            extraData_doc.put("category","Shoes");

            mainObj_doc.put("notification",notificationObj_doc);
            mainObj_doc.put("data",extraData_doc);



            JsonObjectRequest request_doc = new JsonObjectRequest(Request.Method.POST, URL, mainObj_doc, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError{
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAbhYK_dE:APA91bEV8F2sHqIcZnW1xtdGjy5fpaVeqQQVctSDrzgl6SPaaUGnIUxBiDW2tJB1MryeJXy2Qb_4k6UUnNztLvptcAxVF3up_8lShS2KPWbV9ulgaBZpdLNc9Xc4UH7LRGHy0S4l5E8n");
                    System.out.println("passed here");
                    return header;
                }
            };
            mRequestQue.add(request_doc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void edit_check () {
        if(date_picker.getText().equals("00-00-0000")){
            Toast.makeText(Pending_Appointment_Detail.this ,"Please choose the appointment date", Toast.LENGTH_LONG).show();
            date_picker.setTextColor(Color.RED);
        }
        else if(p_appointment_Time.getText().equals("00-00-0000")){
            Toast.makeText(Pending_Appointment_Detail.this ,"Please choose the appointment time", Toast.LENGTH_LONG).show();
            p_appointment_Time.setTextColor(Color.RED);
        }
        else if(list.size() == 0){
            Toast.makeText(Pending_Appointment_Detail.this ,"Please upload medical report", Toast.LENGTH_LONG).show();
        }
        else{
            final Dialog dialog = new Dialog(Pending_Appointment_Detail.this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
            dialog.setContentView(R.layout.appointment_edit_loading_alert_dialog);
            ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.video_call_btn);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            mStorageRef = FirebaseStorage.getInstance().getReference().child("MedicalReports").child(patient_key).child(date);
            for (upload_count = 0; upload_count < ImageList.size(); upload_count++) {
                final StorageReference Ref = mStorageRef.child("mRec" + (upload_count + 1));
                uploadTask = Ref.putFile(ImageList.get(upload_count));
                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return Ref.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            String img_name = "mRec"+(upload_count+1);
                            downloadUri = task.getResult();
//                            Toast.makeText(Pending_Appointment_Detail.this ,"Image Uploaded successfully", Toast.LENGTH_LONG).show();
                            MedicalRecordImage med_report_data = new MedicalRecordImage(img_name,downloadUri.toString());
                            final_med_report_list.add(med_report_data);

                            SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                            String mo_childkey =settings.getString("key", "child_key");

                            if(final_med_report_list.size() == upload_count){

                                MedicalScreeningData data = new MedicalScreeningData(patient_key, patient_name, doc_childkey_list.get(spinner.getSelectedItemPosition()),doc_name_list.get(spinner.getSelectedItemPosition()),"", mo_childkey, maritalStatus, address,temperature,medicalNotes, haveCough,  haveTroubleBreathing, haveTravelHistory, checked, date, checkedDate, record_list, type_value, appointment_date, appointment_time,final_med_report_list);

                                FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed").push().setValue(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        pending_list_rm_Database = FirebaseDatabase.getInstance().getReference().child("MedicalRecords").child(completed_appointment_childkeys);
                                        pending_list_rm_Database.removeValue();
                                        patient_hasappointment_true_db_ref = FirebaseDatabase.getInstance().getReference().child("Patients").child(patient_key);
                                        patient_hasappointment_true_db_ref.child("hasAppointment").setValue(true);
                                        sendNotification();
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {

                                            }
                                        });
                            }

                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE) {
            if(resultCode == RESULT_OK) {
                if(data.getClipData() != null) {
                    int countClipData = data.getClipData().getItemCount();
                    int currentImageSelect = 0;

                    while(currentImageSelect < countClipData) {
                        String name = "mRec" + (currentImageSelect+1);
                        ImageUri = data.getClipData().getItemAt(currentImageSelect).getUri();
                        MedicalRecordImage mrec = new MedicalRecordImage(name, String.valueOf(ImageUri));
                        list.add(mrec);
                        ImageList.add(ImageUri);
                        currentImageSelect = currentImageSelect + 1;
                    }
                    med_report_uploadbtn.setVisibility(View.GONE);
                } else {
                    int currentImageSelect = 0;
                    String name = "mRec" + (currentImageSelect+1);
                    ImageUri = data.getData();
                    MedicalRecordImage mrec = new MedicalRecordImage(name, String.valueOf(ImageUri));
                    list.add(mrec);
                    ImageList.add(ImageUri);

//                    Toast.makeText(this, "Please select multiple images.", Toast.LENGTH_SHORT).show();
                }
                //```````recycle```````View for medical report
                recyclerView_med_report = findViewById(R.id.med_report_recyclerview);

                MedicalReportAdapter medicalReportAdapter = new MedicalReportAdapter(Pending_Appointment_Detail.this,list,med_report_uploadbtn,true);
                recyclerView_med_report.setAdapter(medicalReportAdapter);
                recyclerView_med_report.setHasFixedSize(true);
                recyclerView_med_report.setLayoutManager(new LinearLayoutManager(Pending_Appointment_Detail.this,LinearLayoutManager.HORIZONTAL,false));

                ////////////////////////////////////////////////////////////////////////////////////////
            }
        }
    }
    public void onMethodCallback(int position) {
        ImageList.remove(position);
//        Toast.makeText(Pending_Appointment_Detail.this, "You can't pick the passed time", Toast.LENGTH_SHORT).show();
    }
}
