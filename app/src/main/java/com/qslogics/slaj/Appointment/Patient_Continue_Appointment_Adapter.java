package com.qslogics.slaj.Appointment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;
import com.qslogics.slaj.ScreeningFiles.ScreeningFiles;
import com.qslogics.slaj.VideoCall.VideoActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Patient_Continue_Appointment_Adapter extends RecyclerView.Adapter<Patient_Continue_Appointment_Adapter.Patient_Completed_AppointmentValue>{
    List<MedicalScreeningData> mainlist;
    List<String> completed_appointment_childkeys;
    Context context;
    Intent intent;
    DateFormat df;
    String now;
    String now_two;
    Date d;
    Date now_parse;
    String getnewdate;

    SimpleDateFormat sdf;
    String current_time;
    Date current_time_parse;
    String appointment_time;
    Date appointment_time_parse;
    Calendar calendar;
    String appointment_time_aftertime;
    Date appointment_time_aftertime_parse;

    private DatabaseReference mDatabase_assigned_docname;

    private DatabaseReference mDatabase_patient;

    public Patient_Continue_Appointment_Adapter(Context ct, List<MedicalScreeningData> list,List<String> passed_completed_appointment_childkeys){
        Collections.reverse(list);
        context = ct;
        mainlist = list;
        completed_appointment_childkeys = passed_completed_appointment_childkeys;
    }

    @NonNull
    @Override
    public Patient_Continue_Appointment_Adapter.Patient_Completed_AppointmentValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_patient_pending_appointment_adapter,parent,false);
        return new Patient_Continue_Appointment_Adapter.Patient_Completed_AppointmentValue(view);
    }

    public void onBindViewHolder(@NonNull Patient_Continue_Appointment_Adapter.Patient_Completed_AppointmentValue holder, final int position) {

//        df = new SimpleDateFormat("MM/dd/yy");
//        now = df.format(new Date());
//        now_two = mainlist.get(position).getAppointmentDate();
//        try{
//            d = df.parse(now_two);
//            now_parse = df.parse(now);
//            getnewdate = df.format(d);
//
//            sdf = new SimpleDateFormat("HH:mm:ss");
//            current_time = sdf.format(new Date());
//            current_time_parse = sdf.parse(current_time);
//            appointment_time = mainlist.get(position).getAppointmentTime();
//            appointment_time_parse = sdf.parse(appointment_time);
//            calendar = Calendar.getInstance();
//            calendar.setTime(appointment_time_parse);
//            calendar.add(Calendar.MINUTE, 15);
//            appointment_time_aftertime = sdf.format(calendar.getTime());
//            appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);
//
//            Log.d("current_time",String.valueOf(current_time_parse));
//            Log.d("appointment_time",String.valueOf(appointment_time_aftertime_parse));
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        mDatabase_assigned_docname = FirebaseDatabase.getInstance().getReference();
//
//        mDatabase_patient = FirebaseDatabase.getInstance().getReference();
//
//        boolean another_condition = now_parse.compareTo(d) == 0 && current_time_parse.compareTo(appointment_time_aftertime_parse) >= 0;
//        if(now_parse.after(d) || another_condition){
//            String appointed_professionalKey = mainlist.get(position).getProfessionalKey();
//            Log.d("passed_here_test",mainlist.get(position).getProfessionalKey());
//            MedicalScreeningData data = new MedicalScreeningData(mainlist.get(position).getPatientKey(), mainlist.get(position).getPatientName(), mainlist.get(position).getProfessionalKey(),mainlist.get(position).getProfessionalName(),mainlist.get(position).getProfessionalKey(), mainlist.get(position).getMoKey(), mainlist.get(position).getMaritalStatus(), mainlist.get(position).getAddress(),mainlist.get(position).getTemperature(),mainlist.get(position).getMedicalNotes(), mainlist.get(position).isHaveCough(),  mainlist.get(position).isHaveTroubleBreathing(), mainlist.get(position).isHaveTravelHistory(), true, mainlist.get(position).getScreeningDate(), mainlist.get(position).getCheckedDate(), mainlist.get(position).getMedicalRecords(), mainlist.get(position).getType(), mainlist.get(position).getAppointmentDate(), mainlist.get(position).getAppointmentTime(),mainlist.get(position).getMedicalReports());
//
//            mDatabase_assigned_docname.child("MedicalRecords_Completed").child(completed_appointment_childkeys.get(position)).child("checked").setValue(true);
//            mDatabase_patient.child("Patients").child(mainlist.get(position).getPatientKey()).child("hasAppointment").setValue(false);
//        }

        String professionalName = mainlist.get(position).getProfessionalName();
        String time = mainlist.get(position).getAppointmentTime();

        if(!TextUtils.isEmpty(professionalName)) {
            holder.professional_name.setText(mainlist.get(position).getProfessionalName());
        }

        if(!TextUtils.isEmpty(time)) {
            holder.alarms_time.setText(mainlist.get(position).getAppointmentTime());
        }

        String[] date = mainlist.get(position).getScreeningDate().split("-");
        holder.day.setText(date[0]);
        holder.month.setText(date[1]);
        holder.year.setText(date[2]);

//        holder.pending_a_card.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//
//                intent = new Intent(view.getContext(), VideoActivity.class);
//
//                context.startActivity(intent);
//            }
//        });
        if(mainlist.get(position).isChecked() == true){
            holder.btn_video_call.setVisibility(View.GONE);
        }

        holder.btn_video_call.setOnClickListener(new View.OnClickListener(){
            @Override

            public void onClick(View view) {
                DateFormat df = new SimpleDateFormat("MM/dd/yy");
                String now = df.format(new Date());
                Log.d("now",now);
                String now_two = mainlist.get(position).getAppointmentDate();
                Date d;

                try{
                    d = df.parse(now_two);
                    String getnewdate = df.format(d);

                    if(now.equals(getnewdate)){

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        String current_time = sdf.format(new Date());
                        Date current_time_parse = sdf.parse(current_time);
                        String appointment_time = mainlist.get(position).getAppointmentTime();
                        Date appointment_time_parse = sdf.parse(appointment_time);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(appointment_time_parse);
                        calendar.add(Calendar.MINUTE, 15);
                        String appointment_time_aftertime = sdf.format(calendar.getTime());
                        Date appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);

                        if(current_time_parse.compareTo(appointment_time_parse) >= 0 && current_time_parse.compareTo(appointment_time_aftertime_parse) == -1){
                            final Dialog dialog = new Dialog(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                            dialog.setContentView(R.layout.appointment_call_alert_dialog);
                            ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.video_call_btn);

                            dialogButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    intent = new Intent(view.getContext(), VideoActivity.class);
                                    intent.putExtra("completed_appointment_childkeys", completed_appointment_childkeys.get(position));
                                    intent.putExtra("completed_appointment_pro_key", mainlist.get(position).getProfessionalKey());
                                    intent.putExtra("completed_appointment_time", mainlist.get(position).getAppointmentTime());
                                    intent.putExtra("from", "patient");
                                    intent.putExtra("professional_name", mainlist.get(position).getProfessionalName());
                                    intent.putExtra("patient_key", mainlist.get(position).getPatientKey());
                                    intent.putExtra("patient_name", mainlist.get(position).getPatientName());
                                    context.startActivity(intent);

                                }
                            });
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.show();
                        }
                        else{
                            new AlertDialog.Builder(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                    .setTitle("Couldn't not perform Audio/Video session")
                                    .setMessage((current_time_parse.compareTo(appointment_time_parse) == -1)?"Appointment time hasn't reached yet. Please check it and try again later.":"Appointment time has exceeded. Please take another appointment and try again.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        }
                    }
                    else{
                        new AlertDialog.Builder(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                .setTitle("Couldn't not perform Audio/Video session")
                                .setMessage("Appointment day hasn't reached or exceeded. Please check it and try again later.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.screening_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading
                Dialog dialog = new Dialog(v.getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                dialog.setContentView(R.layout.loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.DKGRAY));
                dialog.show();

                Intent screeningFiles = new Intent(v.getContext(), ScreeningFiles.class);
                screeningFiles.putExtra("childKeys", completed_appointment_childkeys.get(position));
                screeningFiles.putExtra("medicalScreeningStatus", "completed");
                screeningFiles.putExtra("patientKey", mainlist.get(position).getPatientKey());
                dialog.dismiss();
                context.startActivity(screeningFiles);
            }
        });
    }

    public int getItemCount() {
        return mainlist.size();
    }

    public class Patient_Completed_AppointmentValue extends RecyclerView.ViewHolder {

        TextView professional_name;
        TextView alarms_time;
        TextView day;
        TextView month;
        TextView year;
        CardView pending_a_card;
        Button btn_video_call;
        TextView screening_file;

        public Patient_Completed_AppointmentValue(@NonNull View itemView) {
            super(itemView);
            professional_name = itemView.findViewById(R.id._title);
            alarms_time = itemView.findViewById(R.id.alarms_time);
            pending_a_card = itemView.findViewById(R.id.pending_a_card);
            day = itemView.findViewById(R.id.day);
            year = itemView.findViewById(R.id.year);
            month = itemView.findViewById(R.id.month);
            btn_video_call = itemView.findViewById(R.id.btn_video_call);
            screening_file = itemView.findViewById(R.id.screening_file);
        }
    }
}
