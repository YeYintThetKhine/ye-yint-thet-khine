package com.qslogics.slaj.Appointment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.Login.Login_phone;
import com.qslogics.slaj.R;
import com.qslogics.slaj.Register.PhRegisterActivity;
import com.qslogics.slaj.ScreeningFiles.ScreeningFiles;
import com.qslogics.slaj.VideoCall.VideoActivity;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Continue_Appointments_Adapter extends RecyclerView.Adapter<Continue_Appointments_Adapter.Continue_AppointmentValue> {

    List<MedicalScreeningData> mainlist;
    List<String> completed_appointment_childkeys;
    Context context;
    String usertype;
    Intent intent;


    private DatabaseReference mDatabase_assigned_docname;
    private DatabaseReference mDatabase_patient;

    public Continue_Appointments_Adapter(Context ct, List<MedicalScreeningData> list,List<String> recycle_completed_appointment_childkeys,String usertype_passed){
        context = ct;
        mainlist = list;
        completed_appointment_childkeys = recycle_completed_appointment_childkeys;
        usertype = usertype_passed;

    }

    @NonNull
    @Override
    public Continue_Appointments_Adapter.Continue_AppointmentValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_continue__appointments__adapter,parent,false);
        return new Continue_Appointments_Adapter.Continue_AppointmentValue(view);
    }

    public Continue_Appointments_Adapter() {

    }

    public void onBindViewHolder(@NonNull final Continue_Appointments_Adapter.Continue_AppointmentValue holder, final int position) {

        DateFormat df = new SimpleDateFormat("MM/dd/yy");
        String now = df.format(new Date());

        String now_two = mainlist.get(position).getAppointmentDate();
        Date d;
        Date now_parse;
        mDatabase_assigned_docname = FirebaseDatabase.getInstance().getReference();


        try {
            now_parse = df.parse(now);
            d = df.parse(now_two);
            SimpleDateFormat date = new SimpleDateFormat("dd");
            SimpleDateFormat month = new SimpleDateFormat("MMM");
            SimpleDateFormat year = new SimpleDateFormat("yyyy");
            holder.day.setText(date.format(d));
            holder.month.setText(month.format(d));
            holder.year.setText(year.format(d));

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String current_time = sdf.format(new Date());
            Date current_time_parse = sdf.parse(current_time);
            String appointment_time = mainlist.get(position).getAppointmentTime();
            Date appointment_time_parse = sdf.parse(appointment_time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(appointment_time_parse);
            calendar.add(Calendar.MINUTE, 15);
            String appointment_time_aftertime = sdf.format(calendar.getTime());
            Date appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);

            Log.d("current_time",String.valueOf(current_time_parse));
            Log.d("appointment_time",String.valueOf(appointment_time_aftertime_parse));

            boolean another_condition = now_parse.compareTo(d) == 0 && current_time_parse.compareTo(appointment_time_aftertime_parse) >= 0;

            if(now_parse.compareTo(d) > 0 || another_condition){

//                MedicalScreeningData data = new MedicalScreeningData(mainlist.get(position).getPatientKey(), mainlist.get(position).getPatientName(), mainlist.get(position).getProfessionalKey(),mainlist.get(position).getProfessionalName(),mainlist.get(position).getProfessionalKey(), mainlist.get(position).getMoKey(), mainlist.get(position).getMaritalStatus(), mainlist.get(position).getAddress(),mainlist.get(position).getTemperature(),mainlist.get(position).getMedicalNotes(), mainlist.get(position).isHaveCough(),  mainlist.get(position).isHaveTroubleBreathing(), mainlist.get(position).isHaveTravelHistory(), true, mainlist.get(position).getScreeningDate(), mainlist.get(position).getCheckedDate(), mainlist.get(position).getMedicalRecords(), mainlist.get(position).getType(), mainlist.get(position).getAppointmentDate(), mainlist.get(position).getAppointmentTime(),mainlist.get(position).getMedicalReports());
//                mDatabase_assigned_docname.child("MedicalRecords_Completed").child(completed_appointment_childkeys.get(position)).child("checked").setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//
//                    }
//                });
                if(usertype.equals("MO") || usertype.equals("Professional")){

                }
                else{
                    mDatabase_assigned_docname.child("MedicalRecords_Completed").child(completed_appointment_childkeys.get(position)).child("checked").setValue(true);
                    mDatabase_patient = FirebaseDatabase.getInstance().getReference();
                    mDatabase_patient.child("Patients").child(mainlist.get(position).getPatientKey()).child("hasAppointment").setValue(false);
                }
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        holder._assigned_doc_title.setText(mainlist.get(position).getProfessionalName());
        holder.patient_key.setText(mainlist.get(position).getPatientName());
        holder.alarms_time.setText(mainlist.get(position).getAppointmentTime());

        if(mainlist.get(position).isChecked() == true){
            holder.btn_video_call.setVisibility(View.GONE);
        }

        holder.pending_a_card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                intent = new Intent(view.getContext(), Compeleted_Appointment_Detail.class);

                intent.putExtra("completed_appointment_childkeys", completed_appointment_childkeys.get(position));

                context.startActivity(intent);
            }
        });

        holder.btn_video_call.setOnClickListener(new View.OnClickListener(){
            @Override

            public void onClick(View view) {

                DateFormat df = new SimpleDateFormat("MM/dd/yy");
                String now = df.format(new Date());

                String now_two = mainlist.get(position).getAppointmentDate();
                Date d;
                try {
                    d = df.parse(now_two);
                    String getnewdate = df.format(d);

                    if(now.equals(getnewdate)){


                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        String current_time = sdf.format(new Date());
                        Date current_time_parse = sdf.parse(current_time);
                        String appointment_time = mainlist.get(position).getAppointmentTime();
                        Date appointment_time_parse = sdf.parse(appointment_time);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(appointment_time_parse);
                        calendar.add(Calendar.MINUTE, 15);
                        String appointment_time_aftertime = sdf.format(calendar.getTime());
                        Date appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);

                        if(current_time_parse.compareTo(appointment_time_parse) >= 0 && current_time_parse.compareTo(appointment_time_aftertime_parse) == -1){

//                            final Dialog dialog = new Dialog(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
//                            dialog.setContentView(R.layout.videochat_before_loading_alert_dialog);
//                            ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.video_call_btn);
//                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                            dialog.show();

                            final Dialog dialog = new Dialog(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                            dialog.setContentView(R.layout.appointment_call_alert_dialog);
                            ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.video_call_btn);

                            dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
//                        Toast.makeText(context.getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
                                    intent = new Intent(view.getContext(), VideoActivity.class);
                                    intent.putExtra("completed_appointment_childkeys", completed_appointment_childkeys.get(position));
                                    intent.putExtra("completed_appointment_pro_key", mainlist.get(position).getProfessionalKey());
                                    intent.putExtra("completed_appointment_time", mainlist.get(position).getAppointmentTime());
                                    intent.putExtra("from", "professional");
                                    intent.putExtra("professional_name", mainlist.get(position).getProfessionalName());
                                    intent.putExtra("patient_name", mainlist.get(position).getPatientName());
                                    intent.putExtra("patient_key", mainlist.get(position).getPatientKey());

                                    context.startActivity(intent);
                                }
                            });
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.show();
                        }
                        else{
                            new AlertDialog.Builder(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                    .setTitle("Couldn't not perform Audio/Video session")
                                    .setMessage((current_time_parse.compareTo(appointment_time_parse) == -1)?"Appointment time hasn't reached yet. Please check it and try again later.":"Appointment time has exceeded. Please take another appointment and try again.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        }
                    }
                    else{
                        new AlertDialog.Builder(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                .setTitle("Couldn't not perform Audio/Video session")
                                .setMessage("Appointment day hasn't reached or exceeded. Please check it and try again later.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.viewScreeningFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent screeningFiles = new Intent(v.getContext(), ScreeningFiles.class);
                screeningFiles.putExtra("childKeys", completed_appointment_childkeys.get(position));
                screeningFiles.putExtra("medicalScreeningStatus", "completed");
                screeningFiles.putExtra("patientKey", mainlist.get(position).getPatientKey());
                context.startActivity(screeningFiles);
            }
        });
    }

    public int getItemCount() {
        return mainlist.size();
    }

    public class Continue_AppointmentValue extends RecyclerView.ViewHolder {

        TextView _assigned_doc_title;
        TextView patient_key;
        TextView alarms_time;
        TextView day;
        TextView month;
        TextView year;
        CardView pending_a_card;
        Button btn_video_call;
        TextView viewScreeningFile;

        public Continue_AppointmentValue(@NonNull View itemView) {
            super(itemView);
            _assigned_doc_title = itemView.findViewById(R.id._assigned_doc_title);
            patient_key = itemView.findViewById(R.id.patient_key);
            alarms_time = itemView.findViewById(R.id.alarms_time);
            day = itemView.findViewById(R.id.day);
            month = itemView.findViewById(R.id.month);
            year = itemView.findViewById(R.id.year);
            pending_a_card = itemView.findViewById(R.id.pending_a_card);
            btn_video_call = itemView.findViewById(R.id.btn_video_call);
            btn_video_call.setText("Join Now");

            viewScreeningFile = itemView.findViewById(R.id.screening_file);

            if(usertype.equals("MO")){
                btn_video_call.setVisibility(View.GONE);
            }
        }
    }
}
