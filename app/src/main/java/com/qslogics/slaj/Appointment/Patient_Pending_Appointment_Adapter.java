package com.qslogics.slaj.Appointment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.Login.Login;
import com.qslogics.slaj.R;
import com.qslogics.slaj.ScreeningFiles.ScreeningFiles;
import com.qslogics.slaj.VideoCall.VideoActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

// for firebase push noti
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;


public class Patient_Pending_Appointment_Adapter extends RecyclerView.Adapter<Patient_Pending_Appointment_Adapter.Patient_Pending_AppointmentValue>{
    List<MedicalScreeningData> mainlist;
    Context context;
    List<String> childKeys;
    Intent intent;
    DateFormat df;
    String now;
    String now_two;
    Date d;
    Date now_parse;
    String getnewdate;

    SimpleDateFormat sdf;
    String current_time;
    Date current_time_parse;
    String appointment_time;
    Date appointment_time_parse;
    Calendar calendar;
    String appointment_time_aftertime;
    Date appointment_time_aftertime_parse;

    private DatabaseReference mDatabase_assigned_docname;

    private DatabaseReference mDatabase_patient;

    public Patient_Pending_Appointment_Adapter(Context ct, List<MedicalScreeningData> list, List<String> childKeys){
        context = ct;
        mainlist = list;
        this.childKeys = childKeys;
    }

    @NonNull
    @Override
    public Patient_Pending_Appointment_Adapter.Patient_Pending_AppointmentValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_patient_pending_appointment_adapter,parent,false);
        return new Patient_Pending_Appointment_Adapter.Patient_Pending_AppointmentValue(view);
    }

    public void onBindViewHolder(@NonNull Patient_Pending_Appointment_Adapter.Patient_Pending_AppointmentValue holder, final int position) {

        df = new SimpleDateFormat("MM/dd/yy");
        now = df.format(new Date());
        now_two = mainlist.get(position).getAppointmentDate();
        try{
            d = df.parse(now_two);
            now_parse = df.parse(now);
            getnewdate = df.format(d);

            sdf = new SimpleDateFormat("HH:mm:ss");
            current_time = sdf.format(new Date());
            current_time_parse = sdf.parse(current_time);
            appointment_time = mainlist.get(position).getAppointmentTime();
            appointment_time_parse = sdf.parse(appointment_time);
            calendar = Calendar.getInstance();
            calendar.setTime(appointment_time_parse);
            calendar.add(Calendar.MINUTE, 15);
            appointment_time_aftertime = sdf.format(calendar.getTime());
            appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);

            Log.d("current_time",String.valueOf(current_time_parse));
            Log.d("appointment_time",String.valueOf(appointment_time_aftertime_parse));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        mDatabase_assigned_docname = FirebaseDatabase.getInstance().getReference();

        mDatabase_patient = FirebaseDatabase.getInstance().getReference();

        boolean another_condition = now_parse.compareTo(d) == 0 && current_time_parse.compareTo(appointment_time_aftertime_parse) >= 0;
        if(now_parse.compareTo(d) > 0 || another_condition){
            String appointed_professionalKey = mainlist.get(position).getProfessionalKey();
            Log.d("passed_here_test",mainlist.get(position).getProfessionalKey());

            mDatabase_assigned_docname.child("MedicalRecords_Completed").child(childKeys.get(position)).child("checked").setValue(true);
            mDatabase_patient = FirebaseDatabase.getInstance().getReference();
            mDatabase_patient.child("Patients").child(mainlist.get(position).getPatientKey()).child("hasAppointment").setValue(false);
        }

        if(mainlist.get(position).isChecked() == true){
            holder.btn_video_call.setVisibility(View.GONE);
        }

        holder.btn_video_call.setOnClickListener(new View.OnClickListener(){
            @Override

            public void onClick(View view) {
                DateFormat df = new SimpleDateFormat("MM/dd/yy");
                String now = df.format(new Date());
                Log.d("now",now);
                String now_two = mainlist.get(position).getAppointmentDate();
                Date d;

                try{
                    d = df.parse(now_two);
                    String getnewdate = df.format(d);

                    if(now.equals(getnewdate)){

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        String current_time = sdf.format(new Date());
                        Date current_time_parse = sdf.parse(current_time);
                        String appointment_time = mainlist.get(position).getAppointmentTime();
                        Date appointment_time_parse = sdf.parse(appointment_time);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(appointment_time_parse);
                        calendar.add(Calendar.MINUTE, 15);
                        String appointment_time_aftertime = sdf.format(calendar.getTime());
                        Date appointment_time_aftertime_parse = sdf.parse(appointment_time_aftertime);

                        if(current_time_parse.compareTo(appointment_time_parse) >= 0 && current_time_parse.compareTo(appointment_time_aftertime_parse) == -1){
                            final Dialog dialog = new Dialog(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                            dialog.setContentView(R.layout.appointment_call_alert_dialog);
                            ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.video_call_btn);

                            dialogButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    //for volley http library
                                    RequestQueue mRequestQue;

                                    String URL = "https://fcm.googleapis.com/fcm/send";

                                    //for volley http library
                                    mRequestQue = Volley.newRequestQueue(context);

                                    //for push notification
                                    FirebaseMessaging.getInstance().subscribeToTopic(mainlist.get(position).getProfessionalKey());


                                    //json object
                                    JSONObject mainObj = new JSONObject();

                                    JSONObject mainObj_doc = new JSONObject();
                                    try {
                                        mainObj.put("to","/topics/"+mainlist.get(position).getProfessionalKey());
                                        JSONObject notificationObj = new JSONObject();
                                        notificationObj.put("title","Appointment No.12 has started.");
                                        notificationObj.put("body","Its time for your video appointment. Please join the video chat room immediately");

                                        JSONObject extraData = new JSONObject();
                                        extraData.put("brandId","puma");
                                        extraData.put("category","Shoes");

                                        mainObj.put("notification",notificationObj);
                                        mainObj.put("data",extraData);


                                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, mainObj, new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {

                                            }
                                        }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                            }
                                        }){
                                            @Override
                                            public Map<String, String> getHeaders() throws AuthFailureError{
                                                Map<String, String> header = new HashMap<>();
                                                header.put("content-type","application/json");
                                                header.put("authorization","key=AAAAbhYK_dE:APA91bEV8F2sHqIcZnW1xtdGjy5fpaVeqQQVctSDrzgl6SPaaUGnIUxBiDW2tJB1MryeJXy2Qb_4k6UUnNztLvptcAxVF3up_8lShS2KPWbV9ulgaBZpdLNc9Xc4UH7LRGHy0S4l5E8n");
                                                System.out.println("passed here");
                                                return header;
                                            }
                                        };
                                        mRequestQue.add(request);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    intent = new Intent(view.getContext(), VideoActivity.class);
                                    intent.putExtra("completed_appointment_childkeys", childKeys.get(position));
                                    intent.putExtra("completed_appointment_pro_key", mainlist.get(position).getProfessionalKey());
                                    intent.putExtra("completed_appointment_time", mainlist.get(position).getAppointmentTime());
                                    intent.putExtra("from", "patient");
                                    intent.putExtra("professional_name", mainlist.get(position).getProfessionalName());
                                    intent.putExtra("patient_key", mainlist.get(position).getPatientKey());
                                    intent.putExtra("patient_name", mainlist.get(position).getPatientName());
                                    context.startActivity(intent);

                                }
                            });
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.show();
                        }
                        else{
                            new AlertDialog.Builder(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                    .setTitle("Couldn't not perform Audio/Video session")
                                    .setMessage((current_time_parse.compareTo(appointment_time_parse) == -1)?"Appointment time hasn't reached yet. Please check it and try again later.":"Appointment time has exceeded. Please take another appointment and try again.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        }
                    }
                    else{
                        new AlertDialog.Builder(context,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                .setTitle("Couldn't not perform Audio/Video session")
                                .setMessage("Appointment day hasn't reached or exceeded. Please check it and try again later.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        String professionalName = mainlist.get(position).getProfessionalName();
        String time = mainlist.get(position).getAppointmentTime();

        if(!TextUtils.isEmpty(professionalName)) {
            holder.professional_name.setText(mainlist.get(position).getProfessionalName());
        }

        if(!TextUtils.isEmpty(time)) {
            holder.alarms_time.setText(mainlist.get(position).getAppointmentTime());
        }

        String[] date = mainlist.get(position).getScreeningDate().split("-");
        holder.day.setText(date[0]);
        holder.month.setText(date[1]);
        holder.year.setText(date[2]);

        holder.screening_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //loading
                Dialog dialog = new Dialog(v.getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                dialog.setContentView(R.layout.loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.DKGRAY));
                dialog.show();

                Intent screeningFiles = new Intent(v.getContext(), ScreeningFiles.class);
                screeningFiles.putExtra("childKeys", childKeys.get(position));
                screeningFiles.putExtra("medicalScreeningStatus", "pending");
                screeningFiles.putExtra("patientKey", mainlist.get(position).getPatientKey());
                dialog.dismiss();
                context.startActivity(screeningFiles);
            }
        });
    }

    public int getItemCount() {
        return mainlist.size();
    }

    public class Patient_Pending_AppointmentValue extends RecyclerView.ViewHolder {

        TextView professional_name;
        TextView alarms_time;
        TextView day;
        TextView month;
        TextView year;
        CardView pending_a_card;
        Button btn_video_call;
        TextView screening_file;

        public Patient_Pending_AppointmentValue(@NonNull View itemView) {
            super(itemView);
            professional_name = itemView.findViewById(R.id._title);
            alarms_time = itemView.findViewById(R.id.alarms_time);
            pending_a_card = itemView.findViewById(R.id.pending_a_card);
            day = itemView.findViewById(R.id.day);
            year = itemView.findViewById(R.id.year);
            month = itemView.findViewById(R.id.month);
            btn_video_call = itemView.findViewById(R.id.btn_video_call);
            screening_file = itemView.findViewById(R.id.screening_file);
        }
    }
}
