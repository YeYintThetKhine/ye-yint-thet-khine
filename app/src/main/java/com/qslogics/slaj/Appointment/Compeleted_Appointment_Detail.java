package com.qslogics.slaj.Appointment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.R;

import java.util.ArrayList;

public class Compeleted_Appointment_Detail extends AppCompatActivity {
    private DatabaseReference mDatabase;

    TextView edit_id;
    TextView date_picker;
    TextView p_appointment_Time;
    TextView doctorname;
    ArrayList<MedicalRecordImage> med_record_list=new ArrayList<MedicalRecordImage>();
    ArrayList<MedicalRecordImage> med_report_list=new ArrayList<MedicalRecordImage>();
    ImageView imgBackArrow;
    String completed_appointment_key;

    RecyclerView recyclerView;
    RecyclerView recyclerView_med_report;

    Button med_report_uploadbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compeleted__appointment__detail);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        med_report_uploadbtn = findViewById(R.id.edit_btn_appointment);

        med_report_uploadbtn.setVisibility(View.GONE);

        completed_appointment_key = getIntent().getStringExtra("completed_appointment_childkeys");

        mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");

        edit_id = findViewById(R.id.edit_id);
        date_picker = findViewById(R.id.tvDate);
        p_appointment_Time = findViewById(R.id.p_appointment_Time);
        doctorname = findViewById(R.id.doctorname);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    edit_id.setText(dataSnapshot.child(completed_appointment_key).child("patientKey").getValue().toString());
                    doctorname.setText(dataSnapshot.child(completed_appointment_key).child("professionalName").getValue().toString());
                    date_picker.setText(dataSnapshot.child(completed_appointment_key).child("appointmentDate").getValue().toString());
                    p_appointment_Time.setText(dataSnapshot.child(completed_appointment_key).child("appointmentTime").getValue().toString());
                    for (DataSnapshot postSnapshot : dataSnapshot.child(completed_appointment_key).child("medicalRecords").getChildren()) {
                        MedicalRecordImage medicalRecordImage = postSnapshot.getValue(MedicalRecordImage.class);
                        med_record_list.add(medicalRecordImage);
                    }

                    recyclerView = findViewById(R.id.mrecords);

                    final MedicalRecord_Appointment_Adapter medicalRecord_appointment_Adapter = new MedicalRecord_Appointment_Adapter(Compeleted_Appointment_Detail.this,med_record_list);
                    recyclerView.setAdapter(medicalRecord_appointment_Adapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Compeleted_Appointment_Detail.this,LinearLayoutManager.HORIZONTAL,false));


                    for (DataSnapshot postSnapshot : dataSnapshot.child(completed_appointment_key).child("medicalReports").getChildren()) {
                        MedicalRecordImage medicalRecordImage = postSnapshot.getValue(MedicalRecordImage.class);
                        med_report_list.add(medicalRecordImage);
                    }

                    //recycleView for medical report
                    recyclerView_med_report = findViewById(R.id.med_report_recyclerview);

                    MedicalReportAdapter medicalReportAdapter = new MedicalReportAdapter(Compeleted_Appointment_Detail.this,med_report_list,med_report_uploadbtn,false);
                    recyclerView_med_report.setAdapter(medicalReportAdapter);
                    recyclerView_med_report.setHasFixedSize(true);
                    recyclerView_med_report.setLayoutManager(new LinearLayoutManager(Compeleted_Appointment_Detail.this,LinearLayoutManager.HORIZONTAL,false));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
