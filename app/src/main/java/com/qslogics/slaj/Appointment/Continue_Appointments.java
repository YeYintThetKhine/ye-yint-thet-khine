package com.qslogics.slaj.Appointment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Continue_Appointments extends Fragment {

    RecyclerView recyclerView;
    private DatabaseReference mDatabase;
    List<MedicalScreeningData> list_completed_appointments;
    List<String> completed_appointment_childkeys;
    String user_child_key;
    String usertype;
    Query filter_query;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_continue__appointments, container, false);

        list_completed_appointments=new ArrayList<>();
        completed_appointment_childkeys=new ArrayList<>();

        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        usertype =settings.getString("type", "usertype");
        user_child_key =settings.getString("key", "child_key");

        if(usertype.equals("Professional")){
            mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");
            filter_query = mDatabase.orderByChild("professionalKey").equalTo(user_child_key);
        }
        if(usertype.equals("MO")){
            mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");
            filter_query = mDatabase.orderByChild("moKey").equalTo(user_child_key);
        }



        filter_query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_completed_appointments.clear();
                completed_appointment_childkeys.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    MedicalScreeningData appointments_entity = postSnapshot.getValue(MedicalScreeningData.class);
                    if(usertype.equals("Professional") && postSnapshot.child("checked").getValue().equals(true)){
                        list_completed_appointments.add(appointments_entity);
                        completed_appointment_childkeys.add(postSnapshot.getKey());
                    }
                    else if(usertype.equals("MO")){
                        list_completed_appointments.add(appointments_entity);
                        completed_appointment_childkeys.add(postSnapshot.getKey());
                    }
                }

                recyclerView = rootView.findViewById(R.id.appointment_continue_recycleview);

                final Continue_Appointments_Adapter continue_Appointments_Adapter = new Continue_Appointments_Adapter(getContext(),list_completed_appointments,completed_appointment_childkeys,usertype);
                recyclerView.setAdapter(continue_Appointments_Adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }
}
