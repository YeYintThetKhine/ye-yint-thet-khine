package com.qslogics.slaj.Appointment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;
import com.qslogics.slaj.ScreeningFiles.ScreeningFiles;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Pending_Appointment_Adapter extends RecyclerView.Adapter<Pending_Appointment_Adapter.Pending_AppointmentValue>{
    List<MedicalScreeningData> mainlist;
    Context context;
    List<String> completed_appointment_childkeys;

    public Pending_Appointment_Adapter(Context ct, List<MedicalScreeningData> list,List<String> test_completed_appointment_childkeys){
        context = ct;
        mainlist = list;
        completed_appointment_childkeys = test_completed_appointment_childkeys;
    }

    @NonNull
    @Override
    public Pending_Appointment_Adapter.Pending_AppointmentValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_pending__appointment__adapter,parent,false);
        return new Pending_Appointment_Adapter.Pending_AppointmentValue(view);
    }

    public Pending_Appointment_Adapter() {

    }

    public void onBindViewHolder(@NonNull Pending_Appointment_Adapter.Pending_AppointmentValue holder, final int position) {

        String[] date = mainlist.get(position).getScreeningDate().split("-");
        holder.day.setText(date[0]);
        holder.month.setText(date[1]);
        holder.year.setText(date[2]);

        holder.pending_a_card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Pending_Appointment_Detail.class);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();

                String medical_Records = gson.toJson(mainlist.get(position).getMedicalRecords());
                intent.putExtra("completed_appointment_childkeys", completed_appointment_childkeys.get(position));
                intent.putExtra("patientName", mainlist.get(position).getPatientName());
                intent.putExtra("medical_Records", medical_Records);
                intent.putExtra("patientKey",mainlist.get(position).getPatientKey());
                intent.putExtra("address",mainlist.get(position).getAddress());
                intent.putExtra("checked",mainlist.get(position).isChecked());
                intent.putExtra("checkedDate",mainlist.get(position).getCheckedDate());
                intent.putExtra("haveCough",mainlist.get(position).isHaveCough());
                intent.putExtra("haveTravelHistory",mainlist.get(position).isHaveTravelHistory());
                intent.putExtra("haveTroubleBreathing",mainlist.get(position).isHaveTroubleBreathing());
                intent.putExtra("maritalStatus",mainlist.get(position).getMaritalStatus());
                intent.putExtra("medicalNotes",mainlist.get(position).getMedicalNotes());
                intent.putExtra("screeningDate",mainlist.get(position).getScreeningDate());
                intent.putExtra("temperature",mainlist.get(position).getTemperature());
                intent.putExtra("type",mainlist.get(position).getType());

                context.startActivity(intent);
            }
        });

        holder.edit_icon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Pending_Appointment_Detail.class);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();

                String medical_Records = gson.toJson(mainlist.get(position).getMedicalRecords());
                intent.putExtra("completed_appointment_childkeys", completed_appointment_childkeys.get(position));
                intent.putExtra("patientName", mainlist.get(position).getPatientName());
                intent.putExtra("medical_Records", medical_Records);
                intent.putExtra("patientKey",mainlist.get(position).getPatientKey());
                intent.putExtra("address",mainlist.get(position).getAddress());
                intent.putExtra("checked",mainlist.get(position).isChecked());
                intent.putExtra("checkedDate",mainlist.get(position).getCheckedDate());
                intent.putExtra("haveCough",mainlist.get(position).isHaveCough());
                intent.putExtra("haveTravelHistory",mainlist.get(position).isHaveTravelHistory());
                intent.putExtra("haveTroubleBreathing",mainlist.get(position).isHaveTroubleBreathing());
                intent.putExtra("maritalStatus",mainlist.get(position).getMaritalStatus());
                intent.putExtra("medicalNotes",mainlist.get(position).getMedicalNotes());
                intent.putExtra("screeningDate",mainlist.get(position).getScreeningDate());
                intent.putExtra("temperature",mainlist.get(position).getTemperature());
                intent.putExtra("type",mainlist.get(position).getType());

                context.startActivity(intent);
            }
        });

        holder.patient_key.setText(mainlist.get(position).getPatientName());

        holder.screening_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent screeningFiles = new Intent(v.getContext(), ScreeningFiles.class);
                screeningFiles.putExtra("childKeys", completed_appointment_childkeys.get(position));
                screeningFiles.putExtra("medicalScreeningStatus", "pending");
                screeningFiles.putExtra("patientKey", mainlist.get(position).getPatientKey());
                context.startActivity(screeningFiles);
            }
        });
    }

    public int getItemCount() {
        return mainlist.size();
    }

    public class Pending_AppointmentValue extends RecyclerView.ViewHolder {

        TextView patient_key;
        TextView alarms_time;
        CardView pending_a_card;
        TextView screening_file;
        TextView day;
        TextView month;
        TextView year;

        ImageView edit_icon;

        public Pending_AppointmentValue(@NonNull View itemView) {
            super(itemView);
            patient_key = itemView.findViewById(R.id.patient_key);
            alarms_time = itemView.findViewById(R.id.alarms_time);
            pending_a_card = itemView.findViewById(R.id.pending_a_card);
            edit_icon = itemView.findViewById(R.id.edit_icon);
            screening_file = itemView.findViewById(R.id.screening_file);
            day = itemView.findViewById(R.id.day);
            month = itemView.findViewById(R.id.month);
            year = itemView.findViewById(R.id.year);
        }
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_pending__appointment__adapter);
//    }
}
