package com.qslogics.slaj.Appointment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ortiz.touchview.TouchImageView;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.MedicalScreening.MedicalRecordAdapter;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MedicalRecord_Appointment_Adapter extends RecyclerView.Adapter<MedicalRecord_Appointment_Adapter.ImageValue> {

    ArrayList<MedicalRecordImage> mainlist =  new ArrayList<MedicalRecordImage>();
    Context context;
    ImageView removeIcon;

    public MedicalRecord_Appointment_Adapter (Context ct, ArrayList<MedicalRecordImage> list) {
        context = ct;
        mainlist = list;
    }

    @NonNull
    @Override
    public MedicalRecord_Appointment_Adapter.ImageValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_medical_record__appointment__adapter,parent,false);

        return new MedicalRecord_Appointment_Adapter.ImageValue(view);
    }

    public MedicalRecord_Appointment_Adapter() {

    }


    @Override
    public void onBindViewHolder(@NonNull MedicalRecord_Appointment_Adapter.ImageValue holder, final int position) {
        holder.rec_image_name.setText(mainlist.get(position).rec_image_name);
        Picasso.get().load(mainlist.get(position).rec_image_url).into(holder.rec_image_url);
        holder.rec_image_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog=new Dialog(v.getContext(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                dialog.setContentView(R.layout.dialog_image);
                TouchImageView bmImage = (TouchImageView) dialog.findViewById(R.id.img_receipt);
                bmImage.setImageDrawable(holder.rec_image_url.getDrawable());
                FloatingActionButton button= dialog.findViewById(R.id.button_dismiss);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.setCancelable(true);
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class ImageValue extends RecyclerView.ViewHolder {

        ImageView rec_image_url;
        TextView rec_image_name;
        ImageView remove_icon;

        public ImageValue(@NonNull View itemView) {
            super(itemView);
            rec_image_url = itemView.findViewById(R.id.rec_image);
            rec_image_name = itemView.findViewById(R.id.rec_name);
            remove_icon = itemView.findViewById(R.id.remove_icon);
        }
    }


}
