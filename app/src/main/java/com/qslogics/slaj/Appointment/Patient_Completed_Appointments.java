package com.qslogics.slaj.Appointment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.List;

public class Patient_Completed_Appointments extends Fragment {

    RecyclerView recyclerView;
    private DatabaseReference mDatabase;
    List<MedicalScreeningData> list_patient_completed_appointments;
    List<String> completed_appointment_childkeys;
    Query query2;
    String key;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_patient__completed__appointments, container, false);

        list_patient_completed_appointments = new ArrayList<>();

        completed_appointment_childkeys=new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");

        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        key = settings.getString("key", "key");
        query2 = mDatabase.orderByChild("patientKey").equalTo(key);


        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_patient_completed_appointments.clear();

                completed_appointment_childkeys.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    MedicalScreeningData medicalScreeningData = postSnapshot.getValue(MedicalScreeningData.class);
                    if(postSnapshot.child("checked").getValue().equals(true)){
                        list_patient_completed_appointments.add(medicalScreeningData);
                        completed_appointment_childkeys.add(postSnapshot.getKey());
                    }
                }

                recyclerView = rootView.findViewById(R.id.appointment_patient_completed_recycleview);


                final Patient_Continue_Appointment_Adapter patient_continue_appointment_adapter = new Patient_Continue_Appointment_Adapter(getContext(),list_patient_completed_appointments,completed_appointment_childkeys);
                recyclerView.setAdapter(patient_continue_appointment_adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }
}
