package com.qslogics.slaj.Patient;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class Patient_List_Adapter extends RecyclerView.Adapter<Patient_List_Adapter.PatientsValue> {

    ArrayList<Patient> before_filtered_list=new ArrayList<Patient>();
    ArrayList<Patient> mainlist=new ArrayList<Patient>();
    Context context;

    public Patient_List_Adapter(Context ct, ArrayList<Patient> list){
        context = ct;
        before_filtered_list = list;
    }
    public Patient_List_Adapter() {

    }

    @NonNull
    @Override
    public PatientsValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_patient__list__adapter,parent,false);
        return new PatientsValue(view);
    }

    public void filter(String searchText) {
        searchText = searchText.toLowerCase(Locale.getDefault());
        mainlist.clear();
        Log.d("result", String.valueOf(searchText.length()));
        if (searchText.length() == 0) {
            mainlist.addAll(before_filtered_list);
        }
        else
        {
            for (Patient me : before_filtered_list) {
                if (me.getName().toLowerCase(Locale.getDefault()).contains(searchText)) {
                    mainlist.add(me);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void onBindViewHolder(@NonNull PatientsValue holder, final int position) {
        holder.card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Patient_Detail.class);
                intent.putExtra("child_key",mainlist.get(position).getChild_key());
                intent.putExtra("patient_image_url",mainlist.get(position).getPatient_image_url());
                intent.putExtra("name",mainlist.get(position).getName());
                context.startActivity(intent);
            }
        });

        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Patient_Detail.class);
                intent.putExtra("child_key",mainlist.get(position).getChild_key());
                intent.putExtra("patient_image_url",mainlist.get(position).getPatient_image_url());
                intent.putExtra("name",mainlist.get(position).getName());
                context.startActivity(intent);
            }
        });

        holder.fullname.setText(mainlist.get(position).getName());

        holder.phnumber.setText(mainlist.get(position).getPatient_phone_numbers().get("ph_1"));

        if(mainlist.get(position).getEmail().length() <= 4){
            holder.gmail.setText("Not provided");
        }
        else{
            holder.gmail.setText(mainlist.get(position).getEmail());
        }
        Picasso.get().load(mainlist.get(position).getPatient_image_url()).into(holder.img_url);
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class PatientsValue extends RecyclerView.ViewHolder {

        ImageView img_url;
        TextView fullname;
        TextView phnumber;
        TextView gmail;
        RelativeLayout card;
        Button btn_detail;

        public PatientsValue(@NonNull View itemView) {
            super(itemView);
            card= itemView.findViewById(R.id.patient_card_layout);
            fullname = itemView.findViewById(R.id._title);
            phnumber = itemView.findViewById(R.id._phone);
            gmail = itemView.findViewById(R.id._email);
            img_url = itemView.findViewById(R.id._image);
            btn_detail = itemView.findViewById(R.id.btn_detail);
        }
    }

}
