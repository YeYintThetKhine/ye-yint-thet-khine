package com.qslogics.slaj.Patient;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class Patient_List extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Patient> list=new ArrayList<Patient>();
    private DatabaseReference mDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Patients");

        final View rootView = inflater.inflate(R.layout.fragment_patient__list,container,false);

        final SearchView searchValue = (SearchView )rootView.findViewById(R.id.search_patients);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Patient patient = postSnapshot.getValue(Patient.class);
                    list.add(patient);
                }
                recyclerView = rootView.findViewById(R.id._patientRecyclerView);

                final Patient_List_Adapter patient_List_Adapter = new Patient_List_Adapter(getContext(),list);
                recyclerView.setAdapter(patient_List_Adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setFocusable(false);
                patient_List_Adapter.filter("");

                searchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        String text = searchValue.getQuery().toString().toLowerCase(Locale.getDefault());
                        patient_List_Adapter.filter(text);
                        return false;
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }
}
