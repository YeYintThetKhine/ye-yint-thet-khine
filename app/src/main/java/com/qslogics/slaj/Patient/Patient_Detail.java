package com.qslogics.slaj.Patient;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

public class Patient_Detail extends AppCompatActivity {
    String child_key;
    private DatabaseReference mDatabase;
    ImageView imgBackArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient__detail);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);
        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onStart() {
        super.onStart();

        if(getIntent().hasExtra("name")) {
            child_key = getIntent().getStringExtra("child_key");
            mDatabase = FirebaseDatabase.getInstance().getReference().child("Patients");

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        final ImageView patient_image_url;
                        TextView name;
                        TextView age;
                        TextView gender;
                        TextView email;
                        TextView phone1;
                        TextView phone2;
                        TextView phone3;
                        TextView dob;
                        TextView address;

                        patient_image_url = findViewById(R.id.circularImageView_professional_profile);
                        name = findViewById(R.id.patient_profile_name);
                        age = findViewById(R.id.age_text);
                        gender = findViewById(R.id.gender_text);
                        email = findViewById(R.id.mail_text);
                        phone1 = findViewById(R.id.phoneno_1_patient_detail);
                        phone2 = findViewById(R.id.phoneno_2_patient_detail);
                        phone3 = findViewById(R.id.phoneno_3_patient_detail);
                        dob = findViewById(R.id.dob_text);
                        address = findViewById(R.id.address_text);

                        Picasso.get().load(dataSnapshot.child(child_key).child("patient_image_url").getValue().toString()).into(patient_image_url);
                        name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                        age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                        gender.setText(dataSnapshot.child(child_key).child("gender").getValue().toString());
                        email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                        phone1.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_1").getValue().toString());

                        if(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_2").exists()){
                            phone2.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_2").getValue().toString());
                        }
                        else{
                            phone2.setText("");
                        }
                        if(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_3").exists()){
                            phone3.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_3").getValue().toString());
                        }
                        else{
                            phone3.setText("");
                        }
                        dob.setText("24.3.1970");
                        address.setText(dataSnapshot.child(child_key).child("address").getValue().toString());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }
}
