package com.qslogics.slaj.MedicalScreening;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Entity.MO_Entity;
import com.qslogics.slaj.R;

import java.util.ArrayList;

public class MOListAdapter extends RecyclerView.Adapter<MOListAdapter.ViewHook> {
    ArrayList<MO_Entity> mainlist = new ArrayList<>();
    Context context;

    public MOListAdapter(Context ct, ArrayList<MO_Entity> list) {
        context = ct;
        mainlist = list;
    }

    @NonNull
    @Override
    public MOListAdapter.ViewHook onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.ms_mo_card,parent,false);

        return new MOListAdapter.ViewHook(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHook holder, int position) {
        holder.moName.setText(mainlist.get(position).getName());
        holder.moPhoneNumber.setText(mainlist.get(position).getMo_phone_numbers().get("ph_1"));
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class ViewHook extends RecyclerView.ViewHolder {

        TextView moName;
        TextView moPhoneNumber;

        public ViewHook(@NonNull View itemView) {
            super(itemView);
            moName = itemView.findViewById(R.id.mo_name);
            moPhoneNumber = itemView.findViewById(R.id.mo_phone_number);
        }
    }
}
