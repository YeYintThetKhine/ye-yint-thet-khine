package com.qslogics.slaj.MedicalScreening;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.R;

public class MedicalScreening extends Fragment {

    Button msMainContinue;
    DatabaseReference rootRef;
    DatabaseReference fbDatabaseRef_unassigned_appointment;
    Query filter_query;
    Patient patient;
    Boolean unassigned_appointment_check;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_medical_screening, container, false);
        rootRef = FirebaseDatabase.getInstance().getReference();
        msMainContinue = view.findViewById(R.id.ms_main_continue);

        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        final String key = settings.getString("key", "key");

        fbDatabaseRef_unassigned_appointment = FirebaseDatabase.getInstance().getReference().child("MedicalRecords");
        filter_query = fbDatabaseRef_unassigned_appointment.orderByChild("patientKey").equalTo(key);

        filter_query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    unassigned_appointment_check = true;
                }
                else{
                    unassigned_appointment_check = false;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        rootRef.child("Patients").child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                     patient = dataSnapshot.getValue(Patient.class);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        msMainContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(unassigned_appointment_check == true){
                    show_warning_message();
                }
                else{
                    if(patient.getHasAppointment()){
                        show_warning_message();
                    }
                    else{
                        Intent i = new Intent(view.getContext(), PatientInfoOne.class);
                        i.putExtra("key", key);
                        i.putExtra("name", patient.getName());
                        i.putExtra("dob", patient.getDob());
                        i.putExtra("gender", patient.getGender());
                        i.putExtra("phone", patient.getPatient_phone_numbers().get("ph_1"));
                        i.putExtra("email", patient.getEmail());
                        i.putExtra("address", patient.getAddress());
                        startActivity(i);
                        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    }
                }
            }
        });

        return view;
    }

    public void show_warning_message(){
        new AlertDialog.Builder(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                .setTitle(R.string.medical_screening_have_appointment_message_title)
                .setMessage(R.string.medical_screening_have_appointment_message)
                .setCancelable(false)
                .setPositiveButton(R.string.sama_img_no_img_dialog_ok_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

}
