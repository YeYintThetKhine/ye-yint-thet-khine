package com.qslogics.slaj.MedicalScreening;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.MO_Entity;
import com.qslogics.slaj.Home.MainActivity;
import com.qslogics.slaj.R;

import java.util.ArrayList;

public class ContactMo extends AppCompatActivity {

    RecyclerView moListRecyclerView;
    Button cancelButton;
    Button doneButton;
    ArrayList<MO_Entity> moListData = new ArrayList<>();
    DatabaseReference ref;
    boolean fromPatientAppointment;
    boolean fromCompleteScreening;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_contact_mo);

        moListRecyclerView = findViewById(R.id.mo_list_recycler_view);
        ref = FirebaseDatabase.getInstance().getReference().child("MO");
        doneButton = findViewById(R.id.done_btn);

        /*Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fromPatientAppointment = extras.getBoolean("fromPatientAppointment", false);
            fromCompleteScreening = extras.getBoolean("fromCompleteScreening", false);
        }*/

        Intent i = getIntent();
        if(i != null) {
            fromPatientAppointment = i.getBooleanExtra("fromPatientAppointment", false);
            fromCompleteScreening = i.getBooleanExtra("fromCompleteScreening", false);
            //OK
        }

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fromPatientAppointment) {
                    finish();
                }
                if(fromCompleteScreening) {
                    finish();
                    Intent it= new Intent(ContactMo.this, MainActivity.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    it.putExtra("fromCompleteScreening", true);
                    startActivity(it);
                } else {
                    System.out.println("fromMedicalScreeningSS");
                    finish();
                    Intent it= new Intent(ContactMo.this, MainActivity.class);
                    it.putExtra("fromMedicalScreening", true);
                    startActivity(it);
                }
            }
        });

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                moListData.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    MO_Entity mo = postSnapshot.getValue(MO_Entity.class);
                    moListData.add(mo);
                }
                moListRecyclerView = findViewById(R.id.mo_list_recycler_view);

                final MOListAdapter mo_list_adapter = new MOListAdapter(ContactMo.this,moListData);
                moListRecyclerView.setAdapter(mo_list_adapter);
                moListRecyclerView.setHasFixedSize(true);
                moListRecyclerView.setLayoutManager(new LinearLayoutManager(ContactMo.this, LinearLayoutManager.VERTICAL,false));
                moListRecyclerView.setFocusable(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}
