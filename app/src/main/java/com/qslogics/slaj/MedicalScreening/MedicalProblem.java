package com.qslogics.slaj.MedicalScreening;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.qslogics.slaj.R;

import java.util.HashMap;

public class MedicalProblem extends AppCompatActivity {

    Button continueMedicalProblem;
    HashMap<String, String> medicalScreening = new HashMap<>();
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_medical_problem);

        //get intent from another activity
        Intent intent = getIntent();
        medicalScreening = (HashMap<String, String>)intent.getSerializableExtra("medicalScreening");

        continueMedicalProblem = findViewById(R.id.continue_medical_problem);
        cancel = findViewById(R.id.ms_cancel_button4);
        continueMedicalProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MedicalProblem.this, ProblemOne.class);
                intent.putExtra("medicalScreening", medicalScreening);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
