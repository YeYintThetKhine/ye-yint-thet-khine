package com.qslogics.slaj.MedicalScreening;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.qslogics.slaj.R;

import java.util.HashMap;
import java.util.Map;

public class CovidCheckUp extends AppCompatActivity {

    HashMap<String, String> medicalScreening = new HashMap<>();
    Button checkupContinue;
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_covid_checkup);
        checkupContinue = findViewById(R.id.checkup_continue);
        cancel = findViewById(R.id.ms_cancel_button3);

        Intent intent = getIntent();
        medicalScreening = (HashMap<String, String>)intent.getSerializableExtra("medicalScreening");

        for(Map.Entry<String, String> entry : medicalScreening.entrySet()) {
            System.out.println(entry.getKey());
        }
        checkupContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(CovidCheckUp.this, CheckUpZero.class);
                intent.putExtra("medicalScreening", medicalScreening);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
