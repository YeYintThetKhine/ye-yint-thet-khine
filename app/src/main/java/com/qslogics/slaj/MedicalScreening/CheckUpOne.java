package com.qslogics.slaj.MedicalScreening;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.qslogics.slaj.R;

import java.util.HashMap;

public class CheckUpOne extends AppCompatActivity {

    Button checkOneNext;
    HashMap<String, String> medicalScreening = new HashMap<>();
    RadioGroup rGroup;
    String haveCough;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_check_1);

        rGroup = findViewById(R.id.radioGroup);

        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = findViewById(checkedId);
                if(rb.getText().equals("Yes")) {
                    haveCough = "1";
                }

                if(rb.getText().equals("No")) {
                    haveCough = "0";
                }
            }
        });

        //get intent from another activity
        Intent intent = getIntent();
        medicalScreening = (HashMap<String, String>)intent.getSerializableExtra("medicalScreening");

        checkOneNext = findViewById(R.id.check1_next);
        checkOneNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rGroup.getCheckedRadioButtonId() != -1){
                    finish();
                    medicalScreening.put("haveCough", haveCough);
                    Intent intent = new Intent(CheckUpOne.this, CheckUpTwo.class);
                    intent.putExtra("medicalScreening", medicalScreening);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }
                else{
                    Toast toast = Toast.makeText(CheckUpOne.this,getString(R.string.med_screening_validation), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 400);
                    toast.show();
                }
            }
        });
    }
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
