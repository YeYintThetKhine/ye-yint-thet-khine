package com.qslogics.slaj.MedicalScreening;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.qslogics.slaj.Home.MainActivity;
import com.qslogics.slaj.R;

import java.util.HashMap;

public class PatientInfoOne extends AppCompatActivity {

    HashMap<String, String> medicalScreening = new HashMap<>();
    Button cancel;
    Button continuePatientInfoOne;

    TextView fullname;
    TextView dob;
    TextView gender;
    TextView phone;
    TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_patient_info_1);

        cancel = findViewById(R.id.ms_cancel_button);
        continuePatientInfoOne = findViewById(R.id.continue_patient_info_1);
        fullname = findViewById(R.id.name_data);
        dob = findViewById(R.id.dob_data);
        gender = findViewById(R.id.gender_data);
        phone = findViewById(R.id.phone_data);
        email = findViewById(R.id.email_data);

        fullname.setText(getIntent().getStringExtra("name"));
        dob.setText(getIntent().getStringExtra("dob"));
        gender.setText(getIntent().getStringExtra("gender"));
        phone.setText(getIntent().getStringExtra("phone"));
        email.setText(getIntent().getStringExtra("email"));

        continuePatientInfoOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                medicalScreening.put("patientKey", getIntent().getStringExtra("key"));
                medicalScreening.put("address", getIntent().getStringExtra("address"));
                Intent intent = new Intent(PatientInfoOne.this, PatientInfoTwo.class);
                intent.putExtra("medicalScreening", medicalScreening);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
