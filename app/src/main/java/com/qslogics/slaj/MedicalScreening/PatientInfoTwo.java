package com.qslogics.slaj.MedicalScreening;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.ktx.Firebase;
import com.google.firebase.storage.StorageReference;
import com.qslogics.slaj.R;

import java.util.HashMap;

public class PatientInfoTwo extends AppCompatActivity {

    HashMap<String, String> medicalScreening = new HashMap<>();
    Button continuePatientInfoTwo;
    Button cancel;
    RadioButton yes;
    RadioButton no;
    //RadioGroup rGroup;
    TextView address;
    String maritalStatus;
    String haveTravelHistory;

    SharedPreferences settings;
    String child_key;
    DatabaseReference mdbReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_patient_info_2);
        continuePatientInfoTwo = findViewById(R.id.continue_patient_info_2);
        //rGroup = findViewById(R.id.radioGroup);
        address = findViewById(R.id.address_data);
        cancel = findViewById(R.id.ms_cancel_button);

        //get intent from another activity
        Intent intent = getIntent();
        medicalScreening = (HashMap<String, String>)intent.getSerializableExtra("medicalScreening");


        settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        child_key =settings.getString("key", "child_key");

        mdbReference = FirebaseDatabase.getInstance().getReference().child("Patients").child(child_key).child("address");
        mdbReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                address.setText(dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//        address.setText(intent.getStringExtra("address"));

/*        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = findViewById(checkedId);
                if(rb.getText().equals("Yes")) {
                    haveTravelHistory = "1";
                }

                if(rb.getText().equals("No")) {
                    haveTravelHistory = "0";
                }
            }
        });

 */

        final Spinner spinnerMaritalStatus = findViewById(R.id.marital_spinner); //android.R.layout.simple_spinner_item
        ArrayAdapter<CharSequence> spinnerMaritalAdapter = ArrayAdapter.createFromResource(this,R.array.maritalStatus, R.layout.spinner_item_gender);
        spinnerMaritalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMaritalStatus.setAdapter(spinnerMaritalAdapter);
        spinnerMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maritalStatus = parent.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        continuePatientInfoTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(1 == 1){
                    finish();
                    medicalScreening.put("maritalStatus", maritalStatus);
                    medicalScreening.put("address", address.getText().toString());
                    ///medicalScreening.put("haveTravelHistory", haveTravelHistory);
                    Intent intent = new Intent(PatientInfoTwo.this, CovidCheckUp.class);
                    intent.putExtra("medicalScreening", medicalScreening);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }
                else{
                    Toast toast = Toast.makeText(PatientInfoTwo.this,getString(R.string.travel_history_validation), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 400);
                    toast.show();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
