package com.qslogics.slaj.MedicalScreening;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Appointment.Pending_Appointment_Detail;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MedicalRecordAdapter extends RecyclerView.Adapter<MedicalRecordAdapter.ImageValue>{

    ArrayList<MedicalRecordImage> mainlist =  new ArrayList<MedicalRecordImage>();
    Context context;
    ImageView removeIcon;

    public MedicalRecordAdapter() {

    }

    public MedicalRecordAdapter (Context ct, ArrayList<MedicalRecordImage> list) {
        context = ct;
        mainlist = list;
    }

    @NonNull
    @Override
    public MedicalRecordAdapter.ImageValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.ms_md_adapter,parent,false);

        return new MedicalRecordAdapter.ImageValue(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicalRecordAdapter.ImageValue holder, final int position) {
        holder.rec_image_name.setText(mainlist.get(position).rec_image_name);
        Picasso.get().load(mainlist.get(position).rec_image_url).fit().into(holder.rec_image_url);

        holder.remove_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
                ((ProblemOne)context).onMethodCallback(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class ImageValue extends RecyclerView.ViewHolder {

        ImageView rec_image_url;
        TextView rec_image_name;
        ImageView remove_icon;

        public ImageValue(@NonNull View itemView) {
            super(itemView);
            rec_image_url = itemView.findViewById(R.id.rec_image);
            rec_image_name = itemView.findViewById(R.id.rec_name);
            remove_icon = itemView.findViewById(R.id.remove_icon);
        }
    }


    public void removeAt(int position) {
        mainlist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mainlist.size());
    }

}
