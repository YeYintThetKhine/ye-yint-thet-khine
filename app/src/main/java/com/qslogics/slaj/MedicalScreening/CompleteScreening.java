package com.qslogics.slaj.MedicalScreening;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.qslogics.slaj.R;

import java.util.HashMap;

public class CompleteScreening extends AppCompatActivity {

    Button completeCheckUp;
    HashMap<String, String> medicalScreening = new HashMap<>();
    DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_completed_checkup);
        completeCheckUp = findViewById(R.id.checkup_continue);
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        String key = settings.getString("key", "key");

        ref = FirebaseDatabase.getInstance().getReference().child("Patients").child(key).child("hasAppointment");
        ref.setValue(true);
        //get intent from another activity
        Intent intent = getIntent();
        medicalScreening = (HashMap<String, String>)intent.getSerializableExtra("medicalScreening");

        completeCheckUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(CompleteScreening.this, ContactMo.class);
                intent.putExtra("fromCompleteScreening", true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

    }
}
