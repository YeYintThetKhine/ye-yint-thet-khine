package com.qslogics.slaj.MedicalScreening;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.qslogics.slaj.Appointment.Pending_Appointment_Detail;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ProblemOne extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    RecyclerView recyclerView;
    Button continueMedicalProblemOne;
    Button choosePhotoBtn;
    HashMap<String, String> medicalScreening = new HashMap<>();
    EditText mNotes;
    DatabaseReference ref;
    DatabaseReference dataRef;
    String type;
    ArrayList<MedicalRecordImage> list = new ArrayList<>();
    ArrayList<MedicalRecordImage> listForUpload = new ArrayList<>();
    ArrayList<Uri> ImageList = new ArrayList<>();
    private Uri ImageUri;
    private int upload_count = 0;
    RadioGroup rGroup;
    Date c = Calendar.getInstance().getTime();
    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
    String date = df.format(c);
    String replacedate = date.replace(".","");

    boolean haveCough;
    boolean haveTroubleBreathing;
    boolean haveTravelHistory;
    boolean havefever; //yy

    ArrayList<MedicalRecordImage> final_med_record_list = new ArrayList<>();
    ArrayList<MedicalRecordImage> final_med_report_list = new ArrayList<>();

    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    public Uri imguri;
    Uri downloadUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_problem_1);

        mNotes = findViewById(R.id.medical_problem_notes);
        mNotes.setMovementMethod(new ScrollingMovementMethod());
        recyclerView = findViewById(R.id.mrecords);
        choosePhotoBtn = findViewById(R.id.photo_btn);
        rGroup = findViewById(R.id.radioGroup);

        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        final String name = settings.getString("name", "name");

        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = findViewById(checkedId);
                if(rb.getText().equals("Self")) {
                    type = "Self";
                }

                if(rb.getText().equals("Behalf")) {
                    type = "Behalf";
                }
            }
        });



        //get intent from another activity
        Intent intent = getIntent();
        medicalScreening = (HashMap<String, String>)intent.getSerializableExtra("medicalScreening");
        System.out.println(medicalScreening.get("patientKey"));
        System.out.println(date);
        ref = FirebaseDatabase.getInstance().getReference().child("MedicalRecordImages").child(medicalScreening.get("patientKey")).child(replacedate);
        dataRef = FirebaseDatabase.getInstance().getReference().child("MedicalRecords");



        continueMedicalProblemOne = findViewById(R.id.continue_medical_problem_one);

        continueMedicalProblemOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(medicalScreening.get("haveCough").equals("1")) {
                    haveCough = true;
                } else {
                    haveCough = false;
                }

                if(medicalScreening.get("haveTroubleBreathing").equals("1")) {
                    haveTroubleBreathing = true;
                } else {
                    haveTroubleBreathing = false;
                }
                if(medicalScreening.get("haveTravelHistory").equals("1")) {
                    haveTravelHistory = true;
                } else {
                    haveTravelHistory = true;
                }
                if(ImageList.size() > 0){
                    loading_dialog_show();
                    if(rGroup.getCheckedRadioButtonId() != -1 && mNotes.getText().length() > 0){
                        continueMedicalProblemOne.setEnabled(false);
                        continueMedicalProblemOne.setAlpha(0.5f);
                        StorageReference imageFolder = FirebaseStorage.getInstance().getReference().child("MedicalRecords").child(medicalScreening.get("patientKey")).child(date);
                        for (upload_count = 0; upload_count < ImageList.size(); upload_count++) {
                            final String img_name = "mRec"+(upload_count+1);
                            final StorageReference Ref = imageFolder.child("mRec" + (upload_count + 1));
                            uploadTask = Ref.putFile(ImageList.get(upload_count));
                            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }
                                    return Ref.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (task.isSuccessful()) {

                                        downloadUri = task.getResult();
                                        //Toast.makeText(Pending_Appointment_Detail.this ,"Image Uploaded successfully", Toast.LENGTH_LONG).show();
                                        MedicalRecordImage med_report_data = new MedicalRecordImage(img_name,downloadUri.toString());
                                        final_med_record_list.add(med_report_data);
                                        if( upload_count == final_med_record_list.size()) {
                                            MedicalRecordImage medicalReport = new MedicalRecordImage("No Med Report is provided for now","No Med Report is provided for now");
                                            final_med_report_list.add(medicalReport);
                                            MedicalScreeningData data = new MedicalScreeningData(medicalScreening.get("patientKey"), name, "", "","",
                                                    "", medicalScreening.get("maritalStatus"), medicalScreening.get("address"),
                                                    medicalScreening.get("temperature"), mNotes.getText().toString(), haveCough,
                                                    haveTroubleBreathing, haveTravelHistory, false, date, "",
                                                    final_med_record_list, type, "", "", final_med_report_list);
                                            dataRef.push().setValue(data);


                                            finish();
                                            Intent intent = new Intent(ProblemOne.this, CompleteScreening.class);
                                            intent.putExtra("medicalScreening", medicalScreening);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                        }

                                    } else {
                                        MedicalRecordImage medicalReport = new MedicalRecordImage("No Med Report is provided for now","No Med Report is provided for now");
                                        final_med_report_list.add(medicalReport);
                                        MedicalScreeningData data = new MedicalScreeningData(medicalScreening.get("patientKey"), name, "", "","",
                                                "", medicalScreening.get("maritalStatus"), medicalScreening.get("address"),
                                                medicalScreening.get("temperature"), mNotes.getText().toString(), haveCough,
                                                haveTroubleBreathing, haveTravelHistory, false, date, "",
                                                final_med_record_list, type, "", "", final_med_report_list);
                                        dataRef.push().setValue(data);


                                        finish();
                                        Intent intent = new Intent(ProblemOne.this, CompleteScreening.class);
                                        intent.putExtra("medicalScreening", medicalScreening);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                    }
                                }
                            });
                        }
                    }
                    else{
                        Toast toast = Toast.makeText(ProblemOne.this,getString(R.string.med_screening_validation), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 400);
                        toast.show();
                    }

                }
                else{
                    if(rGroup.getCheckedRadioButtonId() != -1 && mNotes.getText().length() > 0){
                        loading_dialog_show();
                        continueMedicalProblemOne.setEnabled(false);
                        continueMedicalProblemOne.setAlpha(0.5f);
                        MedicalRecordImage medicalRecord = new MedicalRecordImage("No Med Record is provided for now","No Med Record is provided for now");
                        final_med_record_list.add(medicalRecord);
                        MedicalRecordImage medicalReport = new MedicalRecordImage("No Med Report is provided for now","No Med Report is provided for now");
                        final_med_report_list.add(medicalReport);
                        MedicalScreeningData data = new MedicalScreeningData(medicalScreening.get("patientKey"), name, "", "","",
                                "", medicalScreening.get("maritalStatus"), medicalScreening.get("address"),
                                medicalScreening.get("temperature"), mNotes.getText().toString(), haveCough,
                                haveTroubleBreathing, haveTravelHistory, false, date, "",
                                final_med_record_list, type, "", "", final_med_report_list);
                        dataRef.push().setValue(data);


                        finish();
                        Intent intent = new Intent(ProblemOne.this, CompleteScreening.class);
                        intent.putExtra("medicalScreening", medicalScreening);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

                    }
                    else{
                        Toast toast = Toast.makeText(ProblemOne.this,getString(R.string.med_screening_validation), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 400);
                        toast.show();
                    }
                }
               // MedicalScreeningData data = new MedicalScreeningData(medicalScreening.get("patientKey"), "", "", medicalScreening.get("maritalStatus"), medicalScreening.get("address"), medicalScreening.get("temperature"), mNotes.getText().toString(), haveCough,  haveTroubleBreathing, haveTravelHistory, false, date, "", list, type, "", "",final_med_report_list);
              //  dataRef.push().setValue(data);

            }
        });

        choosePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent itImage = new Intent(Intent.ACTION_GET_CONTENT);

                Intent itImage = new Intent(Intent.ACTION_GET_CONTENT,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                itImage.setType("image/*");
                itImage.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(itImage, PICK_IMAGE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE) {
            if(resultCode == RESULT_OK) {
                if(data.getClipData() != null) {
                    System.out.println(data.getClipData());

                    int countClipData = data.getClipData().getItemCount();

                    int currentImageSelect = 0;

                    while(currentImageSelect < countClipData) {
                        String name = "mRec" + (currentImageSelect+1);
                        ImageUri = data.getClipData().getItemAt(currentImageSelect).getUri();
                        MedicalRecordImage mrec = new MedicalRecordImage(name, String.valueOf(ImageUri));
                        list.add(mrec);
                        ImageList.add(ImageUri);
                        currentImageSelect = currentImageSelect + 1;
                    }
                } else {
                    int currentImageSelect = 0;
                    String name = "mRec" + (currentImageSelect+1);
                    ImageUri = data.getData();
                    MedicalRecordImage mrec = new MedicalRecordImage(name, String.valueOf(ImageUri));
                    list.add(mrec);
                    ImageList.add(ImageUri);
//                    Toast.makeText(this, "Please select multiple images.", Toast.LENGTH_SHORT).show();
                }

                recyclerView = findViewById(R.id.mrecords);

                MedicalRecordAdapter adapter = new MedicalRecordAdapter(ProblemOne.this,list);
                recyclerView.setAdapter(adapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(ProblemOne.this,LinearLayoutManager.HORIZONTAL,false));
            }
        }
    }

    public void uploadImages () {
        //upload

    }
    private void StoreLink(String name, String url) {
        //DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("UserOne");
        //databaseReference.push().setValue(hashMap);
        MedicalRecordImage mri = new MedicalRecordImage(name, url);
        listForUpload.add(mri);
    }

    public void loading_dialog_show(){
        final Dialog dialog = new Dialog(ProblemOne.this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
        dialog.setContentView(R.layout.appointment_edit_loading_alert_dialog);
        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.video_call_btn);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void onMethodCallback(int position) {
        ImageList.remove(position);
    }


}
