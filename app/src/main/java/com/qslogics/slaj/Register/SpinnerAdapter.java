package com.qslogics.slaj.Register;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.qslogics.slaj.R;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<SpinnerItem> {
    public SpinnerAdapter(Context context, ArrayList<SpinnerItem> spinnerItemList) {
        super(context, 0, spinnerItemList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // (old) return super.getView(position, convertView, parent);
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // (old) return super.getDropDownView(position, convertView, parent);
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.image_spinner_row, parent, false
            );
        }

        ImageView spinnerImage = convertView.findViewById(R.id.spinner_image);
        TextView spinnerImageName = convertView.findViewById(R.id.spinner_imgname);

        SpinnerItem currentItem = getItem(position);

        if(currentItem != null) {
            spinnerImage.setImageResource(currentItem.getTypeImage());
            spinnerImageName.setText(currentItem.getTypeName());
        }
        return convertView;
    }
}
