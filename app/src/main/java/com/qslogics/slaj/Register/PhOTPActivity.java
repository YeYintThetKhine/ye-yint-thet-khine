package com.qslogics.slaj.Register;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.Entity.User;
import com.qslogics.slaj.Home.MainActivity;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PhOTPActivity extends AppCompatActivity {
    private String verificationid;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private EditText editText;
    private EditText otpfield_1;
    private EditText otpfield_2;
    private EditText otpfield_3;
    private EditText otpfield_4;
    private EditText otpfield_5;
    private EditText otpfield_6;
    private DatabaseReference rootRef;
    String phonenumber;
    HashMap<String, String > stringMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stringMap.put("data", "Not Provided Yet.");
        rootRef = FirebaseDatabase.getInstance().getReference();

        setContentView(R.layout.activity_ph_o_t_p);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mAuth = FirebaseAuth.getInstance();
        FirebaseAuth.getInstance().signOut();

        progressBar = findViewById(R.id.progressbar);
//        editText = findViewById(R.id.editTextCode);
        otpfield_1 = findViewById(R.id.otpfield_1);
        otpfield_2 = findViewById(R.id.otpfield_2);
        otpfield_3 = findViewById(R.id.otpfield_3);
        otpfield_4 = findViewById(R.id.otpfield_4);
        otpfield_5 = findViewById(R.id.otpfield_5);
        otpfield_6 = findViewById(R.id.otpfield_6);

        otpfield_1.addTextChangedListener( new TextWatcher () {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                    otpfield_2.requestFocus();
            }
        });
        otpfield_2.addTextChangedListener( new TextWatcher () {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                    otpfield_3.requestFocus();
            }
        });
        otpfield_3.addTextChangedListener( new TextWatcher () {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                    otpfield_4.requestFocus();
            }
        });
        otpfield_4.addTextChangedListener( new TextWatcher () {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                    otpfield_5.requestFocus();
            }
        });
        otpfield_5.addTextChangedListener( new TextWatcher () {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                    otpfield_6.requestFocus();
            }
        });


        phonenumber = getIntent().getStringExtra("phonenumber");
        sendVerificationCode(phonenumber);

        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String code = editText.getText().toString().trim();

                String code = otpfield_1.getText().toString().trim()+otpfield_2.getText().toString().trim()+otpfield_3.getText().toString().trim()+otpfield_4.getText().toString().trim()+otpfield_5.getText().toString().trim()+otpfield_6.getText().toString().trim();

                if ((code.isEmpty() || code.length() < 6)){

                    editText.setError("Enter code...");
                    editText.requestFocus();
                    return;
                }
                verifyCode(code);

            }
        });
    }

    private void verifyCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationid, code);
        signInWithCredential(credential);
    }


    public void storePreferences(User user, String key) {
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("name", user.getFullname());
        editor.putString("type", user.getUsertype());
        editor.putString("key", key);
        editor.commit();
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        final HashMap<String, String> phoneNumbersMap = new HashMap<>();
        phoneNumbersMap.put("ph_1", phonenumber);

        final HashMap<String, String> mrecordMap = new HashMap<>();
        mrecordMap.put("mr_1", "Medical records not provided yet.");

        final ArrayList<String> availableTimeMap = new ArrayList<String>();
        availableTimeMap.add("Available times not provided.");

        final HashMap<String, String> specializationMap = new HashMap<>();
        specializationMap.put("sp_1", "Specializations not provided.");

        final HashMap<String, String> vDocumentsMap = new HashMap<>();
        vDocumentsMap.put("vd_1", "No documents yet.");
        //placeholder image urls
        final String patient_male= "https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fmale_patient.jpg?alt=media&token=dab38ccc-abcd-47cb-9df2-054246e50a8a";
        final String patient_female="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Ffemale_patient.jpg?alt=media&token=a602cdaf-9ea9-436a-8e8b-d48d17ba2a2d";

        final String professional_male="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2FDoctor2.jpg?alt=media&token=2d9ed2be-c09d-41d5-ba5a-886e58f76d31";
        final String professional_female="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fdoctor1.jpg?alt=media&token=1ba98190-b469-4ed2-8325-96bb4fc5ce20";

        final String caregiver_male="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fcaregiver.jpg?alt=media&token=29d264c6-f15d-4e93-8c94-b4e87dfcc270";
        final String caregiver_female = "https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fcaregiver2.jpg?alt=media&token=47bf330a-e29b-482d-8bbe-96c00d29601c";
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

//                            Intent intent = new Intent(PhOTPActivity.this, MainActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//                            startActivity(intent);

                            rootRef.child("users").child("phRegisteredUsers").child(phonenumber).addValueEventListener(new ValueEventListener() {

                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String verified_check = dataSnapshot.child("verified").getValue().toString();
                                    User userObject = dataSnapshot.getValue(User.class);
                                    if(verified_check == "false") {
                                        rootRef.child("users").child("phRegisteredUsers").child(phonenumber).child("verified").setValue(true);
                                        switch(userObject.getUsertype()) {
                                            case "Patient":
                                                specializationMap.put("sp_1", "General");
//                                                String uniqueID = UUID.randomUUID().toString();
//                                                String[] uuidArr = uniqueID.split("-");
                                                long timestampmilisec = System.currentTimeMillis();
                                                String str_timestampmilisec = String.valueOf(timestampmilisec);
                                                Patient patient = new Patient(userObject.getFullname(),"Enter Email", userObject.getGender(), userObject.getAge(), userObject.getUsertype(), userObject.getGender().equals("Male") ? (patient_male):(patient_female), "", true, false, phoneNumbersMap, mrecordMap, phonenumber, str_timestampmilisec, "");
                                                System.out.println(patient);
                                                FirebaseDatabase.getInstance().getReference().child("Patients").child(phonenumber).setValue(patient);
                                                break;
                                            case "Professional":
                                                specializationMap.put("sp_1", "General");
                                                Professional professional = new Professional(userObject.getFullname(), "Enter Email", userObject.getGender(), userObject.getAge(), userObject.getUsertype(), "", "Not provided yet.", "", userObject.getGender().equals("Male") ? (professional_male):(professional_female), true, phoneNumbersMap, availableTimeMap, vDocumentsMap, "Specialization was not choosen", phonenumber);
                                                FirebaseDatabase.getInstance().getReference().child("Professionals").child(phonenumber).setValue(professional);
                                                break;
                                            case "Caregiver":
                                                specializationMap.put("sp_1", "General");
                                                Caregiver caregiver = new Caregiver(userObject.getFullname(), "Enter Email", userObject.getGender(), userObject.getAge(), userObject.getUsertype(), "",userObject.getGender().equals("Male") ? (caregiver_male):(caregiver_female), userObject.getGender().equals("Male") ? (caregiver_male):(caregiver_female), false, phoneNumbersMap, phonenumber);
                                                FirebaseDatabase.getInstance().getReference().child("Caregivers").child(phonenumber).setValue(caregiver);
                                                break;
                                        }
                                        storePreferences(userObject, phonenumber);
                                        Intent intent = new Intent(PhOTPActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                    else {
                                        switch(userObject.getUsertype()) {
                                            case "Patient":
                                                FirebaseDatabase.getInstance().getReference().child("Patients").child(phonenumber).child("active").setValue(true);
                                                break;
                                            case "Professional":
                                                FirebaseDatabase.getInstance().getReference().child("Professionals").child(phonenumber).child("active").setValue(true);
                                                break;
                                            case "Caregivers":
                                                FirebaseDatabase.getInstance().getReference().child("Caregivers").child(phonenumber).child("active").setValue(true);
                                                break;
                                        }
                                        storePreferences(userObject, phonenumber);
                                        Intent intent = new Intent(PhOTPActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });


                        } else {
                            Toast.makeText(PhOTPActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                });
    }

    private void sendVerificationCode(String number){
        Log.d("PHONE_CODE_SEND",number);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                this,
                mCallBack
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationid = s;
            Log.d("code_sent","passed here 1");
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.d("code_real","passed here 2");
            if (code != null){
                progressBar.setVisibility(View.VISIBLE);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(PhOTPActivity.this, e.getMessage(),Toast.LENGTH_LONG).show();

        }
    };
}