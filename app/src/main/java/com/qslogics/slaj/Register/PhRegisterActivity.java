package com.qslogics.slaj.Register;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.User;
import com.qslogics.slaj.Login.Login;
import com.qslogics.slaj.Login.Login_phone;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PhRegisterActivity extends AppCompatActivity {

    TextView fullname;
    String gender;
    TextView age;
    TextView password;
    TextView confirmPassword;
    String usertype;
    String timestamp;
    String phonenumber;
    TextView loginLabel;

    private Spinner spinner;
    private EditText editText;
    private ArrayList<SpinnerItem> userTypeList;
    private SpinnerAdapter adapter;
    DatabaseReference Database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ph_register);

        //Spinner ph postal code
        spinner = findViewById(R.id.spinnerCountries);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CountryData.countryNames));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#72bb53"));/* if you want your item to be white */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        editText = findViewById(R.id.editTextPhone);

        fullname = findViewById(R.id.fullname_text);
        age = findViewById(R.id.age_text);
        password = findViewById(R.id.login_password_text);
        confirmPassword = findViewById(R.id.confirm_password_text);
        loginLabel = findViewById(R.id.login_label);
        final HashMap<String, String > stringMap = new HashMap<>();
        stringMap.put("data", "Not Provided Yet.");

        Database = FirebaseDatabase.getInstance().getReference();

        final Spinner spinnerGender = findViewById(R.id.gender_spinner);
        ///spinnerGender.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Gender.Gendertype));
        ArrayAdapter<CharSequence> spinnerGenderAdapter = ArrayAdapter.createFromResource(this,R.array.gender, R.layout.spinner_item_gender);
        spinnerGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(spinnerGenderAdapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#72bb53")); /* if you want your item to be white */
                gender = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //Spinner UserTypes
        initList();
        Spinner spinnerUserTypes = findViewById(R.id.spinner_usertypes_register);
        adapter = new SpinnerAdapter(this,userTypeList);
        spinnerUserTypes.setAdapter(adapter);
        spinnerUserTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItem clickedItem = (SpinnerItem) parent.getItemAtPosition(position);
                usertype = clickedItem.getTypeName();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = CountryData.countryAreaCodes[spinner.getSelectedItemPosition()];

                String number = editText.getText().toString().trim();
                phonenumber = "+" + code + number;

                if (number.isEmpty() || (number.length () < 8 && number.length() > 15)) {
                    editText.setError("Valid number is required");
                    editText.requestFocus();
                    return;
                }
                else {
                    Database= FirebaseDatabase.getInstance().getReference().child("users").child("phRegisteredUsers").child(phonenumber);
                    Database.addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                new AlertDialog.Builder(PhRegisterActivity.this)
                                        .setTitle("Ph number already existed!")
                                        .setMessage(phonenumber + " is already registered in our user list. Please try and register using another phone number")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).show();
                                Log.d("no data","There is no data");
                            }
                            else{
                                Date date= new Date();
                                long time = date.getTime();
                                timestamp = String.valueOf(time);
                                String Gender = "";
                                String UserType = "";
                                String FullName = fullname.getText().toString().trim();
                                if(gender.equals("ကျား")){
                                    Gender = "Male";
                                }
                                else if(gender.equals("မ")){
                                    Gender = "Female";
                                }
                                else{
                                    Gender = gender;
                                }
                                int Age = !age.getText().toString().trim().equals("")  ? Integer.parseInt(age.getText().toString().trim()) : 0;
                                if(usertype.equals("လူနာများ")){
                                    UserType = "Patient";
                                }
                                else if(usertype.equals("ဆရာဝန်")){
                                    UserType = "Professional";
                                }
                                else if(usertype.equals("ဝန်ဆောင်မှုများ")){
                                    UserType = "Caregiver";
                                }
                                else{
                                    UserType = usertype;
                                }
                                String Email = "null";
                                String PhNumber = phonenumber;

                                User user = new User(FullName, UserType, Gender, Age, Email ,PhNumber, false, timestamp);
                                FirebaseDatabase.getInstance().getReference().child("users").child("phRegisteredUsers").child(PhNumber).setValue(user);

                                //verified data saving
                                /*switch(UserType) {
                                    case "Patient":
                                        Patient patient = new Patient(FullName, Email, Gender, Age, UserType, "", "", false, false, stringMap, stringMap);
                                        FirebaseDatabase.getInstance().getReference().child("Patients").child(PhNumber).setValue(patient);
                                        break;
                                    case "Professional":
                                        Professional professional = new Professional(FullName, Email, Gender, Age, UserType, "", "", "", "", false, stringMap, stringMap, stringMap, stringMap);
                                        FirebaseDatabase.getInstance().getReference().child("Professionals").child(PhNumber).setValue(professional);
                                        break;
                                    case "Caregiver":
                                        Caregiver caregiver = new Caregiver(FullName, Email, Gender,Age, UserType, "", "", "", false, stringMap);
                                        FirebaseDatabase.getInstance().getReference().child("Caregivers").child(PhNumber).setValue(caregiver);
                                        break;
                                } */

                                Intent intent = new Intent(PhRegisterActivity.this, PhOTPActivity.class);
                                intent.putExtra("phonenumber", phonenumber);
                                FirebaseAuth.getInstance().signOut();
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.d("databaseError",databaseError.getMessage());
                        }

                    });
                }

            }
        });

        loginLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), Login_phone.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initList() {
        userTypeList = new ArrayList<>();
        userTypeList.add(new SpinnerItem(getString(R.string.patient), R.drawable.patient));
        userTypeList.add(new SpinnerItem(getString(R.string.professional), R.drawable.doctor));
        userTypeList.add(new SpinnerItem(getString(R.string.caregivers), R.drawable.others));
    }

    public static class Gender {
        public static final String[] Gendertype = {"Male","Female"};
    }
}
