package com.qslogics.slaj.Register;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.qslogics.slaj.Entity.User;
import com.qslogics.slaj.Login.Login;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Date;

public class Register extends AppCompatActivity {

    //data fields
    EditText fullname;
    String gender;
    String supportRole;
    EditText age;
    EditText email;
    EditText password;
    EditText confirmPassword;
    TextView loginLabel;
    String usertype;
    String timestamp;
    Button btnRegister;
    FirebaseAuth fAuth;
    DatabaseReference mDatabase;
    Spinner spinnerCountries;
    private ArrayList<SpinnerItem> userTypeList;
    private SpinnerAdapter adapter;
    private EditText phoneNumber;
    String finalPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //set data from text fields and other components
        fullname = findViewById(R.id.fullname_text);
        spinnerCountries = findViewById(R.id.spinnerCountriesPhoneRegister);
        spinnerCountries.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CountryData.countryNames));
        spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#72bb53"));/* if you want your item to be white */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        phoneNumber = findViewById(R.id.editTextPhoneRegister);
        age = findViewById(R.id.age_text);
        email = findViewById(R.id.login_email_text);
        password = findViewById(R.id.login_password_text);
        confirmPassword = findViewById(R.id.confirm_password_text);
        loginLabel = findViewById(R.id.login_label);
        btnRegister = findViewById(R.id.btn_register);
        //initialize firebase auth
        fAuth = FirebaseAuth.getInstance();
        //initialize firebase real time db
        mDatabase = FirebaseDatabase.getInstance().getReference();



        //Spinner Gender
        final Spinner spinnerGender = (Spinner) findViewById(R.id.gender_spinner); //android.R.layout.simple_spinner_item
        ArrayAdapter<CharSequence> spinnerGenderAdapter = ArrayAdapter.createFromResource(this,R.array.gender, R.layout.spinner_item_gender);
        spinnerGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(spinnerGenderAdapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = parent.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //Spinner UserTypes
        initList();
        Spinner spinnerUserTypes = findViewById(R.id.spinner_usertypes_register);
        adapter = new SpinnerAdapter(this, userTypeList);
        spinnerUserTypes.setAdapter(adapter);
        spinnerUserTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItem clickedItem = (SpinnerItem) parent.getItemAtPosition(position);
                usertype = clickedItem.getTypeName();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //Register button
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mFullName = fullname.getText().toString().trim();
                String mGender = "";
                String mUserType = "";
                if(gender.equals("ကျား")){
                    mGender = "Male";
                }
                else if(gender.equals("မ")){
                    mGender = "Female";
                }
                else{
                    mGender = gender;
                }
                int mAge = !age.getText().toString().trim().equals("")  ? Integer.parseInt(age.getText().toString().trim()) : 0;
                if(usertype.equals("လူနာများ")){
                    mUserType = "Patient";
                }
                else if(usertype.equals("ဆရာဝန်")){
                    mUserType = "Professional";
                }
                else if(usertype.equals("ဝန်ဆောင်မှုများ")){
                    mUserType = "Caregiver";
                }
                else{
                    mUserType = usertype;
                }
                String mEmail = email.getText().toString().trim();
                String mPassword = password.getText().toString().trim();
                String mConfirmPassword = confirmPassword.getText().toString().trim();

                String code =  CountryData.countryAreaCodes[spinnerCountries.getSelectedItemPosition()];
                String number = phoneNumber.getText().toString().trim();

                Date date= new Date();
                long time = date.getTime();
                timestamp = String.valueOf(time);

                //validation methods
                if(TextUtils.isEmpty(mFullName)) {
                    fullname.setError("Fullname is required.");
                    return;
                }
                if(TextUtils.isEmpty(age.getText().toString().trim())) {
                    age.setError("Age is required.");
                    return;
                }
                if(mAge < 15 || mAge > 120) {
                    age.setError("Age must be between 15 and 120.");
                    return;
                }
                if(TextUtils.isEmpty(mEmail)) {
                    email.setError("Email is required.");
                    return;
                }

                if(TextUtils.isEmpty(mPassword)){
                    password.setError("Password is required.");
                    return;
                }
                if(TextUtils.isEmpty(mConfirmPassword)){
                    confirmPassword.setError("Confirm password is required.");
                    return;
                }
                if(!mPassword.equals(mConfirmPassword)) {
                    password.setError("Passwords did not match.");
                    return;
                }
                if(TextUtils.isEmpty(number)) {
                    phoneNumber.setError("Phone number is required.");
                }

                finalPhoneNumber = "+" + code + number;

                btnRegister.setEnabled(false);

                User user = new User(mFullName, mUserType, mGender,mAge, mEmail ,finalPhoneNumber, false, timestamp);
                String rmDotPath = user.email.replace(".", "");
                String rmAtPath = rmDotPath.replace("@", "");
                //String path = user.email.substring(0, user.email.indexOf("@"));
                String path = rmAtPath;
                mDatabase.child("users").child("emailRegisteredUsers").child(path).setValue(user);

                //register user
                fAuth.createUserWithEmailAndPassword(mEmail, mPassword).addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            //Toast.makeText(Register.this,  "User created successfully.", Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            FirebaseUser user = fAuth.getCurrentUser();
                            if(user!= null) {
                                sendVerificationEmail();
                                btnRegister.setEnabled(true);
                                FirebaseAuth.getInstance().signOut();
                            }
                            /*fAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
                                @Override
                                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                    FirebaseUser user = firebaseAuth.getCurrentUser();
                                    if (user != null) {
                                        sendVerificationEmail();
                                        btnRegister.setEnabled(true);
                                        FirebaseAuth.getInstance().signOut();
                                        //finish();
                                        //Intent intent = new Intent(getApplicationContext(), Login.class);
                                        //startActivity(intent);
                                    } else {
                                        // User is signed out
                                    }
                                }
                            }); */

                        } else {
                            Toast.makeText(Register.this, "Error: "+ task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            btnRegister.setEnabled(true);
                        }
                    }
                });

            }
        });

        loginLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void initList() {
        userTypeList = new ArrayList<>();
        userTypeList.add(new SpinnerItem(getString(R.string.patient), R.drawable.patient));
        userTypeList.add(new SpinnerItem(getString(R.string.professional), R.drawable.doctor));
        userTypeList.add(new SpinnerItem(getString(R.string.caregivers), R.drawable.others));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }
    public static void hideKeyboard(Register activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public void sendVerificationEmail()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String[] message = {""};
        user.sendEmailVerification()
                .addOnCompleteListener(Register.this,new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Register.this,  "User created successfully. Please check \n verification mail to login.", Toast.LENGTH_SHORT).show();
                            //TODO : //////////
                            return;
                        }
                        else
                        {
                            // email not sent, so display message and restart the activity
                            //restart this activity
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            finish();
                            startActivity(getIntent());
                            Toast.makeText(Register.this,  "Verification mail did not sent. Please \n register again.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });
    }


}


