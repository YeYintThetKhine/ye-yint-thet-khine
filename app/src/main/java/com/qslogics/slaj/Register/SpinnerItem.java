package com.qslogics.slaj.Register;

public class SpinnerItem {
    private String typeName;
    private int typeImage;

    public SpinnerItem(String typeName, int typeImage) {
        this.typeImage = typeImage;
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTypeImage() {
        return typeImage;
    }

    public void setTypeImage(int typeImage) {
        this.typeImage = typeImage;
    }
}
