package com.qslogics.slaj.ScreeningFiles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Appointment.MedicalRecord_Appointment_Adapter;
import com.qslogics.slaj.Appointment.MedicalReportAdapter;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.R;

public class ScreeningFiles extends AppCompatActivity {

    //patient info
    TextView fullname;
    TextView email;
    TextView phone;
    TextView dob;
    TextView gender;
    TextView maritalStatus;
    TextView address;
    TextView hasTravelHistory;

    //covid checkup
    TextView haveCough;
    TextView haveTroubleBreathing;
    TextView temperature;
    TextView travelAbroad;

    //medical problem
    TextView recordType;
    TextView medicalNotes;


    ImageView imgBackArrow;
    RecyclerView medicalProblemRecycler;
    RecyclerView medicalReportRecycler;

    String patientKey;
    String msdKey;
    String msStatus;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabasePatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screening_file_detail);
        if(getIntent().getExtras() != null) {
            msdKey = getIntent().getStringExtra("childKeys");
            msStatus = getIntent().getStringExtra("medicalScreeningStatus");
            patientKey = getIntent().getStringExtra("patientKey");
        }

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //patient info hook
        fullname = findViewById(R.id.full_name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        dob = findViewById(R.id.date_of_birth);
        gender = findViewById(R.id.gender);
        maritalStatus = findViewById(R.id.marital_status);
        address = findViewById(R.id.address);
        hasTravelHistory = findViewById(R.id.travel_history);

        // covid checkup hook
        haveCough = findViewById(R.id.have_cough);
        haveTroubleBreathing = findViewById(R.id.have_trouble_breathing);
        temperature = findViewById(R.id.body_temp);
        travelAbroad = findViewById(R.id.travel_abroad);

        // medical problem hook
        recordType = findViewById(R.id.record_type);
        medicalNotes = findViewById(R.id.medical_notes);

        //for medical report and medical records
        medicalProblemRecycler = findViewById(R.id.mrecords);
        medicalReportRecycler = findViewById(R.id.mreports);

        if(msStatus.equals("completed")) {
            mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed").child(msdKey);
        } else {
            mDatabase = FirebaseDatabase.getInstance().getReference().child("MedicalRecords").child(msdKey);
        }

        mDatabasePatient = FirebaseDatabase.getInstance().getReference().child("Patients").child(patientKey);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    MedicalScreeningData msdata = dataSnapshot.getValue(MedicalScreeningData.class);

                    //patient info hook
                    fullname.setText(msdata.getPatientName());
                    maritalStatus.setText(msdata.getMaritalStatus());
                    address.setText(msdata.getAddress());

                    hasTravelHistory.setText(msdata.isHaveTravelHistory() ? getString(R.string.check_4_yes) : getString(R.string.check_4_no));

                    // covid checkup hook
                    haveCough.setText(msdata.isHaveCough() ? getString(R.string.check_1_yes) : getString(R.string.check_1_no));
                    haveTroubleBreathing.setText(msdata.isHaveTroubleBreathing() ? getString(R.string.check_2_yes) : getString(R.string.check_2_no));
                    temperature.setText(msdata.getTemperature());
                    travelAbroad.setText(msdata.isHaveTravelHistory() ? getString(R.string.check_4_yes):getString(R.string.check_4_yes));
                    String v = msdata.getTemperature();
                    if("Yes".equals(v)){
                        temperature.setText(getString(R.string.check_3_yes));
                    }
                    else if("No".equals(v)){
                        temperature.setText(getString(R.string.check_3_no));
                    }


                    // medical problem hook
                    recordType.setText(msdata.getType());
                    medicalNotes.setText(msdata.getMedicalNotes());

                    // medical records
                    if(msdata.getMedicalRecords().size() != 0) {
                        MedicalRecord_Appointment_Adapter medicalRecord_appointment_Adapter = new MedicalRecord_Appointment_Adapter(ScreeningFiles.this,msdata.getMedicalRecords());
                        medicalProblemRecycler.setAdapter(medicalRecord_appointment_Adapter);
                        medicalProblemRecycler.setHasFixedSize(true);
                        medicalProblemRecycler.setLayoutManager(new LinearLayoutManager(ScreeningFiles.this,LinearLayoutManager.HORIZONTAL,false));
                    }

                    // medical reports
                    if(msdata.getMedicalReports().size() !=0 ) {
                        MedicalReportAdapter medicalReportAdapter = new MedicalReportAdapter(ScreeningFiles.this,msdata.getMedicalReports(),null,false);
                        medicalReportRecycler.setAdapter(medicalReportAdapter);
                        medicalReportRecycler.setHasFixedSize(true);
                        medicalReportRecycler.setLayoutManager(new LinearLayoutManager(ScreeningFiles.this,LinearLayoutManager.HORIZONTAL,false));
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDatabasePatient.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Patient patient = dataSnapshot.getValue(Patient.class);
                    email.setText(patient.getEmail());
                    phone.setText(patient.getPatient_phone_numbers().get("ph_1"));
                    dob.setText(patient.getDob().equals("") || patient.getDob() == null ? "-----" : patient.getDob());
                    gender.setText(patient.getGender());
                }
                System.out.println("does not exist.");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}