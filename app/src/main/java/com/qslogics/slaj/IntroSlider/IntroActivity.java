package com.qslogics.slaj.IntroSlider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.qslogics.slaj.Login.Login_main;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabIndicator;
    Button btnNext;
    int indicatorPosition = 0;
    //Button btnLetsStart;
    Animation btnAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        if(restorePrefData()) {
            Intent intent = new Intent(getApplicationContext(), Login_main.class);
            startActivity(intent);
            finish();
        }
        // make full screen activity
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // hide the action bar
        //getSupportActionBar().hide();

        // ini views
        tabIndicator = findViewById(R.id.tab_indicator);
        //btnLetsStart = findViewById(R.id.btn_lets_start);
        btnNext = findViewById(R.id.btn_next);

        btnAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_animation);

        // setting up viewpager
        final List<ScreenItem> mList = new ArrayList<>();
        mList.add(new ScreenItem("Call Center", "Contact with medical professional easily.", R.drawable.call_center));
        mList.add(new ScreenItem("Track", "Track COVID-19 easily with simple questions.", R.drawable.track));
        mList.add(new ScreenItem("Video Call", "Meet doctors via Video Call.", R.drawable.videocall));
        mList.add(new ScreenItem("Stay Home", "Get medical support and information from Home.", R.drawable.stay_home));

        screenPager = findViewById(R.id.screen_viewpager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewPagerAdapter);

        //setting up tablayout with viewpager
        tabIndicator.setupWithViewPager(screenPager);



        //button click listener
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicatorPosition = screenPager.getCurrentItem();
                if(indicatorPosition < mList.size()) {
                    //btnLetsStart.setVisibility(View.INVISIBLE);
                    btnNext.setVisibility(View.VISIBLE);
                    tabIndicator.setVisibility(View.VISIBLE);

                    indicatorPosition++;
                    screenPager.setCurrentItem(indicatorPosition);
                }

                //hide next button on last page
                if(indicatorPosition == mList.size()) {
                    //displayLastScreen();
                    Intent tAndC = new Intent(getApplicationContext(), TermsAndConditions.class);
                    startActivity(tAndC);
                    finish();
                }
            }
        });

        //tab layout change listener
        tabIndicator.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == mList.size()) {
                   // displayLastScreen();
                    Intent tAndC = new Intent(getApplicationContext(), TermsAndConditions.class);
                    startActivity(tAndC);
                    finish();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //lets start button listener
        //btnLetsStart.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        Intent login = new Intent(getApplicationContext(), Login_main.class);
        //        startActivity(login);
        //        //to check whether slider has been displayed or not
        //        saveUserSliderStatus();
        //        finish();
        //    }
        //});
    }

    private void saveUserSliderStatus() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isSliderViewed", true);
        editor.commit();
    }

    private boolean restorePrefData() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isSliderViewed = prefs.getBoolean("isSliderViewed", false);
        return isSliderViewed;
    }

    private void displayLastScreen() {
        //btnLetsStart.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.INVISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);

        //setting lets start button animation
        //Start.setAnimation(btnAnimation);
    }
}
