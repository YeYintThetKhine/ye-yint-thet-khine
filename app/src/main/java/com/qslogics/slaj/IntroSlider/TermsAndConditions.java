package com.qslogics.slaj.IntroSlider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.appcompat.app.AppCompatActivity;

import com.qslogics.slaj.Login.Login_main;
import com.qslogics.slaj.R;

public class TermsAndConditions extends AppCompatActivity {

    CheckBox agreeCheckBox;
    Button btnLetsStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        agreeCheckBox = findViewById(R.id.agree_checkbox);
        btnLetsStart = findViewById(R.id.btn_lets_start);

        agreeCheckBox.setChecked(false);
        btnLetsStart.setAlpha(.5f);
        btnLetsStart.setClickable(false);
        btnLetsStart.setEnabled(false);

        agreeCheckBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()) {
                    btnLetsStart.setAlpha(1);
                    btnLetsStart.setClickable(true);
                    btnLetsStart.setEnabled(true);
                }
                if(!buttonView.isChecked()) {
                    btnLetsStart.setAlpha(.5f);
                    btnLetsStart.setClickable(false);
                    btnLetsStart.setEnabled(false);
                }
            }
        });

        btnLetsStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(getApplicationContext(), Login_main.class);
                startActivity(login);
                //to check whether slider has been displayed or not
                saveUserSliderStatus();
                finish();
            }
        });


    }

    private void saveUserSliderStatus() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isSliderViewed", true);
        editor.commit();
    }

    private boolean restorePrefData() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isSliderViewed = prefs.getBoolean("isSliderViewed", false);
        return isSliderViewed;
    }
}
