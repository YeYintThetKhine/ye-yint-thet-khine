package com.qslogics.slaj.Profile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.qslogics.slaj.Appointment.Pending_Appointment_Detail;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.HashMap;

public class Patient_Profile extends Fragment {

    String child_key;
    private DatabaseReference mDatabase;
    Button edit_btn_Click;


    ImageView patient_image_url;
    TextView name;
    TextView age;
    TextView age_title;
    TextView gender;
    Spinner spinnerGender;
    TextView email;
    TextView phone1;
    TextView phone2;
    TextView phone3;
    TextView dobirth;
    TextView address;

    boolean edit_indicator;
    String gender_value;
    String img_url;
    String patient_id;
    String converted_age = "";
    RelativeLayout img_layout;
    RelativeLayout img_layout_edit;

    String date  = "";

    public Uri imguri;

    StorageReference mStorageRef;
    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    Uri downloadUri;

    HashMap<String, String> patient_phone_numbers = new HashMap<>();
    HashMap<String, String> medical_record_images = new HashMap<>();

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_patient_profile,container,false);
        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        child_key =settings.getString("key", "child_key");
        mStorageRef = FirebaseStorage.getInstance().getReference("Images");

        edit_btn_Click = rootView.findViewById(R.id.edit_btn);

        edit_indicator = false;

        edit_btn_Click.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                onEditClick((Button) view);
            }

        });

        spinnerGender = rootView.findViewById(R.id.gender_spinner_p_profile);

        patient_image_url = rootView.findViewById(R.id.circularImageView_patient_profile);
        //this is to reduce the img resoluction....the image wont appear if the img has high resolution....the reason is because of de.hdodenhof.circleimageview.CircleImageView
        //it work fine in normal image view
        patient_image_url.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        img_layout = rootView.findViewById(R.id.patient_profile_image_layout);
        img_layout_edit = rootView.findViewById(R.id.patient_profile_image_layout_edit);

        name = rootView.findViewById(R.id.patient_Profile_name);
        age = rootView.findViewById(R.id.age_text);
        gender = rootView.findViewById(R.id.gender_text);
        age_title = rootView.findViewById(R.id.textView);
        email = rootView.findViewById(R.id.mail_text);
        phone1 = rootView.findViewById(R.id.phoneno_1);
        phone2 = rootView.findViewById(R.id.phoneno_2);
        phone3 = rootView.findViewById(R.id.phoneno_3);
        dobirth = rootView.findViewById(R.id.dob_profile_text);
        address = rootView.findViewById(R.id.address_text);

        spinnerGender.setVisibility(View.GONE);

        img_layout_edit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                imagePicker(view);
            }
        });

        img_layout_edit.setVisibility(View.GONE);

        dobirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
//                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                date = month + "/" + day + "/" + year;
                dobirth.setText(date);

                Log.d("checking day",String.valueOf(Calendar.getInstance().get(Calendar.MONTH)));

                int get_age = Calendar.getInstance().get(Calendar.YEAR) - year;
                if(month >= Calendar.getInstance().get(Calendar.MONTH) && day >= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)){
                    age.setText(String.valueOf(get_age));
                }
                else{
                    get_age= get_age - 1;
                    age.setText(String.valueOf(get_age));
                }

            }
        };

        name.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        name.setHint("");
        name.setFocusable(false);

        age.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        age.setFocusable(false);

        email.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        email.setHint("");
        email.setFocusable(false);

        phone2.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phone2.setHint("");
        phone2.setFocusable(false);

        phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phone3.setHint("");
        phone3.setFocusable(false);

        phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phone3.setHint("");
        phone3.setFocusable(false);


        dobirth.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        dobirth.setHint("");
        dobirth.setFocusable(false);
        dobirth.setEnabled(false);

        address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        address.setHint("");
        address.setFocusable(false);


        mDatabase = FirebaseDatabase.getInstance().getReference().child("Patients");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    if(getContext() != null) {
                        ArrayAdapter<CharSequence> spinnerGenderAdapter = ArrayAdapter.createFromResource(getContext(),R.array.gender, android.R.layout.simple_spinner_dropdown_item);
                        spinnerGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerGender.setAdapter(spinnerGenderAdapter);
//                        spinnerGender.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, Patient_Profile.Gender.Gendertype));
                        if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                            spinnerGender.setSelection(0);
                        }
                        else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                            spinnerGender.setSelection(1);
                        }
                        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE); /* if you want your item to be white */
                                gender_value = parent.getSelectedItem().toString();
                                String t = parent.getSelectedItem().toString();
                                if("Male".equals(t) || "ကျား".equals(t)){
                                    gender.setText(getString(R.string.male));
                                }
                                else if("Female".equals(t) || "မ".equals(t)){
                                    gender.setText ( getString ( R.string.female));
                                }
                                ///gender.setText(parent.getSelectedItem().toString());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                    patient_id = dataSnapshot.child(child_key).child("id").getValue().toString();
                    img_url = dataSnapshot.child(child_key).child("patient_image_url").getValue().toString();
                    Picasso.get().load(dataSnapshot.child(child_key).child("patient_image_url").getValue().toString()).into(patient_image_url);
                    name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                    int age_length = dataSnapshot.child(child_key).child("age").getValue().toString().length( );
                    if(age_title.getText().toString().equals("အသက်")){
                        converted_age = "";
                        for(int i = 0 ; i < age_length ; i++){
                            int itwo = i;
                            Character raw_age = dataSnapshot.child(child_key).child("age").getValue().toString().charAt(i);
                            System.out.println(raw_age);
                            if(raw_age.equals('0')){
                                converted_age = converted_age+"၀";
                            }
                            else if(raw_age.equals('1')){
                                converted_age = converted_age+"၁";
                            }
                            else if(raw_age.equals('2')){
                                converted_age = converted_age+"၂";
                            }
                            else if(raw_age.equals('3')){
                                converted_age = converted_age+"၃";
                            }
                            else if(raw_age.equals('4')){
                                converted_age = converted_age+"၄";
                            }
                            else if(raw_age.equals('5')){
                                converted_age = converted_age+"၅";
                            }
                            else if(raw_age.equals('6')){
                                converted_age = converted_age+"၆";
                            }
                            else if(raw_age.equals('7')){
                                converted_age = converted_age+"၇";
                            }
                            else if(raw_age.equals('8')){
                                converted_age = converted_age+"၈";
                            }
                            else if(raw_age.equals('9')){
                                converted_age = converted_age+"၉";
                            }
                        }
                        System.out.println(converted_age);
                        age.setText(converted_age);
                    }
                    else{
                        age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                    }
                    if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                        gender.setText(R.string.male);
                    }
                    else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                        gender.setText(R.string.female);
                    }
//                    gender.setText(dataSnapshot.child(child_key).child("gender").getValue().toString());
                    email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                    phone1.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_1").getValue().toString());

                    if(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_2").exists()){
                        phone2.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_2").getValue().toString());
                    }
                    else{
                        phone2.setText("");
                    }
                    if(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_3").exists()){
                        phone3.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_3").getValue().toString());
                    }
                    else{
                        phone3.setText("");
                    }
                    if(dataSnapshot.child(child_key).child("dob").getValue().toString().equals("")){
                        dobirth.setText("Pick your DOB");
                        date = "";
                    }
                    else{
                        dobirth.setText(dataSnapshot.child(child_key).child("dob").getValue().toString());
                        date = dataSnapshot.child(child_key).child("dob").getValue().toString();
                    }
                    address.setText(dataSnapshot.child(child_key).child("address").getValue().toString());

                    phone2.setMovementMethod(LinkMovementMethod.getInstance());
                    Spannable spannable = new SpannableString(phone2.getText().toString());
                    Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                    phone2.setText(spannable);

                    phone3.setMovementMethod(LinkMovementMethod.getInstance());
                    spannable = new SpannableString(phone3.getText().toString());
                    Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                    phone3.setText(spannable);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onEditClick(View view) {
        if(edit_indicator == false){
            Spannable spannable = new SpannableString(phone2.getText().toString());
            img_layout.setAlpha(0.5f);
            img_layout_edit.setVisibility(View.VISIBLE);

            phone2.setText(spannable);

            spannable = new SpannableString(phone3.getText().toString());
            phone3.setText(spannable);

            Log.d("clicked_here_false","button is clicked");
            edit_btn_Click.setText("Done");

            spinnerGender.setVisibility(View.VISIBLE);
            gender.setVisibility(View.GONE);

            name.setFocusableInTouchMode(true);
            name.setHint("Enter your name");
            name.setBackgroundTintList(ColorStateList.valueOf(Color.DKGRAY));

            age.setFocusableInTouchMode(true);
            age.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));


            if(child_key.startsWith("+")){
                email.setFocusableInTouchMode(true);
                email.setHint("Enter your email");
                email.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
            }

            phone2.setFocusableInTouchMode(true);
            phone2.setHint("Enter your 2nd Ph number");
            phone2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));


            phone3.setFocusableInTouchMode(true);
            phone3.setHint("Enter your 3rd Ph number");
            phone3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));

            dobirth.setFocusable(false);
            dobirth.setEnabled(true);
            dobirth.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));

            address.setFocusableInTouchMode(true);
            address.setHint("Enter your address detail");
            address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
            edit_indicator = true;
        }
        else{
            if(TextUtils.isEmpty(name.getText().toString())) {
                name.setError("Name field can not be empty");
                return;
            }

            if(TextUtils.isEmpty(age.getText().toString())) {
                age.setError("Age field can not be empty");
                return;
            }
            if(TextUtils.isEmpty(email.getText().toString())) {
                email.setError("Email field can not be empty");
                return;
            }
            else{
                new AlertDialog.Builder(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                    .setTitle("Edit Profile Confirmation")
                    .setMessage("Are you sure you want to accept these changes and save it?")
                    .setCancelable(false)
                    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Dialog loading_dialog = new Dialog(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                            loading_dialog.setContentView(R.layout.appointment_edit_loading_alert_dialog);
                            ImageButton dialogButton = (ImageButton) loading_dialog.findViewById(R.id.video_call_btn);
                            loading_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            loading_dialog.show();

                            phone2.setMovementMethod(LinkMovementMethod.getInstance());
                            img_layout.setAlpha(1.0f);
                            img_layout_edit.setVisibility(View.GONE);

                            Spannable spannable = new SpannableString(phone2.getText().toString());
                            Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                            phone2.setText(spannable);

                            phone3.setMovementMethod(LinkMovementMethod.getInstance());
                            spannable = new SpannableString(phone3.getText().toString());
                            Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                            phone3.setText(spannable);

                            Log.d("clicked_here_true","button is clicked");
                            edit_btn_Click.setText("Edit");

                            spinnerGender.setVisibility(View.GONE);
                            gender.setVisibility(View.VISIBLE);

                            name.setFocusable(false);
                            name.setHint("");
                            name.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            age.setFocusable(false);
                            age.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            email.setFocusable(false);
                            email.setHint("");
                            email.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            phone2.setFocusable(false);
                            phone2.setHint("");
                            phone2.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            phone3.setFocusable(false);
                            phone3.setHint("");
                            phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            dobirth.setFocusable(false);
                            dobirth.setHint("");
                            dobirth.setEnabled(false);
                            dobirth.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            address.setFocusable(false);
                            address.setHint("");
                            address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

//            String img_url = "https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fmale_patient.jpg?alt=media&token=dab38ccc-abcd-47cb-9df2-054246e50a8a";


                            patient_phone_numbers.put("ph_1",phone1.getText().toString());
                            patient_phone_numbers.put("ph_2",phone2.getText().toString());
                            patient_phone_numbers.put("ph_3",phone3.getText().toString());

                            medical_record_images.put("mdr_1","Not Provided Yet.");

                            if(imguri != null) {
                                if (uploadTask != null && uploadTask.isInProgress()) {
                                    Toast.makeText(getActivity(), "Upload in progress", Toast.LENGTH_LONG).show();
                                }
                                else{
                                    final StorageReference Ref = mStorageRef.child("img_"+child_key+"."+getExtension(imguri));
                                    uploadTask = Ref.putFile(imguri);
                                    Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                        @Override
                                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                            if (!task.isSuccessful()) {
                                                throw task.getException();
                                            }

                                            // Continue with the task to get the download URL
                                            return Ref.getDownloadUrl();
                                        }
                                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if (task.isSuccessful()) {
                                                String real_gender_value = "";
                                                if(gender.getText().toString().equals("ကျား")){
                                                    real_gender_value = "Male";
                                                }
                                                else if(gender.getText().toString().equals("မ")){
                                                    real_gender_value = "Female";
                                                }
                                                else{
                                                    real_gender_value = gender.getText().toString();
                                                }
                                                downloadUri = task.getResult();
//                                                Toast.makeText(getActivity(),"Image Uploaded successfully",Toast.LENGTH_LONG).show();
                                                Patient patient = new Patient(name.getText().toString(),email.getText().toString(), real_gender_value , Integer.parseInt(age.getText().toString()),"Patient",downloadUri.toString() ,address.getText().toString(),true,false,patient_phone_numbers,medical_record_images,child_key, patient_id, date);
                                                FirebaseDatabase.getInstance().getReference().child("Patients").child(child_key).setValue(patient);

                                                loading_dialog.dismiss();
                                            } else {
                                                Toast.makeText(getContext(),"Profile edit failed. Please try again",Toast.LENGTH_LONG).show();
                                                // Handle failures
                                                // ...
                                            }
                                        }
                                    });
                                }
                            }
                            else{
                                String real_gender_value = "";
                                if(gender.getText().toString().equals("ကျား")){
                                    real_gender_value = "Male";
                                }
                                else if(gender.getText().toString().equals("မ")){
                                    real_gender_value = "Female";
                                }
                                else{
                                    real_gender_value = gender.getText().toString();
                                }
                                Patient patient = new Patient(name.getText().toString(), email.getText().toString(), real_gender_value , Integer.parseInt(age.getText().toString()),"Patient",img_url ,address.getText().toString(),true,false,patient_phone_numbers,medical_record_images,child_key, patient_id, date);
                                FirebaseDatabase.getInstance().getReference().child("Patients").child(child_key).setValue(patient);

                                loading_dialog.dismiss();
                            }
                            imguri = null;
                            edit_indicator = false;
                        }
                    })
                    .setNegativeButton("Continue Editing", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            phone2.setMovementMethod(LinkMovementMethod.getInstance());
                            img_layout.setAlpha(1.0f);
                            img_layout_edit.setVisibility(View.GONE);

                            edit_btn_Click.setText("Edit");

                            Spannable spannable = new SpannableString(phone2.getText().toString());
                            Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                            phone2.setText(spannable);

                            phone3.setMovementMethod(LinkMovementMethod.getInstance());
                            spannable = new SpannableString(phone3.getText().toString());
                            Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                            phone3.setText(spannable);

                            Log.d("clicked_here_true","button is clicked");
                            edit_btn_Click.setText("Edit");

                            spinnerGender.setVisibility(View.GONE);
                            gender.setVisibility(View.VISIBLE);

                            name.setFocusable(false);
                            name.setHint("");
                            name.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            age.setFocusable(false);
                            age.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            email.setFocusable(false);
                            email.setHint("");
                            email.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            phone2.setFocusable(false);
                            phone2.setHint("");
                            phone2.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            phone3.setFocusable(false);
                            phone3.setHint("");
                            phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            dobirth.setFocusable(false);
                            dobirth.setHint("");
                            dobirth.setEnabled(false);
                            dobirth.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                            address.setFocusable(false);
                            address.setHint("");
                            address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            imguri = null;
                            edit_indicator = false;

                            mDatabase.addValueEventListener(new ValueEventListener() {
                                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
                                        ArrayAdapter<CharSequence> spinnerGenderAdapter = ArrayAdapter.createFromResource(getContext(),R.array.gender, android.R.layout.simple_spinner_dropdown_item);
                                        spinnerGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinnerGender.setAdapter(spinnerGenderAdapter);
//                                        spinnerGender.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, Patient_Profile.Gender.Gendertype));
                                        if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                                            spinnerGender.setSelection(0);
                                        }
                                        else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                                            spinnerGender.setSelection(1);
                                        }
                                        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE); /* if you want your item to be white */
                                                gender_value = parent.getSelectedItem().toString();
                                                String t = parent.getSelectedItem().toString();
                                                if("Male".equals(t) || "ကျား".equals(t)){
                                                    gender.setText(getString(R.string.male));
                                                }
                                                else if("Female".equals(t) || "မ".equals(t)){
                                                    gender.setText ( getString ( R.string.female));
                                                }
                                                ///gender.setText(parent.getSelectedItem().toString());
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {
                                            }
                                        });

                                        img_url = dataSnapshot.child(child_key).child("patient_image_url").getValue().toString();
                                        Picasso.get().load(dataSnapshot.child(child_key).child("patient_image_url").getValue().toString()).into(patient_image_url);
                                        name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                                        int age_length = dataSnapshot.child(child_key).child("age").getValue().toString().length( );
                                        if(age_title.getText().toString().equals("အသက်")){
                                            converted_age = "";
                                            for(int i = 0 ; i < age_length ; i++){
                                                int itwo = i;
                                                Character raw_age = dataSnapshot.child(child_key).child("age").getValue().toString().charAt(i);
                                                System.out.println(raw_age);
                                                if(raw_age.equals('0')){
                                                    converted_age = converted_age+"၀";
                                                }
                                                else if(raw_age.equals('1')){
                                                    converted_age = converted_age+"၁";
                                                }
                                                else if(raw_age.equals('2')){
                                                    converted_age = converted_age+"၂";
                                                }
                                                else if(raw_age.equals('3')){
                                                    converted_age = converted_age+"၃";
                                                }
                                                else if(raw_age.equals('4')){
                                                    converted_age = converted_age+"၄";
                                                }
                                                else if(raw_age.equals('5')){
                                                    converted_age = converted_age+"၅";
                                                }
                                                else if(raw_age.equals('6')){
                                                    converted_age = converted_age+"၆";
                                                }
                                                else if(raw_age.equals('7')){
                                                    converted_age = converted_age+"၇";
                                                }
                                                else if(raw_age.equals('8')){
                                                    converted_age = converted_age+"၈";
                                                }
                                                else if(raw_age.equals('9')){
                                                    converted_age = converted_age+"၉";
                                                }
                                            }
                                            System.out.println(converted_age);
                                            age.setText(converted_age);
                                        }
                                        else{
                                            age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                                        }
                                        if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                                            gender.setText(R.string.male);
                                        }
                                        else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                                            gender.setText(R.string.female);
                                        };
                                        email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                                        phone1.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_1").getValue().toString());

                                        if(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_2").exists()){
                                            phone2.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_2").getValue().toString());
                                        }
                                        else{
                                            phone2.setText("");
                                        }
                                        if(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_3").exists()){
                                            phone3.setText(dataSnapshot.child(child_key).child("patient_phone_numbers").child("ph_3").getValue().toString());
                                        }
                                        else{
                                            phone3.setText("");
                                        }
                                        if(dataSnapshot.child(child_key).child("dob").getValue().toString().equals("")){
                                            dobirth.setText("Pick your DOB");
                                        }
                                        else{
                                            dobirth.setText(dataSnapshot.child(child_key).child("dob").getValue().toString());
                                        }
                                        address.setText(dataSnapshot.child(child_key).child("address").getValue().toString());

                                        phone2.setMovementMethod(LinkMovementMethod.getInstance());
                                        Spannable spannable = new SpannableString(phone2.getText().toString());
                                        Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                                        phone2.setText(spannable);

                                        phone3.setMovementMethod(LinkMovementMethod.getInstance());
                                        spannable = new SpannableString(phone3.getText().toString());
                                        Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                                        phone3.setText(spannable);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }).show();
            }


        }
    }


    public static class Gender {
        public static final String[] Gendertype = {"Male","Female"};
    }

    public void imagePicker(View view) {
//        Intent intent = new Intent();
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("image/'");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode== Activity.RESULT_OK && data!=null && data.getData()!=null){
            imguri=data.getData();
            patient_image_url.setImageURI(imguri);
        }
    }

    private String getExtension(Uri uri){
        ContentResolver cr = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }
}
