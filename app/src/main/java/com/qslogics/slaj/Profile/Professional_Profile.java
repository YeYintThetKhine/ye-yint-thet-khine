package com.qslogics.slaj.Profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class Professional_Profile extends Fragment {

    private DatabaseReference mDatabase;
    private DatabaseReference mDatabase_doctor_specialist;
    private DatabaseReference mDatabase_fetch_specific_doctor_specialist;
    String child_key;
    ArrayList<String> available_list=new ArrayList<String>();
    HashMap<String, String > available_time_extra;
    HashMap<String, String > specializations_extra;
    String specializations_main;
    String specializations_plus;
    Button edit_btn;

    //for edit
    boolean edit_indicator;
    EditText professionalExperience;
    String professional_experience;
    EditText professionalEmail;
    String professional_email;
    EditText professionalName;
    String professional_name;
    EditText phoneOne;
    String phone_one;
    EditText phoneTwo;
    String phone_two;
    EditText phoneThree;
    String phone_three;
    EditText qualificationDetail;
    String qualification_detail;
    EditText specializationsDetail;
    EditText sm_num_value;
    String sm_num_get;

    String[] specializations;
    ArrayList<String> specializations_array = new ArrayList<String>();
    ArrayList<String> specializations_key_array = new ArrayList<String>();
    ArrayList<String> specializations_english = new ArrayList<String>();
    ArrayList<String> specializations_mm = new ArrayList<String>();
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();
    int singleselectitem_array_number;
    ImageButton editSp;


    ImageView professional_image_url;
    String img_url;
    RelativeLayout img_layout;
    RelativeLayout img_layout_edit;
    public Uri imguri;
    StorageReference mStorageRef;
    StorageReference mStorageRef_Sama_img;
    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    Uri downloadUri;


    EditText name;
    EditText experience;
    EditText email;
    EditText phone1;
    EditText phone2;
    EditText phone3;
    RecyclerView available_time_recycler;
    EditText qualification;
    EditText specialize;
    ArrayList<String> selectedSpValues = new ArrayList<>();
    Button sm_img_uploadbtn;
    private static final int PICK_IMAGE = 2;
    private Uri ImageUri;
    ArrayList<MedicalRecordImage> list = new ArrayList<>();
    ArrayList<Uri> ImageList = new ArrayList<>();
    RecyclerView sm_img_recyclerview;
    RecyclerView sm_img_recyclerview_display;
    Button doctor_new_available_time_btn;
    LayoutInflater layoutinflater;
    PopupWindow popupWindow;
    ConstraintLayout constraintlayout;

    ArrayList<MedicalRecordImage> final_sm_img_info= new ArrayList<>();
    private int upload_count = 0;
    ArrayList<MedicalRecordImage> final_sm_img_info_display= new ArrayList<>();

    ConstraintLayout constraintLayout;
    ConstraintSet constraintSet;
    TextView phonenumbers_label;
    int count=0;
    TextView specialize_text;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_professional_profile,container,false);
        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        child_key =settings.getString("key", "child_key");
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Professionals");
        mDatabase_doctor_specialist = FirebaseDatabase.getInstance().getReference().child("Categories");
        mStorageRef = FirebaseStorage.getInstance().getReference("Images");
        //Hooks
        edit_btn = rootView.findViewById(R.id.edit_btn_professional);
        professionalName = rootView.findViewById(R.id.doc_name);
        professionalExperience = rootView.findViewById(R.id.text_experience);
        professionalEmail = rootView.findViewById(R.id.email_text);
        phoneOne = rootView.findViewById(R.id.phoneno_1);
        phoneTwo = rootView.findViewById(R.id.phoneno_2);
        phoneThree = rootView.findViewById(R.id.phoneno_3);
        qualificationDetail = rootView.findViewById(R.id.qualification_detail);
        specializationsDetail = rootView.findViewById(R.id.specialize_detail_text);
        editSp = rootView.findViewById(R.id.edit_sp);
        sm_num_value = rootView.findViewById(R.id.sm_num_value);
        sm_img_uploadbtn = rootView.findViewById(R.id.sm_img_uploadbtn);
        phonenumbers_label = rootView.findViewById(R.id.phonenumbers_label);
        doctor_new_available_time_btn = rootView.findViewById(R.id.doctor_new_available_time_btn);
        constraintlayout = rootView.findViewById(R.id.constraintlayout);

        //Image
        professional_image_url = rootView.findViewById(R.id.circularImageView_professional_profile);
        //this is to reduce the img resoluction....the image wont appear if the img has high resolution....the reason is because of de.hdodenhof.circleimageview.CircleImageView
        //it work fine in normal image view
        professional_image_url.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        img_layout = rootView.findViewById(R.id.professional_profile_image_layout);
        img_layout_edit = rootView.findViewById(R.id.professional_profile_image_layout_edit);

        //recycleView for medical report
        sm_img_recyclerview = rootView.findViewById(R.id.sm_img_recyclerview);

        sm_img_recyclerview_display = rootView.findViewById(R.id.sm_img_recyclerview_display);

        specialize_text = rootView.findViewById(R.id.specialize_text);

        //for specialization
        specializations = getResources().getStringArray(R.array.specializations);

        checkedItems = new boolean[specializations.length];
//        editSp.setVisibility(View.INVISIBLE);
//        editSp.setEnabled(false);

        professionalName.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        professionalName.setHint("");
        professionalName.setFocusable(false);
        professionalName.setEnabled(false);

        professionalExperience.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        professionalExperience.setHint("");
        professionalExperience.setFocusable(false);
        professionalExperience.setEnabled(false);

        professionalEmail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        professionalEmail.setHint("");
        professionalEmail.setFocusable(false);
        professionalEmail.setEnabled(false);

        phoneOne.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phoneOne.setHint("");
        phoneOne.setFocusable(false);
        phoneOne.setEnabled(false);

        phoneTwo.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phoneTwo.setHint("");
        phoneTwo.setFocusable(false);
        phoneTwo.setEnabled(false);

        phoneThree.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phoneThree.setHint("");
        phoneThree.setFocusable(false);
        phoneThree.setEnabled(false);

        qualificationDetail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        qualificationDetail.setHint("");
        qualificationDetail.setFocusable(false);
        qualificationDetail.setEnabled(false);

        specializationsDetail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        specializationsDetail.setHint("");
        specializationsDetail.setFocusable(false);

        sm_num_value.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        sm_num_value.setHint("");
        sm_num_value.setFocusable(false);
        sm_num_value.setEnabled(false);


        constraintLayout = rootView.findViewById(R.id.secondpart);
        constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout);
        constraintSet.connect(R.id.phonenumbers_label,ConstraintSet.TOP,R.id.sm_img_recyclerview_display,ConstraintSet.BOTTOM,0);
        constraintSet.applyTo(constraintLayout);

        sm_img_uploadbtn.setVisibility(View.GONE);
        img_layout_edit.setVisibility(View.INVISIBLE);
        sm_img_recyclerview.setVisibility(View.INVISIBLE);

        doctor_new_available_time_btn.setVisibility(View.GONE);

        editSp.setVisibility(View.GONE);

        img_layout_edit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                imagePicker(view);
            }
        });

        sm_img_uploadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean first_condition = sm_img_uploadbtn.getText().equals("Re-Upload Sama Image") && ImageList.size() == 0;
                boolean second_condition = sm_img_uploadbtn.getText().equals("ပြန်လည်၍စမဓာတ်ပုံတင်မည်") && ImageList.size() == 0;
                if(first_condition || second_condition){
                    new androidx.appcompat.app.AlertDialog.Builder(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                        .setTitle(getString(R.string.sama_img_upload_dialog_title))
                        .setMessage(getString(R.string.sama_img_upload_dialog_message))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.sama_img_upload_dialog_ok_btn), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //              Intent itImage = new Intent(Intent.ACTION_GET_CONTENT);
                                Intent itImage = new Intent(Intent.ACTION_GET_CONTENT,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                itImage.setType("image/*");
                                itImage.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                startActivityForResult(itImage, PICK_IMAGE);
                            }})
                        .setNegativeButton(getString(R.string.sama_img_upload_dialog_cancel_btn), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }}).show();
                }
                else{
                    //Intent itImage = new Intent(Intent.ACTION_GET_CONTENT);
                    Intent itImage = new Intent(Intent.ACTION_GET_CONTENT,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    itImage.setType("image/*");
                    itImage.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    startActivityForResult(itImage, PICK_IMAGE);
                }
            }
        });

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                    img_url = dataSnapshot.child(child_key).child("pro_image_url").getValue().toString();
                    Picasso.get().load(dataSnapshot.child(child_key).child("pro_image_url").getValue().toString()).into(professional_image_url);
                    name = rootView.findViewById(R.id.doc_name);
                    experience = rootView.findViewById(R.id.text_experience);
                    email = rootView.findViewById(R.id.email_text);
                    phone1 = rootView.findViewById(R.id.phoneno_1);
                    phone2 = rootView.findViewById(R.id.phoneno_2);
                    phone3 = rootView.findViewById(R.id.phoneno_3);
                    available_time_recycler = rootView.findViewById(R.id.available_time_recycler);
                    qualification = rootView.findViewById(R.id.qualification_detail);
                    specialize = rootView.findViewById(R.id.specialize_detail_text);

//                    specializations_extra = (HashMap<String, String>) dataSnapshot.child(child_key).child("specializations").getValue();

                    for (DataSnapshot postSnapshot: dataSnapshot.child(child_key).child("available_time").getChildren()) {
                        available_list.add(postSnapshot.getValue(String.class));
                    }

//                    for (Map.Entry<String, String> entry : specializations_extra.entrySet()) {
//                        specializations_plus=entry.getValue()+",";
//                        selectedSpValues.add(entry.getValue());
//                        if(specializations_main == null){
//                            specializations_main=specializations_plus;
//                        }
//                        else{
//                            specializations_main=specializations_main+specializations_plus;
//                        }
//                    }

                    //int here = Arrays.asList(specializations).indexOf(selectedSpValues.get(0));
                    for(int i = 0; i < selectedSpValues.size(); i++) {
                        mUserItems.add(Arrays.asList(specializations).indexOf(selectedSpValues.get(i)));
                        checkedItems[Arrays.asList(specializations).indexOf(selectedSpValues.get(i))] = true;
                    }

                    Picasso.get().load(dataSnapshot.child(child_key).child("pro_image_url").getValue().toString()).into(professional_image_url);
                    name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                    experience.setText(dataSnapshot.child(child_key).child("experience").getValue().toString());
                    email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                    phone1.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_1").getValue().toString());
                    if(dataSnapshot.child(child_key).child("sm_no").getValue().toString().equals("")){
                        sm_num_value.setText("SM No not provided yet");
                    }
                    else{
                        sm_num_value.setText(dataSnapshot.child(child_key).child("sm_no").getValue().toString());
                    }
                    if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").exists()){

                        phone2.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").getValue().toString());
                    }
                    else{
                        phone2.setText("");
                    }
                    if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").exists()){

                        phone3.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").getValue().toString());
                    }
                    else{
                        phone3.setText("");
                    }
                    qualification.setText(dataSnapshot.child(child_key).child("qualifications").getValue().toString());

                    if(dataSnapshot.child(child_key).child("specializations").getValue().equals("Specialization was not choosen.")){
                        specializationsDetail.setText(R.string.no_specialist_value);
                    }
                    else{
                        mDatabase_fetch_specific_doctor_specialist = FirebaseDatabase.getInstance().getReference().child("Categories").child(dataSnapshot.child(child_key).child("specializations").getValue().toString());
                        mDatabase_fetch_specific_doctor_specialist.addListenerForSingleValueEvent(new ValueEventListener() {

                            public void onDataChange(DataSnapshot dataSnapshot){
                                specializationsDetail.setText(specialize_text.getText().equals("Specialize")?dataSnapshot.child("englishVer").getValue().toString():dataSnapshot.child("burmeseVer").getValue().toString());
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }



                    final AvailabletimeDoctor_adapter availabletimeDoctor_adapter = new AvailabletimeDoctor_adapter(getActivity(),available_list,false,Professional_Profile.this);
                    available_time_recycler.setAdapter(availabletimeDoctor_adapter);
                    available_time_recycler.setHasFixedSize(true);
                    available_time_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    final_sm_img_info_display.clear();
                    if(dataSnapshot.child(child_key).child("sama_img").exists()){
                        sm_img_uploadbtn.setText(getString(R.string.upload_sama_image));
                        for (DataSnapshot postSnapshot: dataSnapshot.child(child_key).child("sama_img").getChildren()) {
                            final_sm_img_info_display.add(postSnapshot.getValue(MedicalRecordImage.class));
                        }
                        SamaImgAdapterDisplay samaimgAdapterDisplay = new SamaImgAdapterDisplay(getActivity(),final_sm_img_info_display,sm_img_uploadbtn,true,Professional_Profile.this);
                        sm_img_recyclerview_display.setAdapter(samaimgAdapterDisplay);
                        sm_img_recyclerview_display.setHasFixedSize(true);
                        sm_img_recyclerview_display.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
                    }
                    else{
                        sm_img_uploadbtn.setText(getString(R.string.upload_sm_img));
                        MedicalRecordImage myData = new MedicalRecordImage("Sama Image/Images have not submitted","");
                        final_sm_img_info_display.add(myData);
                        SamaImgAdapterDisplay samaimgAdapterDisplay = new SamaImgAdapterDisplay(getActivity(),final_sm_img_info_display,sm_img_uploadbtn,true,Professional_Profile.this);
                        sm_img_recyclerview_display.setAdapter(samaimgAdapterDisplay);
                        sm_img_recyclerview_display.setHasFixedSize(true);
                        sm_img_recyclerview_display.setLayoutManager(new GridLayoutManager(getActivity(),1));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase_doctor_specialist.addListenerForSingleValueEvent(new ValueEventListener() {

            public void onDataChange(DataSnapshot dataSnapshot){
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    specializations_english.add(postSnapshot.child("englishVer").getValue().toString());
                    specializations_mm.add(postSnapshot.child("burShort").getValue().toString());
                    specializations_key_array.add(postSnapshot.getKey());
                }
            }

             @Override
             public void onCancelled(@NonNull DatabaseError databaseError) {

             }
        });

        doctor_new_available_time_btn.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    layoutinflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                    ViewGroup container = (ViewGroup) layoutinflater.inflate(R.layout.doctor_available_time_popup,null);

                    popupWindow = new PopupWindow(container, ViewPager.LayoutParams.WRAP_CONTENT,ViewPager.LayoutParams.WRAP_CONTENT,true);
                    popupWindow.showAtLocation(constraintlayout, Gravity.NO_GRAVITY,150,500);

                    View Container_two = popupWindow.getContentView().getRootView();

                    Context context = popupWindow.getContentView().getContext();
                    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                    WindowManager.LayoutParams p = (WindowManager.LayoutParams) Container_two.getLayoutParams();
                    p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    p.dimAmount = 0.5f;
                    wm.updateViewLayout(Container_two, p);

                    TextView from_textview = container.findViewById(R.id.from_time_picker);
                    TextView to_textview = container.findViewById(R.id.to_time_picker);
                    TextView time_save_btn = container.findViewById(R.id.time_save_btn);
                    TextView cancel_btn =  container.findViewById(R.id.cancel_btn);
                    from_textview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_access_time_black_24dp, 0, 0, 0);
                    to_textview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_access_time_black_24dp, 0, 0, 0);

                    ImageButton close_btn = container.findViewById(R.id.close_btn);

                    close_btn.setOnTouchListener(new View.OnTouchListener(){
                        public boolean onTouch(View v, MotionEvent event){
                            popupWindow.dismiss();
                            return true;
                        }
                    });

                    container.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            popupWindow.dismiss();
                            return true;
                        }
                    });

                    from_textview.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            settime(from_textview);
                        }
                    });

                    to_textview.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            settime(to_textview);
                        }
                    });

                    time_save_btn.setOnClickListener(new View.OnClickListener(){
                        public void onClick(View v){
                            if(available_list.size() > 0 && available_list.get(0).equals("Available times not provided.")){
                                available_list.clear();
                            }
                            available_list.add(from_textview.getText().toString()+" - "+to_textview.getText().toString());
                            final AvailabletimeDoctor_adapter availabletimeDoctor_adapter = new AvailabletimeDoctor_adapter(getActivity(),available_list,true,Professional_Profile.this);
                            available_time_recycler.setAdapter(availabletimeDoctor_adapter);
                            available_time_recycler.setHasFixedSize(true);
                            available_time_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                        }
                    });

                    cancel_btn.setOnClickListener(new View.OnClickListener(){
                        public void onClick(View v){
                            popupWindow.dismiss();
                        }
                    });

                    popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {

                        }
                    });
                }

            }

        );

        edit_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                onEditClick(v);
            }
        });


        editSp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                mBuilder.setTitle("Specializations");
                mBuilder.setSingleChoiceItems(specialize_text.getText().equals("Specialize")?specializations_english.toArray(new String[specializations_array.size()]):specializations_mm.toArray(new String[specializations_array.size()]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int array_num) {
                        singleselectitem_array_number = array_num;

//                        if(isChecked){
//
//                        }else{
//                            mUserItems.remove((Integer.valueOf(position)));
//                        }

                    }
                });
                mBuilder.setCancelable(false);
                mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDatabase_fetch_specific_doctor_specialist = FirebaseDatabase.getInstance().getReference().child("Categories").child(specializations_key_array.get(singleselectitem_array_number));
                        mDatabase_fetch_specific_doctor_specialist.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue().equals("Specialization was not choosen.")){
                                    specializationsDetail.setText(R.string.no_specialist_value);
                                }
                                else{
                                    specializationsDetail.setText(specialize_text.getText().equals("Specialize")?dataSnapshot.child("englishVer").getValue().toString():dataSnapshot.child("burmeseVer").getValue().toString());
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
                });
                mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                mBuilder.setNeutralButton("Clear all", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        singleselectitem_array_number = -1;
                        specializationsDetail.setText(R.string.no_specialist_value);
                    }
                });
                mBuilder.show();
            }

        });

//        editSp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
//                mBuilder.setTitle("Specializations");
//                mBuilder.setMultiChoiceItems(specializations, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int position, boolean isChecked) {
//                        if(isChecked){
//                            mUserItems.add(position);
//                        }else{
//                            mUserItems.remove((Integer.valueOf(position)));
//                        }
//                    }
//                });
//                mBuilder.setCancelable(false);
//                mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String items = "";
//                        specializations_extra.clear();
//                        for(int i = 0; i < mUserItems.size(); i++) {
//                            items = items + specializations[mUserItems.get(i)];
//                            int saveKey = i + 1;
//                            specializations_extra.put("sp_"+Integer.toString(saveKey),specializations[mUserItems.get(i)]);
//                            if(i != mUserItems.size() - 1) {
//                                items = items + ", ";
//                            }
//                        }
//                        specializationsDetail.setText(items);
//                    }
//                });
//                mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                mBuilder.setNeutralButton("Clear all", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        for(int i = 0; i < checkedItems.length; i++) {
//                            checkedItems[i] = false;
//                            mUserItems.clear();
//                            specializationsDetail.setText("");
//                        }
//                    }
//                });
//                mBuilder.show();
//            }
//
//        });

        return rootView;
    }

    public void settime(TextView receivedtext){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                receivedtext.setText( selectedHour + ":" + selectedMinute + ":00");
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void geteditedData(Dialog loading_dialog){
        available_list.clear();
//        specializations_extra.clear();
        specializations_main="";
        available_list.clear();
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                    img_url = dataSnapshot.child(child_key).child("pro_image_url").getValue().toString();
                    Picasso.get().load(dataSnapshot.child(child_key).child("pro_image_url").getValue().toString()).into(professional_image_url);

//                    specializations_extra = (HashMap<String, String>) dataSnapshot.child(child_key).child("specializations").getValue();
//
//
//                    for (Map.Entry<String, String> entry : specializations_extra.entrySet()) {
//                        specializations_plus=entry.getValue()+",";
//                        selectedSpValues.add(entry.getValue());
//                        if(specializations_main == null){
//                            specializations_main=specializations_plus;
//                        }
//                        else{
//                            specializations_main=specializations_main+specializations_plus;
//                        }
//                    }

                    for (DataSnapshot postSnapshot: dataSnapshot.child(child_key).child("available_time").getChildren()) {
                        available_list.add(postSnapshot.getValue(String.class));
                    }
                    //int here = Arrays.asList(specializations).indexOf(selectedSpValues.get(0));
                    for(int i = 0; i < selectedSpValues.size(); i++) {
                        mUserItems.add(Arrays.asList(specializations).indexOf(selectedSpValues.get(i)));
                        checkedItems[Arrays.asList(specializations).indexOf(selectedSpValues.get(i))] = true;
                    }

                    Picasso.get().load(dataSnapshot.child(child_key).child("pro_image_url").getValue().toString()).into(professional_image_url);
                    name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                    experience.setText(dataSnapshot.child(child_key).child("experience").getValue().toString());
                    email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                    phone1.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_1").getValue().toString());
                    if(dataSnapshot.child(child_key).child("sm_no").getValue().toString().equals("")){
                        sm_num_value.setText("SM No not provided yet");
                    }
                    else{
                        sm_num_value.setText(dataSnapshot.child(child_key).child("sm_no").getValue().toString());
                    }
                    if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").exists()){

                        phone2.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").getValue().toString());
                    }
                    else{
                        phone2.setText("");
                    }
                    if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").exists()){

                        phone3.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").getValue().toString());
                    }
                    else{
                        phone3.setText("");
                    }
                    qualification.setText(dataSnapshot.child(child_key).child("qualifications").getValue().toString());
                    if(dataSnapshot.child(child_key).child("specializations").getValue().equals("Specialization was not choosen.")){
                        specializationsDetail.setText(R.string.no_specialist_value);
                    }
                    else{
                        mDatabase_fetch_specific_doctor_specialist = FirebaseDatabase.getInstance().getReference().child("Categories").child(dataSnapshot.child(child_key).child("specializations").getValue().toString());
                        mDatabase_fetch_specific_doctor_specialist.addListenerForSingleValueEvent(new ValueEventListener() {

                            public void onDataChange(DataSnapshot dataSnapshot){
                                specializationsDetail.setText(specialize_text.getText().equals("Specialize")?dataSnapshot.child("englishVer").getValue().toString():dataSnapshot.child("burmeseVer").getValue().toString());
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }


                    final AvailabletimeDoctor_adapter availabletimeDoctor_adapter = new AvailabletimeDoctor_adapter(getActivity(),available_list,false,Professional_Profile.this);
                    available_time_recycler.setAdapter(availabletimeDoctor_adapter);
                    available_time_recycler.setHasFixedSize(true);
                    available_time_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    final_sm_img_info_display.clear();
                    if(dataSnapshot.child(child_key).child("sama_img").exists()){
                        sm_img_uploadbtn.setText(getString(R.string.upload_sama_image));
                        for (DataSnapshot postSnapshot: dataSnapshot.child(child_key).child("sama_img").getChildren()) {
                            final_sm_img_info_display.add(postSnapshot.getValue(MedicalRecordImage.class));
                        }
                        SamaImgAdapterDisplay samaimgAdapterDisplay = new SamaImgAdapterDisplay(getActivity(),final_sm_img_info_display,sm_img_uploadbtn,true,Professional_Profile.this);
                        sm_img_recyclerview_display.setAdapter(samaimgAdapterDisplay);
                        sm_img_recyclerview_display.setHasFixedSize(true);
                        sm_img_recyclerview_display.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
                    }
                    else{
                        sm_img_uploadbtn.setText(getString(R.string.upload_sm_img));
                        MedicalRecordImage myData = new MedicalRecordImage("Sama Image/Images have not submitted","");
                        final_sm_img_info_display.add(myData);
                        SamaImgAdapterDisplay samaimgAdapterDisplay = new SamaImgAdapterDisplay(getActivity(),final_sm_img_info_display,sm_img_uploadbtn,true,Professional_Profile.this);
                        sm_img_recyclerview_display.setAdapter(samaimgAdapterDisplay);
                        sm_img_recyclerview_display.setHasFixedSize(true);
                        sm_img_recyclerview_display.setLayoutManager(new GridLayoutManager(getActivity(),1));
                    }
                }
                loading_dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onEditClick(View rootView) {
        if(edit_indicator == false) {

            img_layout.setAlpha(0.5f);
            img_layout_edit.setVisibility(View.VISIBLE);

            Spannable spannable = new SpannableString(phoneOne.getText().toString());
            phoneOne.setText(spannable);

            spannable = new SpannableString(phoneTwo.getText().toString());
            phoneTwo.setText(spannable);

            spannable = new SpannableString(phoneThree.getText().toString());
            phoneThree.setText(spannable);

            edit_btn.setText("Done");

            //name
            professionalName.setFocusableInTouchMode(true);
            professionalName.setHint("Enter your name");
            professionalName.setBackgroundTintList(ColorStateList.valueOf(Color.DKGRAY));
            professionalName.setEnabled(true);

            //experience
            professionalExperience.setFocusableInTouchMode(true);
            professionalExperience.setHint("Enter experience");
            professionalExperience.setBackgroundTintList(ColorStateList.valueOf(Color.DKGRAY));
            professionalExperience.setEnabled(true);

            professionalEmail.setFocusableInTouchMode(true);
            professionalEmail.setHint("Enter email");
            professionalEmail.setBackgroundTintList(ColorStateList.valueOf(Color.DKGRAY));
            professionalEmail.setEnabled(true);

            sm_num_value.setFocusableInTouchMode(true);
            sm_num_value.setHint("Enter SM number");
            sm_num_value.setBackgroundTintList(ColorStateList.valueOf(Color.DKGRAY));
            sm_num_value.setEnabled(true);

            if(phoneOne.getText().toString().equals("Phone numbers not provided yet.")) {
                phoneOne.setFocusableInTouchMode(true);
                phoneOne.setHint("Enter your Main Ph number");
                phoneOne.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
                phoneOne.setEnabled(true);
            }

            phoneTwo.setFocusableInTouchMode(true);
            phoneTwo.setHint("Enter your 2nd Ph number");
            phoneTwo.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
            phoneTwo.setEnabled(true);

            phoneThree.setFocusableInTouchMode(true);
            phoneThree.setHint("Enter your 3rd Ph number");
            phoneThree.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
            phoneThree.setEnabled(true);

            qualificationDetail.setFocusableInTouchMode(true);
            qualificationDetail.setHint("Enter your qualification details.");
            qualificationDetail.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
            qualificationDetail.setEnabled(true);

            editSp.setVisibility(View.VISIBLE);
            editSp.setEnabled(true);

            edit_indicator = true;



            constraintSet.connect(R.id.phonenumbers_label,ConstraintSet.TOP,R.id.sm_img_recyclerview,ConstraintSet.BOTTOM,0);
            constraintSet.applyTo(constraintLayout);
            sm_img_uploadbtn.setVisibility(View.VISIBLE);
            img_layout_edit.setVisibility(View.VISIBLE);
            sm_img_recyclerview.setVisibility(View.VISIBLE);

            sm_img_recyclerview_display.setVisibility(View.INVISIBLE);

            doctor_new_available_time_btn.setVisibility(View.VISIBLE);

            editSp.setVisibility(View.VISIBLE);

            final AvailabletimeDoctor_adapter availabletimeDoctor_adapter = new AvailabletimeDoctor_adapter(getActivity(),available_list,true,Professional_Profile.this);
            available_time_recycler.setAdapter(availabletimeDoctor_adapter);
            available_time_recycler.setHasFixedSize(true);
            available_time_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        } else {
            new androidx.appcompat.app.AlertDialog.Builder(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                    .setTitle("Edit Profile Confirmation")
                    .setMessage("Are you sure you want to accept these changes and save it?")
                    .setCancelable(false)
                    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            professional_email = professionalEmail.getText().toString().trim();
                            professional_experience = professionalExperience.getText().toString().trim();
                            professional_name = professionalName.getText().toString().trim();
                            qualification_detail = qualificationDetail.getText().toString().trim();
                            phone_one = phoneOne.getText().toString().trim();
                            phone_two = phoneTwo.getText().toString().trim();
                            phone_three = phoneThree.getText().toString().trim();
                            sm_num_get = sm_num_value.getText().toString().trim();

                            System.out.println(sm_num_get);

                            if(TextUtils.isEmpty(professional_email)) {
                                professionalEmail.setError("Email is required.");
                                return;
                            }
                            if(TextUtils.isEmpty(professional_name)) {
                                professionalName.setError("Name is required.");
                                return;
                            }
                            if(TextUtils.isEmpty(phone_one)) {
                                phoneOne.setError("Main phone is required.");
                            }
                            boolean first_condition = sm_num_value.getText().toString().length() > 0 && !sm_num_value.getText().toString().equals("SM No not provided yet");
                            boolean second_condition = sm_num_value.getText().toString().length() < 0 && sm_num_value.getText().toString().equals("SM No not provided yet");
                            boolean third_condition = !final_sm_img_info_display.get(0).rec_image_name.equals("Sama Image/Images have not submitted");
                            boolean fifth_condition = ImageList.size() == 0 && final_sm_img_info_display.get(0).rec_image_name.equals("Sama Image/Images have not submitted");
                            if(ImageList.size() == 0 && final_sm_img_info_display.get(0).rec_image_name.equals("Sama Image/Images have not submitted")) {
                                new androidx.appcompat.app.AlertDialog.Builder(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                                        .setTitle(getString(R.string.sama_img_upload_dialog_title))
                                        .setMessage(getString(R.string.sama_img_no_img_dialog_message))
                                        .setCancelable(true)
                                        .setPositiveButton(getString(R.string.sama_img_no_img_dialog_ok_btn), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).show();
                            }
                            else if(sm_num_value.getText().toString().length() == 0 || sm_num_value.getText().toString().equals("SM No not provided yet")){
                                sm_num_value.setError(getString(R.string.sama_no_empty_field_message));
                            }
                            else{
                                img_layout.setAlpha(1.0f);
                                img_layout_edit.setVisibility(View.GONE);
                                edit_btn.setText("Edit");

                                //name
                                professionalName.setFocusableInTouchMode(false);
                                professionalName.setHint("");
                                professionalName.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                professionalName.setEnabled(false);

                                sm_num_value.setFocusableInTouchMode(false);
                                sm_num_value.setHint("");
                                sm_num_value.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                sm_num_value.setEnabled(false);

                                //experience
                                professionalExperience.setFocusableInTouchMode(false);
                                professionalExperience.setHint("");
                                professionalExperience.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                professionalExperience.setEnabled(false);

                                professionalEmail.setFocusableInTouchMode(false);
                                professionalEmail.setHint("");
                                professionalEmail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                professionalEmail.setEnabled(false);

                                phoneOne.setFocusableInTouchMode(false);
                                phoneOne.setHint("");
                                phoneOne.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                phoneOne.setEnabled(false);


                                phoneTwo.setFocusableInTouchMode(false);
                                phoneTwo.setHint("");
                                phoneTwo.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                phoneTwo.setEnabled(false);

                                phoneThree.setFocusableInTouchMode(false);
                                phoneThree.setHint("");
                                phoneThree.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                phoneThree.setEnabled(false);

                                qualificationDetail.setFocusableInTouchMode(false);
                                qualificationDetail.setHint("");
                                qualificationDetail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                                qualificationDetail.setEnabled(false);

                                editSp.setVisibility(View.INVISIBLE);
                                editSp.setEnabled(false);

                                constraintSet.connect(R.id.phonenumbers_label,ConstraintSet.TOP,R.id.sm_img_recyclerview_display,ConstraintSet.BOTTOM,0);
                                constraintSet.applyTo(constraintLayout);
                                sm_img_uploadbtn.setVisibility(View.GONE);
                                img_layout_edit.setVisibility(View.INVISIBLE);
                                sm_img_recyclerview.setVisibility(View.INVISIBLE);

                                sm_img_recyclerview_display.setVisibility(View.VISIBLE);

                                doctor_new_available_time_btn.setVisibility(View.GONE);

                                editSp.setVisibility(View.GONE);

                                final Dialog loading_dialog = new Dialog(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                                loading_dialog.setContentView(R.layout.appointment_edit_loading_alert_dialog);
                                ImageButton dialogButton = (ImageButton) loading_dialog.findViewById(R.id.video_call_btn);
                                loading_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                loading_dialog.show();
                                if(imguri != null) {
                                    if (uploadTask != null && uploadTask.isInProgress()) {
                                        Toast.makeText(getActivity(), "Upload in progress", Toast.LENGTH_LONG).show();
                                    }
                                    else{
                                        final StorageReference Ref = mStorageRef.child("img_"+child_key+"."+getExtension(imguri));
                                        uploadTask = Ref.putFile(imguri);
                                        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                            @Override
                                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                if (!task.isSuccessful()) {
                                                    throw task.getException();
                                                }

                                                // Continue with the task to get the download URL
                                                return Ref.getDownloadUrl();
                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Uri> task) {
                                                if (task.isSuccessful()) {
                                                    System.out.println(sm_num_value.getText().toString());
                                                    downloadUri = task.getResult();
//                                                Toast.makeText(getActivity(),"Image Uploaded successfully",Toast.LENGTH_LONG).show();
                                                    DatabaseReference proRootRef = mDatabase.child(child_key);
                                                    proRootRef.child("email").setValue(professional_email);
                                                    proRootRef.child("sm_no").setValue(sm_num_get);
                                                    proRootRef.child("experience").setValue(professional_experience);
                                                    proRootRef.child("name").setValue(professional_name);
                                                    proRootRef.child("qualifications").setValue(qualification_detail);
                                                    proRootRef.child("pro_phone_numbers").child("ph_1").setValue(phone_one);
                                                    proRootRef.child("pro_phone_numbers").child("ph_2").setValue(phone_two);
                                                    proRootRef.child("pro_phone_numbers").child("ph_3").setValue(phone_three);
                                                    if(singleselectitem_array_number == -1){
                                                        proRootRef.child("specializations").setValue("Specialization was not choosen.");
                                                    }
                                                    else{
                                                        proRootRef.child("specializations").setValue(specializations_key_array.get(singleselectitem_array_number));
                                                    }
                                                    proRootRef.child("available_time").setValue(available_list);
                                                    proRootRef.child("pro_image_url").setValue(downloadUri.toString());

                                                    geteditedData(loading_dialog);

                                                } else {
                                                    Toast.makeText(getContext(),"Profile edit failed. Please try again",Toast.LENGTH_LONG).show();
                                                    // Handle failures
                                                    // ...
                                                }
                                            }
                                        });
                                    }
                                }
                                if(ImageList.size() != 0){
                                    upload_count=0;
                                    StorageReference listRef = FirebaseStorage.getInstance().getReference().child("SamaImages/thihaaungkyawqslogicscom");
                                    listRef.listAll()
                                            .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                                                @Override
                                                public void onSuccess(ListResult listResult) {
                                                    for (StorageReference prefix : listResult.getPrefixes()) {
                                                        // All the prefixes under listRef.
                                                        // You may call listAll() recursively on them.
                                                    }

                                                    for (StorageReference item : listResult.getItems()) {
                                                        // All the items under listRef.
                                                        List<StorageReference> items = listResult.getItems();

                                                        Log.d("filename",item.getName());
                                                        StorageReference listRef_to_delete = FirebaseStorage.getInstance().getReference().child("SamaImages/thihaaungkyawqslogicscom/"+item.getName());
                                                        listRef_to_delete.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                                System.out.println("deleted");
                                                                // File deleted successfully
                                                            }
                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception exception) {
                                                                // Uh-oh, an error occurred!
                                                            }
                                                        });

                                                    }
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    // Uh-oh, an error occurred!
                                                }
                                            });
                                    SharedPreferences settings = getActivity().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                                    String doc_childkey =settings.getString("key", "child_key");
                                    mStorageRef_Sama_img = FirebaseStorage.getInstance().getReference().child("SamaImages").child(doc_childkey);
                                    for (upload_count = 0; upload_count < ImageList.size(); upload_count++) {
                                        final String img_name = "SamaImg-"+(upload_count+1);
                                        final StorageReference Ref = mStorageRef_Sama_img.child("SamaImg-" + (upload_count + 1));
                                        uploadTask = Ref.putFile(ImageList.get(upload_count));
                                        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                            @Override
                                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                if (!task.isSuccessful()) {
                                                    throw task.getException();
                                                }

                                                // Continue with the task to get the download URL
                                                return Ref.getDownloadUrl();
                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Uri> task) {
                                                if (task.isSuccessful()) {
                                                    downloadUri = task.getResult();
                                                    count++;
                                                    MedicalRecordImage med_report_data = new MedicalRecordImage("SamaImg-"+String.valueOf(count),downloadUri.toString());
                                                    final_sm_img_info.add(med_report_data);

                                                    SharedPreferences settings = getActivity().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                                                    String doc_childkey =settings.getString("key", "child_key");

                                                    if(final_sm_img_info.size() == upload_count){
                                                        DatabaseReference proRootRef = mDatabase.child(child_key);
                                                        proRootRef.child("email").setValue(professional_email);
                                                        proRootRef.child("experience").setValue(professional_experience);
                                                        proRootRef.child("name").setValue(professional_name);
                                                        proRootRef.child("qualifications").setValue(qualification_detail);
                                                        proRootRef.child("sm_no").setValue(sm_num_get);
                                                        proRootRef.child("pro_phone_numbers").child("ph_1").setValue(phone_one);
                                                        proRootRef.child("pro_phone_numbers").child("ph_2").setValue(phone_two);
                                                        proRootRef.child("pro_phone_numbers").child("ph_3").setValue(phone_three);
                                                        proRootRef.child("specializations").setValue(specializations_key_array.get(singleselectitem_array_number));
                                                        proRootRef.child("pro_image_url").setValue(img_url);
                                                        proRootRef.child("available_time").setValue(available_list);
                                                        proRootRef.child("sama_img").setValue(final_sm_img_info);

                                                        final_sm_img_info_display.clear();
                                                        ImageList.clear();
                                                        list.clear();
                                                        upload_count=0;

                                                        geteditedData(loading_dialog);
                                                    }

                                                } else {
                                                    // Handle failures
                                                    // ...
                                                }
                                            }
                                        });
                                    }
                                }
                                else{
                                    DatabaseReference proRootRef = mDatabase.child(child_key);
                                    proRootRef.child("email").setValue(professional_email);
                                    proRootRef.child("experience").setValue(professional_experience);
                                    proRootRef.child("name").setValue(professional_name);
                                    proRootRef.child("qualifications").setValue(qualification_detail);
                                    proRootRef.child("sm_no").setValue(sm_num_get);
                                    proRootRef.child("pro_phone_numbers").child("ph_1").setValue(phone_one);
                                    proRootRef.child("pro_phone_numbers").child("ph_2").setValue(phone_two);
                                    proRootRef.child("pro_phone_numbers").child("ph_3").setValue(phone_three);
                                    if(singleselectitem_array_number == -1){
                                        proRootRef.child("specializations").setValue("Specialization was not choosen.");
                                    }
                                    else{
                                        proRootRef.child("specializations").setValue(specializations_key_array.get(singleselectitem_array_number));
                                    }
                                    proRootRef.child("available_time").setValue(available_list);
                                    proRootRef.child("pro_image_url").setValue(img_url);

                                    geteditedData(loading_dialog);
//                                    final_sm_img_info_display.clear();
                                    ImageList.clear();
                                    list.clear();
                                    upload_count=0;
                                }
                                imguri = null;
                                edit_indicator = false;
                            }
                        }
                    })
                    .setNegativeButton("Continue Editing", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            img_layout.setAlpha(1.0f);
                            img_layout_edit.setVisibility(View.GONE);
                            edit_btn.setText("Edit");

                            //name
                            professionalName.setFocusableInTouchMode(false);
                            professionalName.setHint("");
                            professionalName.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            professionalName.setEnabled(false);

                            sm_num_value.setFocusableInTouchMode(false);
                            sm_num_value.setHint("");
                            sm_num_value.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            sm_num_value.setEnabled(false);

                            //experience
                            professionalExperience.setFocusableInTouchMode(false);
                            professionalExperience.setHint("");
                            professionalExperience.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            professionalExperience.setEnabled(false);

                            professionalEmail.setFocusableInTouchMode(false);
                            professionalEmail.setHint("");
                            professionalEmail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            professionalEmail.setEnabled(false);

                            phoneOne.setFocusableInTouchMode(false);
                            phoneOne.setHint("");
                            phoneOne.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            phoneOne.setEnabled(false);


                            phoneTwo.setFocusableInTouchMode(false);
                            phoneTwo.setHint("");
                            phoneTwo.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            phoneTwo.setEnabled(false);

                            phoneThree.setFocusableInTouchMode(false);
                            phoneThree.setHint("");
                            phoneThree.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            phoneThree.setEnabled(false);

                            qualificationDetail.setFocusableInTouchMode(false);
                            qualificationDetail.setHint("");
                            qualificationDetail.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                            qualificationDetail.setEnabled(false);

                            editSp.setVisibility(View.INVISIBLE);
                            editSp.setEnabled(false);

                            edit_indicator = false;

                            constraintSet.connect(R.id.phonenumbers_label,ConstraintSet.TOP,R.id.sm_img_recyclerview_display,ConstraintSet.BOTTOM,0);
                            constraintSet.applyTo(constraintLayout);
                            sm_img_uploadbtn.setVisibility(View.GONE);
                            img_layout_edit.setVisibility(View.INVISIBLE);
                            sm_img_recyclerview.setVisibility(View.INVISIBLE);

                            sm_img_recyclerview_display.setVisibility(View.VISIBLE);

                            doctor_new_available_time_btn.setVisibility(View.GONE);

                            editSp.setVisibility(View.GONE);

                            available_list.clear();
                            specializations_plus="";
                            specializations_main="";
                            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){

//                                        for (Map.Entry<String, String> entry : specializations_extra.entrySet()) {
////                                            specializations_plus=entry.getValue()+",";
////                                            selectedSpValues.add(entry.getValue());
////                                            if(specializations_main == null){
////                                                specializations_main=specializations_plus;
////                                            }
////                                            else{
////                                                specializations_main=specializations_main+specializations_plus;
////                                            }
////                                        }

                                        for (DataSnapshot postSnapshot: dataSnapshot.child(child_key).child("available_time").getChildren()) {
                                            available_list.add(postSnapshot.getValue(String.class));
                                        }
                                        //int here = Arrays.asList(specializations).indexOf(selectedSpValues.get(0));
                                        for(int i = 0; i < selectedSpValues.size(); i++) {
                                            mUserItems.add(Arrays.asList(specializations).indexOf(selectedSpValues.get(i)));
                                            checkedItems[Arrays.asList(specializations).indexOf(selectedSpValues.get(i))] = true;
                                        }
                                        System.out.println(dataSnapshot.child(child_key).child("name").getValue().toString());
                                        Picasso.get().load(dataSnapshot.child(child_key).child("pro_image_url").getValue().toString()).into(professional_image_url);
                                        name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                                        experience.setText(dataSnapshot.child(child_key).child("experience").getValue().toString());
                                        email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                                        phone1.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_1").getValue().toString());
                                        if(dataSnapshot.child(child_key).child("sm_no").getValue().toString().equals("")){
                                            sm_num_value.setText("SM No not provided yet");
                                        }
                                        else{
                                            sm_num_value.setText(dataSnapshot.child(child_key).child("sm_no").getValue().toString());
                                        }
                                        if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").exists()){

                                            phone2.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").getValue().toString());
                                        }
                                        else{
                                            phone2.setText("");
                                        }
                                        if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").exists()){

                                            phone3.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").getValue().toString());
                                        }
                                        else{
                                            phone3.setText("");
                                        }
                                        qualification.setText(dataSnapshot.child(child_key).child("qualifications").getValue().toString());
                                        mDatabase_fetch_specific_doctor_specialist = FirebaseDatabase.getInstance().getReference().child("Categories").child(dataSnapshot.child(child_key).child("specializations").getValue().toString());
                                        mDatabase_fetch_specific_doctor_specialist.addListenerForSingleValueEvent(new ValueEventListener() {

                                            public void onDataChange(DataSnapshot dataSnapshot){
                                                specializationsDetail.setText(specialize_text.getText().equals("Specialize")?dataSnapshot.child("englishVer").getValue().toString():dataSnapshot.child("burmeseVer").getValue().toString());
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                        final AvailabletimeDoctor_adapter availabletimeDoctor_adapter = new AvailabletimeDoctor_adapter(getActivity(),available_list,false,Professional_Profile.this);
                                        available_time_recycler.setAdapter(availabletimeDoctor_adapter);
                                        available_time_recycler.setHasFixedSize(true);
                                        available_time_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));

                                        final_sm_img_info_display.clear();
                                        if(dataSnapshot.child(child_key).child("sama_img").exists()){
                                            sm_img_uploadbtn.setText(getString(R.string.upload_sama_image));
                                            for (DataSnapshot postSnapshot: dataSnapshot.child(child_key).child("sama_img").getChildren()) {
                                                final_sm_img_info_display.add(postSnapshot.getValue(MedicalRecordImage.class));
                                            }
                                            SamaImgAdapterDisplay samaimgAdapterDisplay = new SamaImgAdapterDisplay(getActivity(),final_sm_img_info_display,sm_img_uploadbtn,true,Professional_Profile.this);
                                            sm_img_recyclerview_display.setAdapter(samaimgAdapterDisplay);
                                            sm_img_recyclerview_display.setHasFixedSize(true);
                                            sm_img_recyclerview_display.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
                                        }
                                        else{
                                            sm_img_uploadbtn.setText(getString(R.string.upload_sm_img));
                                            MedicalRecordImage myData = new MedicalRecordImage("Sama Image/Images have not submitted","");
                                            final_sm_img_info_display.add(myData);
                                            SamaImgAdapterDisplay samaimgAdapterDisplay = new SamaImgAdapterDisplay(getActivity(),final_sm_img_info_display,sm_img_uploadbtn,true,Professional_Profile.this);
                                            sm_img_recyclerview_display.setAdapter(samaimgAdapterDisplay);
                                            sm_img_recyclerview_display.setHasFixedSize(true);
                                            sm_img_recyclerview_display.setLayoutManager(new GridLayoutManager(getActivity(),1));
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            final_sm_img_info_display.clear();
                            ImageList.clear();
                            list.clear();
                            upload_count=0;
                        }
                    }).show();
        }
    }

    private String getExtension(Uri uri){
        ContentResolver cr = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

    public void imagePicker(View view) {
//        Intent intent = new Intent();
//        intent.setType("image/'");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode== Activity.RESULT_OK && data!=null && data.getData()!=null){
            imguri=data.getData();
            professional_image_url.setImageURI(imguri);
        }
        if(requestCode == PICK_IMAGE) {
            if(resultCode == Activity.RESULT_OK) {
                if(data.getClipData() != null) {
                    int countClipData = data.getClipData().getItemCount();
                    int currentImageSelect = 0;


                    while(currentImageSelect < countClipData) {
                        int img_name_increase = list.size();
                        String name = "SamaImg-" + (img_name_increase+1);
                        ImageUri = data.getClipData().getItemAt(currentImageSelect).getUri();
                        MedicalRecordImage mrec = new MedicalRecordImage(name, String.valueOf(ImageUri));
                        list.add(mrec);
                        ImageList.add(ImageUri);
                        currentImageSelect = currentImageSelect + 1;
                    }
//                        med_report_uploadbtn.setVisibility(View.GONE);
                } else {
                    int currentImageSelect = 0;
                    int img_name_increase = list.size();
                    String name = "SamaImg-" + (img_name_increase+1);
                    ImageUri = data.getData();
                    MedicalRecordImage mrec = new MedicalRecordImage(name, String.valueOf(ImageUri));
                    list.add(mrec);
                    ImageList.add(ImageUri);

//                    Toast.makeText(this, "Please select multiple images.", Toast.LENGTH_SHORT).show();
                }

                SamaImgAdapter samaImgAdapter = new SamaImgAdapter(getActivity(),list,sm_img_uploadbtn,true,Professional_Profile.this);
                sm_img_recyclerview.setAdapter(samaImgAdapter);
                sm_img_recyclerview.setHasFixedSize(true);
                sm_img_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));

                ////////////////////////////////////////////////////////////////////////////////////////
            }
        }
        else{

        }
    }
    public void onMethodCallback(int position,String delete_type) {
        if(delete_type.equals("sama_img")){
            ImageList.remove(position);
        }
    }

}
