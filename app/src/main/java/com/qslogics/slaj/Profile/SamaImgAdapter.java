package com.qslogics.slaj.Profile;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ortiz.touchview.TouchImageView;
import com.qslogics.slaj.Appointment.MedicalReportAdapter;
import com.qslogics.slaj.Appointment.Pending_Appointment_Detail;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SamaImgAdapter extends RecyclerView.Adapter<SamaImgAdapter.ImageValue> {
    ArrayList<MedicalRecordImage> mainlist =  new ArrayList<MedicalRecordImage>();
    Context context;
    ImageView removeIcon;
    Button med_report_uploadbtn;
    Boolean edit_check;
    Professional_Profile professional_profile_main;

    public SamaImgAdapter (Context ct, ArrayList<MedicalRecordImage> list, Button passed_med_report_uploadbtn, Boolean passed_edit_check,Professional_Profile professional_profile) {
        context = ct;
        mainlist = list;
        med_report_uploadbtn = passed_med_report_uploadbtn;
        edit_check = passed_edit_check;
        this.professional_profile_main = professional_profile;
    }

    @NonNull
    @Override
    public SamaImgAdapter.ImageValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_sama_img_adapter,parent,false);

        return new SamaImgAdapter.ImageValue(view);
    }

    public SamaImgAdapter() {

    }

    @Override
    public void onBindViewHolder(@NonNull SamaImgAdapter.ImageValue holder, final int position) {
        holder.rec_image_name.setText(mainlist.get(position).rec_image_name);
        Picasso.get().load(mainlist.get(position).rec_image_url).into(holder.rec_image_url);

        holder.rec_image_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog=new Dialog(v.getContext(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                dialog.setContentView(R.layout.dialog_image);
                TouchImageView bmImage = (TouchImageView) dialog.findViewById(R.id.img_receipt);
                bmImage.setImageDrawable(holder.rec_image_url.getDrawable());
                FloatingActionButton button=(FloatingActionButton) dialog.findViewById(R.id.button_dismiss);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.setCancelable(true);
                dialog.show();
            }
        });

        holder.remove_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
//                ((Professional_Profile)professional_profile_main).onMethodCallback(position,"sama_img");
            }
        });
    }
    public static interface AdapterCallback {
        void onMethodCallback();
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class ImageValue extends RecyclerView.ViewHolder {

        ImageView rec_image_url;
        TextView rec_image_name;
        ImageView remove_icon;


        public ImageValue(@NonNull View itemView) {
            super(itemView);
            rec_image_url = itemView.findViewById(R.id.rec_image);
            rec_image_name = itemView.findViewById(R.id.rec_name);
            remove_icon = itemView.findViewById(R.id.remove_icon);
            if(edit_check == false){
                remove_icon.setVisibility(View.GONE);
            }
            else{
                remove_icon.setVisibility(View.VISIBLE);
            }
        }
    }
    public void removeAt(int position) {
        mainlist.remove(position);
        notifyDataSetChanged();

        if(mainlist.isEmpty()){
            med_report_uploadbtn.setVisibility(View.VISIBLE);
        }

    }
}
