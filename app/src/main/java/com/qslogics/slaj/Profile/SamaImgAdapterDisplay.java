package com.qslogics.slaj.Profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ortiz.touchview.TouchImageView;
import com.qslogics.slaj.Entity.MedicalRecordImage;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SamaImgAdapterDisplay extends RecyclerView.Adapter<SamaImgAdapterDisplay.ImageValue> {
    ArrayList<MedicalRecordImage> mainlist =  new ArrayList<MedicalRecordImage>();
    Context context;
    ImageView removeIcon;
    Button med_report_uploadbtn;
    Boolean edit_check;
    Professional_Profile professional_profile_main;

    public SamaImgAdapterDisplay (Context ct, ArrayList<MedicalRecordImage> list, Button passed_med_report_uploadbtn, Boolean passed_edit_check,Professional_Profile professional_profile) {
        context = ct;
        mainlist = list;
        med_report_uploadbtn = passed_med_report_uploadbtn;
        edit_check = passed_edit_check;
        this.professional_profile_main = professional_profile;
    }

    @NonNull
    @Override
    public SamaImgAdapterDisplay.ImageValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_sama_img_adapter_display,parent,false);

        return new SamaImgAdapterDisplay.ImageValue(view);
    }

    public SamaImgAdapterDisplay() {

    }

    @Override
    public void onBindViewHolder(@NonNull SamaImgAdapterDisplay.ImageValue holder, final int position) {

        if(mainlist.get(0).rec_image_name == "Sama Image/Images have not submitted"){
            holder.rec_name_empty.setVisibility(View.VISIBLE);
            holder.rec_name_empty.setText(mainlist.get(0).rec_image_name);
            holder.rec_image_url.setVisibility(View.GONE);
            holder.rec_image_name.setVisibility(View.GONE);
        }
        else{
            holder.rec_name_empty.setVisibility(View.GONE);
            holder.rec_image_url.setVisibility(View.VISIBLE);
            holder.rec_image_name.setVisibility(View.VISIBLE);
            holder.rec_image_name.setText(mainlist.get(position).rec_image_name);
            Picasso.get().load(mainlist.get(position).rec_image_url).into(holder.rec_image_url);

            holder.rec_image_url.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog=new Dialog(v.getContext(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    dialog.setContentView(R.layout.dialog_image);
                    TouchImageView bmImage = (TouchImageView) dialog.findViewById(R.id.img_receipt);
                    bmImage.setImageDrawable(holder.rec_image_url.getDrawable());
                    FloatingActionButton button=(FloatingActionButton) dialog.findViewById(R.id.button_dismiss);

                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.setCancelable(true);
                    dialog.show();
                }
            });
        }



    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class ImageValue extends RecyclerView.ViewHolder {

        ImageView rec_image_url;
        TextView rec_image_name;
        TextView rec_name_empty;


        public ImageValue(@NonNull View itemView) {
            super(itemView);
            rec_image_url = itemView.findViewById(R.id.rec_image);
            rec_image_name = itemView.findViewById(R.id.rec_name);
            rec_name_empty = itemView.findViewById(R.id.rec_name_empty);
        }
    }

}