package com.qslogics.slaj.Profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.qslogics.slaj.Entity.MO_Entity;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class MO_Profile extends Fragment {

    String child_key;
    String img_url;
    String gender_value;
    private DatabaseReference mDatabase;

    ImageView mo_image_url;
    TextView name;
    TextView age;
    TextView age_title;
    TextView gender;
    Spinner spinnerGender;
    TextView email;
    TextView phone1;
    TextView phone2;
    TextView phone3;
    TextView address;
    RelativeLayout img_layout;
    RelativeLayout img_layout_edit;
    boolean edit_indicator;
    Button edit_btn_Click;
    public Uri imguri;
    SharedPreferences settings;

    HashMap<String, String> mo_phone_numbers = new HashMap<>();

    StorageReference mStorageRef;
    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    Uri downloadUri;

    String converted_age = "";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_m_o__profile, container, false);

        edit_btn_Click = rootView.findViewById(R.id.edit_btn_mo_profile);
        edit_indicator = false;

        edit_btn_Click.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                onEditClick((Button) view);
            }

        });

        settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        child_key =settings.getString("key", "child_key");
        mStorageRef = FirebaseStorage.getInstance().getReference("Images");

        mo_image_url = rootView.findViewById(R.id.mo_profile_image);
        //this is to reduce the img resoluction....the image wont appear if the img has high resolution....the reason is because of de.hdodenhof.circleimageview.CircleImageView
        //it work fine in normal image view
        mo_image_url.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        img_layout = rootView.findViewById(R.id.mo_profile_image_layout);
        img_layout_edit = rootView.findViewById(R.id.mo_profile_image_layout_edit);

        name = rootView.findViewById(R.id.mo_name);
        age = rootView.findViewById(R.id.age_text);
        age_title = rootView.findViewById(R.id.textView9);
        gender = rootView.findViewById(R.id.gender_text);
        spinnerGender = rootView.findViewById(R.id.gender_spinner_p_profile);
        email = rootView.findViewById(R.id.email_text);
        phone1 = rootView.findViewById(R.id.phoneno_1);
        phone2 = rootView.findViewById(R.id.phoneno_2);
        phone3 = rootView.findViewById(R.id.phoneno_3);
        address = rootView.findViewById(R.id.address_text);

        //to hide edit image icon at the start of the fragment
        img_layout_edit.setVisibility(View.GONE);

        name.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        name.setHint("");
        name.setFocusable(false);

        age.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        age.setFocusable(false);

        spinnerGender.setVisibility(View.GONE);

        img_layout_edit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                imagePicker(view);
            }

        });

        email.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        email.setHint("");
        email.setFocusable(false);

        address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        address.setHint("");
        address.setFocusable(false);

        phone2.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phone2.setHint("");
        phone2.setFocusable(false);

        phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        phone3.setHint("");
        phone3.setFocusable(false);


        mDatabase = FirebaseDatabase.getInstance().getReference().child("MO");

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    //these method are for gender spinner menthod
                    if (getContext() != null) {
                        ArrayAdapter<CharSequence> spinnerGenderAdapter = ArrayAdapter.createFromResource(getContext(),R.array.gender, android.R.layout.simple_spinner_dropdown_item);
                        spinnerGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerGender.setAdapter(spinnerGenderAdapter);
//                        spinnerGender.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, Patient_Profile.Gender.Gendertype));
                        if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                            spinnerGender.setSelection(0);
                        }
                        else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                            spinnerGender.setSelection(1);
                        }
//                        if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
//                            spinnerGender.setSelection(0);
//                        }
//                        else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
//                            spinnerGender.setSelection(1);
//                        }
                        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE); /* if you want your item to be white */
                                gender_value = parent.getSelectedItem().toString();
                                ///gender.setText(parent.getSelectedItem().toString());
                                String t = parent.getSelectedItem().toString();
                                if("Male".equals(t) || "ကျား".equals(t)){
                                    gender.setText(getString(R.string.male));
                                }
                                else if("Female".equals(t) || "မ".equals(t)){
                                    gender.setText ( getString ( R.string.female));
                                }
//                                if("Male".equals(t)){
//                                    gender.setText(getString(R.string.male));
//                                }
//                                else if("Female".equals(t)){
//                                    gender.setText ( getString ( R.string.female));
//                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                    ///////////////////////////////////////

                    //these codes are for saving edited info to firebase
                    img_url = dataSnapshot.child(child_key).child("mo_image_url").getValue().toString();

                    Picasso.get().load(dataSnapshot.child(child_key).child("mo_image_url").getValue().toString()).into(mo_image_url);
                    name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                    email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                    int age_length = dataSnapshot.child(child_key).child("age").getValue().toString().length( );
                    if(age_title.getText().toString().equals("အသက်")){
                        converted_age = "";
                        for(int i = 0 ; i < age_length ; i++){
                            int itwo = i;
                            Character raw_age = dataSnapshot.child(child_key).child("age").getValue().toString().charAt(i);
                            System.out.println(raw_age);
                            if(raw_age.equals('0')){
                                converted_age = converted_age+"၀";
                            }
                            else if(raw_age.equals('1')){
                                converted_age = converted_age+"၁";
                            }
                            else if(raw_age.equals('2')){
                                converted_age = converted_age+"၂";
                            }
                            else if(raw_age.equals('3')){
                                converted_age = converted_age+"၃";
                            }
                            else if(raw_age.equals('4')){
                                converted_age = converted_age+"၄";
                            }
                            else if(raw_age.equals('5')){
                                converted_age = converted_age+"၅";
                            }
                            else if(raw_age.equals('6')){
                                converted_age = converted_age+"၆";
                            }
                            else if(raw_age.equals('7')){
                                converted_age = converted_age+"၇";
                            }
                            else if(raw_age.equals('8')){
                                converted_age = converted_age+"၈";
                            }
                            else if(raw_age.equals('9')){
                                converted_age = converted_age+"၉";
                            }
                        }
                        System.out.println(converted_age);
                        age.setText(converted_age);
                    }
                    else{
                        age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                    }
//                    age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                    if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                        gender.setText(R.string.male);
                    }
                    else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                        gender.setText(R.string.female);
                    };
//                    gender.setText(dataSnapshot.child(child_key).child("gender").getValue().toString());
                    address.setText(dataSnapshot.child(child_key).child("address").getValue().toString());
                    phone1.setText(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_1").getValue().toString());
                    if(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_2").exists()){
                        phone2.setText(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_2").getValue().toString());
                    }
                    else{
                        phone2.setText("");
                    }
                    if(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_3").exists()){
                        phone3.setText(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_3").getValue().toString());
                    }
                    else{
                        phone3.setText("");
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onEditClick(View view) {
        if(edit_indicator == false) {
            edit_btn_Click.setText("Done");
            edit_indicator = true;
            Log.d("check_btn","clicked here");

            Spannable spannable = new SpannableString(phone2.getText().toString());
            img_layout.setAlpha(0.5f);
            img_layout_edit.setVisibility(View.VISIBLE);

            phone2.setText(spannable);

            spannable = new SpannableString(phone3.getText().toString());
            phone3.setText(spannable);

            //to make spinner gender visible on edit mode
            spinnerGender.setVisibility(View.VISIBLE);
            gender.setVisibility(View.GONE);
            //to make name editview visible on edit mode
            name.setFocusableInTouchMode(true);
            name.setHint("Enter your name");
            name.setBackgroundTintList(ColorStateList.valueOf(Color.DKGRAY));
            //to make name age visible on edit mode
            age.setFocusableInTouchMode(true);
            age.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));

            if(child_key.startsWith("+")){
                //to make email editview visible on edit mode
                email.setFocusableInTouchMode(true);
                email.setHint("Enter your email");
                email.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
            }

            //to make phone2 editview visible on edit mode
            phone2.setFocusableInTouchMode(true);
            phone2.setHint("Enter your 2nd Ph number");
            phone2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));

            //to make phone3 editview visible on edit mode
            phone3.setFocusableInTouchMode(true);
            phone3.setHint("Enter your 3rd Ph number");
            phone3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));
            //to make address editview visible on edit mode
            address.setFocusableInTouchMode(true);
            address.setHint("Enter your address detail");
            address.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ffffff")));


        }
        else{
            if(TextUtils.isEmpty(name.getText().toString())) {
                name.setError("Name field can not be empty");
                return;
            }

            if(TextUtils.isEmpty(age.getText().toString())) {
                age.setError("Age field can not be empty");
                return;
            }
            if(TextUtils.isEmpty(email.getText().toString())) {
                email.setError("Email field can not be empty");
                return;
            }
            else{
                new AlertDialog.Builder(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
                        .setTitle("Edit Profile Confirmation")
                        .setMessage("Are you sure you want to accept these changes and save it?")
                        .setCancelable(false)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final Dialog loading_dialog = new Dialog(getContext(),R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                                loading_dialog.setContentView(R.layout.appointment_edit_loading_alert_dialog);
                                ImageButton dialogButton = (ImageButton) loading_dialog.findViewById(R.id.video_call_btn);
                                loading_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                loading_dialog.show();

                                img_layout.setAlpha(1.0f);
                                img_layout_edit.setVisibility(View.GONE);

                                phone2.setMovementMethod(LinkMovementMethod.getInstance());
                                Spannable spannable = new SpannableString(phone2.getText().toString());
                                Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                                phone2.setText(spannable);

                                phone3.setMovementMethod(LinkMovementMethod.getInstance());
                                Spannable spannablethree = new SpannableString(phone3.getText().toString());
                                Linkify.addLinks(spannablethree, Linkify.PHONE_NUMBERS);
                                phone3.setText(spannablethree);

                                spinnerGender.setVisibility(View.GONE);
                                gender.setVisibility(View.VISIBLE);

                                name.setFocusable(false);
                                name.setHint("");
                                name.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                age.setFocusable(false);
                                age.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                email.setFocusable(false);
                                email.setHint("");
                                email.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                phone2.setFocusable(false);
                                phone2.setHint("");
                                phone2.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                phone3.setFocusable(false);
                                phone3.setHint("");
                                phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                address.setFocusable(false);
                                address.setHint("");
                                address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                mo_phone_numbers.put("ph_1",phone1.getText().toString());
                                mo_phone_numbers.put("ph_2",phone2.getText().toString());
                                mo_phone_numbers.put("ph_3",phone3.getText().toString());

                                if(imguri != null) {
                                    if (uploadTask != null && uploadTask.isInProgress()) {
                                        Toast.makeText(getActivity(), "Upload in progress", Toast.LENGTH_LONG).show();
                                    }
                                    else{
                                        final StorageReference Ref = mStorageRef.child("img_"+child_key+"."+getExtension(imguri));
                                        uploadTask = Ref.putFile(imguri);
                                        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                            @Override
                                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                if (!task.isSuccessful()) {
                                                    throw task.getException();
                                                }

                                                // Continue with the task to get the download URL
                                                return Ref.getDownloadUrl();
                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Uri> task) {
                                                if (task.isSuccessful()) {

                                                    downloadUri = task.getResult();
                                                    String real_gender_value = "";
                                                    if(gender.getText().toString().equals("ကျား")){
                                                        real_gender_value = "Male";
                                                    }
                                                    else if(gender.getText().toString().equals("မ")){
                                                        real_gender_value = "Female";
                                                    }
                                                    else{
                                                        real_gender_value = gender.getText().toString();
                                                    }

                                                    Toast.makeText(getActivity(),"Image Uploaded successfully",Toast.LENGTH_LONG).show();

                                                    MO_Entity mo_entity = new MO_Entity(name.getText().toString(), downloadUri.toString(), Integer.parseInt(age.getText().toString()),real_gender_value ,email.getText().toString(),mo_phone_numbers,address.getText().toString(),child_key,true);
                                                    FirebaseDatabase.getInstance().getReference().child("MO").child(child_key).setValue(mo_entity);

                                                    loading_dialog.dismiss();
                                                } else {
                                                    // Handle failures
                                                    // ...
                                                    Toast.makeText(getContext(),"Profile edit failed. Please try again",Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                                    }
                                }
                                else{
                                    String real_gender_value = "";
                                    if(gender.getText().toString().equals("ကျား")){
                                        real_gender_value = "Male";
                                    }
                                    else if(gender.getText().toString().equals("မ")){
                                        real_gender_value = "Female";
                                    }
                                    else{
                                        real_gender_value = gender.getText().toString();
                                    }
                                    MO_Entity mo_entity = new MO_Entity(name.getText().toString(), img_url, Integer.parseInt(age.getText().toString()),real_gender_value ,email.getText().toString(),mo_phone_numbers,address.getText().toString(),child_key,true);
                                    FirebaseDatabase.getInstance().getReference().child("MO").child(child_key).setValue(mo_entity);

                                    loading_dialog.dismiss();

//                                    SharedPreferences get_settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
//                                    SharedPreferences.Editor editor = get_settings.edit();
//                                    editor.putString("name", name.getText().toString());
//                                    editor.putString("drawer_img", img_url);
//                                    editor.commit();
//                                    Log.d("changed_name",settings.getString("name", "name"));
                                }
                            }
                        })
                        .setNegativeButton("Continue Editing", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                img_layout.setAlpha(1.0f);
                                img_layout_edit.setVisibility(View.GONE);

                                phone2.setMovementMethod(LinkMovementMethod.getInstance());
                                Spannable spannable = new SpannableString(phone2.getText().toString());
                                Linkify.addLinks(spannable, Linkify.PHONE_NUMBERS);
                                phone2.setText(spannable);

                                phone3.setMovementMethod(LinkMovementMethod.getInstance());
                                Spannable spannablethree = new SpannableString(phone3.getText().toString());
                                Linkify.addLinks(spannablethree, Linkify.PHONE_NUMBERS);
                                phone3.setText(spannablethree);

                                spinnerGender.setVisibility(View.GONE);
                                gender.setVisibility(View.VISIBLE);

                                name.setFocusable(false);
                                name.setHint("");
                                name.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                age.setFocusable(false);
                                age.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                email.setFocusable(false);
                                email.setHint("");
                                email.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                phone2.setFocusable(false);
                                phone2.setHint("");
                                phone2.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                phone3.setFocusable(false);
                                phone3.setHint("");
                                phone3.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                address.setFocusable(false);
                                address.setHint("");
                                address.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));

                                mDatabase.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()){
                                            //these method are for gender spinner menthod
                                            ArrayAdapter<CharSequence> spinnerGenderAdapter = ArrayAdapter.createFromResource(getContext(),R.array.gender, android.R.layout.simple_spinner_dropdown_item);
                                            spinnerGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            spinnerGender.setAdapter(spinnerGenderAdapter);
//                                            spinnerGender.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, Patient_Profile.Gender.Gendertype));
                                            if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                                                spinnerGender.setSelection(0);
                                            }
                                            else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                                                spinnerGender.setSelection(1);
                                            }
//                                            if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
//                                                spinnerGender.setSelection(0);
//                                            }
//                                            else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
//                                                spinnerGender.setSelection(1);
//                                            }
                                            spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                    ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE); /* if you want your item to be white */
                                                    gender_value = parent.getSelectedItem().toString();
                                                    ///gender.setText(parent.getSelectedItem().toString());
                                                    String t = parent.getSelectedItem().toString();
                                                    if("Male".equals(t) || "ကျား".equals(t)){
                                                        gender.setText(getString(R.string.male));
                                                    }
                                                    else if("Female".equals(t) || "မ".equals(t)){
                                                        gender.setText ( getString ( R.string.female));
                                                    }
//                                                    if("Male".equals(t)){
//                                                        gender.setText(getString(R.string.male));
//                                                    }
//                                                    else if("Female".equals(t)){
//                                                        gender.setText ( getString ( R.string.female));
//                                                    }
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> parent) {
                                                }
                                            });
                                            ///////////////////////////////////////

                                            //these codes are for saving edited info to firebase
                                            img_url = dataSnapshot.child(child_key).child("mo_image_url").getValue().toString();

                                            Picasso.get().load(dataSnapshot.child(child_key).child("mo_image_url").getValue().toString()).into(mo_image_url);
                                            name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                                            email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());
                                            int age_length = dataSnapshot.child(child_key).child("age").getValue().toString().length( );
                                            if(age_title.getText().toString().equals("အသက်")){
                                                converted_age = "";
                                                for(int i = 0 ; i < age_length ; i++){
                                                    int itwo = i;
                                                    Character raw_age = dataSnapshot.child(child_key).child("age").getValue().toString().charAt(i);
                                                    System.out.println(raw_age);
                                                    if(raw_age.equals('0')){
                                                        converted_age = converted_age+"၀";
                                                    }
                                                    else if(raw_age.equals('1')){
                                                        converted_age = converted_age+"၁";
                                                    }
                                                    else if(raw_age.equals('2')){
                                                        converted_age = converted_age+"၂";
                                                    }
                                                    else if(raw_age.equals('3')){
                                                        converted_age = converted_age+"၃";
                                                    }
                                                    else if(raw_age.equals('4')){
                                                        converted_age = converted_age+"၄";
                                                    }
                                                    else if(raw_age.equals('5')){
                                                        converted_age = converted_age+"၅";
                                                    }
                                                    else if(raw_age.equals('6')){
                                                        converted_age = converted_age+"၆";
                                                    }
                                                    else if(raw_age.equals('7')){
                                                        converted_age = converted_age+"၇";
                                                    }
                                                    else if(raw_age.equals('8')){
                                                        converted_age = converted_age+"၈";
                                                    }
                                                    else if(raw_age.equals('9')){
                                                        converted_age = converted_age+"၉";
                                                    }
                                                }
                                                System.out.println(converted_age);
                                                age.setText(converted_age);
                                            }
                                            else{
                                                age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                                            }
//                                            age.setText(dataSnapshot.child(child_key).child("age").getValue().toString());
                                            if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Male")){
                                                gender.setText(R.string.male);
                                            }
                                            else if(dataSnapshot.child(child_key).child("gender").getValue().toString().equals("Female")){
                                                gender.setText(R.string.female);
                                            };
//                                            gender.setText(dataSnapshot.child(child_key).child("gender").getValue().toString());
                                            address.setText(dataSnapshot.child(child_key).child("address").getValue().toString());
                                            phone1.setText(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_1").getValue().toString());
                                            if(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_2").exists()){
                                                phone2.setText(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_2").getValue().toString());
                                            }
                                            else{
                                                phone2.setText("");
                                            }
                                            if(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_3").exists()){
                                                phone3.setText(dataSnapshot.child(child_key).child("mo_phone_numbers").child("ph_3").getValue().toString());
                                            }
                                            else{
                                                phone3.setText("");
                                            }

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        }).show();
                edit_btn_Click.setText("Edit");
                edit_indicator = false;
            }
        }
    }

    public void imagePicker(View view) {
//        Intent intent = new Intent();
//        intent.setType("image/'");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode== Activity.RESULT_OK && data!=null && data.getData()!=null){
            imguri=data.getData();
            mo_image_url.setImageURI(imguri);
        }
    }

    private String getExtension(Uri uri){
        ContentResolver cr = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

}
