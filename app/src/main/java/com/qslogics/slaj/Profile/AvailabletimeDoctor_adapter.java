package com.qslogics.slaj.Profile;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.qslogics.slaj.Profile.Professional_Profile;
import com.qslogics.slaj.R;
import com.qslogics.slaj.ScreeningFiles.ScreeningFiles;

import java.util.ArrayList;

public class AvailabletimeDoctor_adapter extends RecyclerView.Adapter<AvailabletimeDoctor_adapter.AvailableValue> {


    ArrayList<String> mainlist=new ArrayList<String>();
    Boolean edit_check;
    Context context;
    Professional_Profile professional_profile_main;

    public AvailabletimeDoctor_adapter(Context ct, ArrayList<String> list, Boolean edit_check_received,Professional_Profile professional_profile_main_received){
        context = ct;
        mainlist = list;
        edit_check = edit_check_received;
        professional_profile_main = professional_profile_main_received;
    }

    public AvailabletimeDoctor_adapter() {

    }

    @NonNull
    @Override
    public AvailableValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_availabletime_doctor,parent,false);
        return new AvailableValue(view);
    }



    @Override
    public void onBindViewHolder(@NonNull AvailableValue holder, final int position) {
        holder.available_time_text.setText(mainlist.get(position));
        holder.available_time_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_access_time_black_24dp, 0, 0, 0);

        holder.time_remove_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
            }
        });
    }

    public static interface AdapterCallback {
        void onMethodCallback();
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class AvailableValue extends RecyclerView.ViewHolder {

        TextView available_time_text;
        ImageButton time_remove_btn;

        public AvailableValue(@NonNull View itemView) {
            super(itemView);
            available_time_text= itemView.findViewById(R.id.available_time_text);
            time_remove_btn = itemView.findViewById(R.id.time_remove_btn);
            if(edit_check == false){
                time_remove_btn.setVisibility(View.GONE);
            }
            else{
                time_remove_btn.setVisibility(View.VISIBLE);
            }
        }
    }
    public void removeAt(int position) {
        mainlist.remove(position);
        notifyDataSetChanged();
    }
}
