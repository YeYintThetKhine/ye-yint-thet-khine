package com.qslogics.slaj.Login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.R;
import com.qslogics.slaj.Register.CountryData;
import com.qslogics.slaj.Register.PhOTPActivity;
import com.qslogics.slaj.Register.PhRegisterActivity;
import com.qslogics.slaj.Register.Register;

public class Login_phone extends AppCompatActivity {

    DatabaseReference dbref;
    String phonenumber;
    private Spinner spinner;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_phone);
        editText = findViewById(R.id.editTextPhone);
        spinner = findViewById(R.id.spinnerCountries);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CountryData.countryNames));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#72bb53")); /* if you want your item to be white */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void phSignin(View view) {
        String code = CountryData.countryAreaCodes[spinner.getSelectedItemPosition()];
        String number = editText.getText().toString().trim();
        phonenumber = "+" + code + number;

        if (number.isEmpty() || number.length() < 10) {
            editText.setError("Valid number is required");
            editText.requestFocus();
            return;
        }
        else
        {
            dbref= FirebaseDatabase.getInstance().getReference().child("users").child("phRegisteredUsers").child(phonenumber);

            dbref.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                       // String name = dataSnapshot.child("fullname").getValue().toString();
                        //String usertype = dataSnapshot.child("usertype").getValue().toString();
                        //String key = phonenumber;
                        //storePreferences(name, usertype, key);
                        Intent intent = new Intent(Login_phone.this, PhOTPActivity.class);
                        intent.putExtra("phonenumber", phonenumber);
                        startActivity(intent);
                    }
                    else{
                        new AlertDialog.Builder(Login_phone.this)
                                .setTitle("Sign In Failed!")
                                .setMessage("This Phone number is not in our registration list. Please undergo registration process with " + phonenumber + " and try to sign in later")
                                .setCancelable(false)
                                .setPositiveButton("Register Now", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Login_phone.this, PhRegisterActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                        Log.d("no data","There is no data");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("databaseError",databaseError.getMessage());
                }

            });
        }


//        Intent intent = new Intent(this, PhRegisterActivity.class);
//        startActivity(intent);
    }

    public void storePreferences(String name, String usertype, String key) {
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("name", name);
        editor.putString("type", usertype);
        editor.putString("key", key);
        editor.commit();
    }


    public void phRegister(View view) {
        Intent intent = new Intent(this, PhRegisterActivity.class);
        startActivity(intent);
    }

    public void emailRegister(View view) {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }

}
