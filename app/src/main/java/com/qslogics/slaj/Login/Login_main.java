package com.qslogics.slaj.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.auth.FirebaseAuth;
import com.qslogics.slaj.Home.MainActivity;
import com.qslogics.slaj.R;
import com.qslogics.slaj.Register.PhRegisterActivity;
import com.qslogics.slaj.Register.Register;

import java.util.Locale;


public class Login_main extends AppCompatActivity {
    Button btnLogin, btnGotoRegEmailPage;
    FirebaseAuth fAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        fAuth = FirebaseAuth.getInstance();
        //redirect to main activity if user is already logged in
        if(fAuth.getCurrentUser() != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
        Spinner spinner = (Spinner) findViewById(R.id.spinner_menu);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener () {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                if (pos == 1) {

                    Toast.makeText(parent.getContext(),
                            "You have selected Myanmar", Toast.LENGTH_SHORT)
                            .show();
                    saveLocale("my");
                } else if (pos == 2) {

                    Toast.makeText(parent.getContext(),
                            "You have selected English", Toast.LENGTH_SHORT)
                            .show();
                    saveLocale("en");
                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });

    }

    public void saveLocale(String language) {
        SharedPreferences shp = getSharedPreferences( "pref",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shp.edit();
        editor.putString("USER_LANGUAGE", language);
        editor.apply();
        LoadLanguage ();
        finish();
        startActivity(getIntent());
    }

    public void LoadLanguage(){
        SharedPreferences shp = getSharedPreferences( "pref",Context.MODE_PRIVATE);
        String language = shp.getString("USER_LANGUAGE","");
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }
    public void emailRegister(View view) {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }

    public void emailSignin(View view) {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void phSignin(View view) {
        Intent intent = new Intent(this, Login_phone.class);
        startActivity(intent);
    }

    public void phRegister(View view) {
        Intent intent = new Intent(this, PhRegisterActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onStart() {
        super.onStart();

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        } else {
            FirebaseAuth.getInstance().signOut();
        }
    }
}
