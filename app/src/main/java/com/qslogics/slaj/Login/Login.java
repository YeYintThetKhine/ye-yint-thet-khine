package com.qslogics.slaj.Login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Appointment.Pending_Appointment_Detail;
import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.Entity.Patient;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.Entity.User;
import com.qslogics.slaj.Home.MainActivity;
import com.qslogics.slaj.R;
import com.qslogics.slaj.Register.PhRegisterActivity;
import com.qslogics.slaj.Register.Register;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Login extends AppCompatActivity {
    Button btnLogin, btnGotoRegEmailPage;
    EditText mEmail, mPassword;
    private FirebaseAuth fAuthLogin;
    DatabaseReference rootRef;
    FirebaseDatabase database;
    TextView dialogText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btn_signin);
        btnGotoRegEmailPage = findViewById(R.id.btn_mail);
        mEmail = findViewById(R.id.login_email_text);
        mPassword = findViewById(R.id.login_password_text);
        fAuthLogin = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();

        //placeholder image urls
        final String patient_male= "https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fmale_patient.jpg?alt=media&token=dab38ccc-abcd-47cb-9df2-054246e50a8a";
        final String patient_female="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Ffemale_patient.jpg?alt=media&token=a602cdaf-9ea9-436a-8e8b-d48d17ba2a2d";

        final String professional_male="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2FDoctor2.jpg?alt=media&token=2d9ed2be-c09d-41d5-ba5a-886e58f76d31";
        final String professional_female="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fdoctor1.jpg?alt=media&token=1ba98190-b469-4ed2-8325-96bb4fc5ce20";

        final String caregiver_male="https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fcaregiver.jpg?alt=media&token=29d264c6-f15d-4e93-8c94-b4e87dfcc270";
        final String caregiver_female = "https://firebasestorage.googleapis.com/v0/b/seinlanarauga-1320f.appspot.com/o/Images%2Fcaregiver2.jpg?alt=media&token=47bf330a-e29b-482d-8bbe-96c00d29601c";


        final HashMap<String, String> phoneNumbersMap = new HashMap<>();
        phoneNumbersMap.put("ph_1","Phone numbers not provided yet.");

        final HashMap<String, String> mrecordMap = new HashMap<>();
        mrecordMap.put("mr_1", "Medical records not provided yet.");

        final ArrayList<String> availableTimeMap = new ArrayList<String>();
        availableTimeMap.add("Available times not provided.");

        final HashMap<String, String> specializationMap = new HashMap<>();
        specializationMap.put("sp_1", "Specializations not provided.");

        final HashMap<String, String> vDocumentsMap = new HashMap<>();
        vDocumentsMap.put("vd_1", "No documents yet.");


        //redirect to main activity if user is already logged in
        if(fAuthLogin.getCurrentUser() != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }

        //go to register page (email)
        btnGotoRegEmailPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Register.class));
            }
        });

        //login method
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();

                if(TextUtils.isEmpty(email)) {
                    mEmail.setError("Email is required.");
                    return;
                }
                if(TextUtils.isEmpty(password)) {
                    mPassword.setError("Password is required.");
                    return;
                }

                final Dialog dialog = new Dialog(Login.this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background);
                dialog.setContentView(R.layout.appointment_edit_loading_alert_dialog);
                dialogText = dialog.findViewById(R.id.textcontent);
                dialogText.setText("Logging in...");
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();


                fAuthLogin.signInWithEmailAndPassword(email, password).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            final FirebaseUser user = fAuthLogin.getCurrentUser();
                            if(user != null) {
                                if(user.isEmailVerified()) {
                                    String rmDotPath = email.replace(".", "");
                                    String rmASignPath = rmDotPath.replace("@", "");
                                    final String path = rmASignPath;


                                    //set verified to true
                                    //rootRef.child("users").child("emailRegisteredUsers").child(path).child("verified").setValue(true);
                                    rootRef.child("users").child("emailRegisteredUsers").child(path).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            final User userObject = dataSnapshot.getValue(User.class);
                                            if(userObject.getVerified()) {
                                                switch(userObject.getUsertype()) {
                                                    case "Patient":
                                                        rootRef.child("Patients").child(path).child("active").setValue(true);
                                                        break;
                                                    case "Professional":
                                                        rootRef.child("Professionals").child(path).child("active").setValue(true);
                                                        break;
                                                    case "Caregivers":
                                                        rootRef.child("Caregivers").child(path).child("active").setValue(true);
                                                        break;
                                                    case "MO":
                                                        rootRef.child("MO").child(path).child("active").setValue(true);
                                                        break;
                                                }
                                                storePreferences(userObject, path);
                                                dialog.dismiss();
                                                finish();
                                                startActivity(new Intent(getApplicationContext(), MainActivity.class));

                                            } else {
                                                switch(userObject.getUsertype()) {
                                                    case "Patient":
                                                        phoneNumbersMap.put("ph_1", userObject.getPhnumber());
                                                        specializationMap.put("sp_1", "General");
//                                                        String uniqueID = UUID.randomUUID().toString();
//                                                        String[] uuidArr = uniqueID.split("-");
                                                        long timestampmilisec = System.currentTimeMillis();
                                                        String str_timestampmilisec = String.valueOf(timestampmilisec);

                                                        Patient patient = new Patient(userObject.getFullname(),userObject.getEmail(), userObject.getGender(), userObject.getAge(), userObject.getUsertype(), userObject.getGender().equals("Male") ? (patient_male):(patient_female), "", false, false, phoneNumbersMap, mrecordMap, path, str_timestampmilisec, "");
                                                        rootRef.child("Patients").child(path).setValue(patient);
                                                        rootRef.child("users").child("emailRegisteredUsers").child(path).child("verified").setValue(true);
                                                        rootRef.child("Patients").child(path).child("active").setValue(true);
                                                        break;
                                                    case "Professional":
                                                        phoneNumbersMap.put("ph_1", userObject.getPhnumber());
                                                        specializationMap.put("sp_1", "General");
                                                        Professional professional = new Professional(userObject.getFullname(), userObject.getEmail(), userObject.getGender(), userObject.getAge(), userObject.getUsertype(), "", "Not provided yet.", "", userObject.getGender().equals("Male")  ? (professional_male):(professional_female), false, phoneNumbersMap, availableTimeMap, vDocumentsMap, "Specialization was not choosen", path);
                                                        rootRef.child("Professionals").child(path).setValue(professional);
                                                        rootRef.child("users").child("emailRegisteredUsers").child(path).child("verified").setValue(true);
                                                        rootRef.child("Professionals").child(path).child("active").setValue(true);
                                                        break;
                                                    case "Caregiver":
                                                        phoneNumbersMap.put("ph_1", userObject.getPhnumber());
                                                        specializationMap.put("sp_1", "General");
                                                        Caregiver caregiver = new Caregiver(userObject.getFullname(), userObject.getEmail(), userObject.getGender(), userObject.getAge(), userObject.getUsertype(), "", "", userObject.getGender().equals("Male")  ? (caregiver_male) : (caregiver_female) , false, phoneNumbersMap, path);
                                                        rootRef.child("Caregivers").child(path).setValue(caregiver);
                                                        rootRef.child("users").child("emailRegisteredUsers").child(path).child("verified").setValue(true);
                                                        rootRef.child("Caregivers").child(path).child("active").setValue(true);
                                                        break;
                                                }
                                                storePreferences(userObject, path);
                                                dialog.dismiss();
                                                finish();
                                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                } else {
                                    dialog.dismiss();
                                    Toast.makeText(Login.this, "Please verify your email to continue.",Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            dialog.dismiss();
                            Toast.makeText(Login.this, "Error : " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    public void storePreferences(User user, String key) {
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("name", user.getFullname());
        editor.putString("type", user.getUsertype());
        editor.putString("key", key);
        editor.commit();
    }

    public void removePreferences() {
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    public void phRegister(View view) {
        Intent intent = new Intent(this, PhRegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Login activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

}
