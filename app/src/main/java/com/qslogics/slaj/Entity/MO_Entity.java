package com.qslogics.slaj.Entity;

import java.util.Map;

public class MO_Entity {
    private String name;
    private String mo_image_url;
    private int age;
    private String gender;
    private String email;
    private Map<String, String> mo_phone_numbers;
    private String address;
    private String child_key;
    private Boolean active;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMo_image_url() {
        return mo_image_url;
    }

    public void setMo_image_url(String mo_image_url) {
        this.mo_image_url = mo_image_url;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> getMo_phone_numbers() {
        return mo_phone_numbers;
    }

    public void setMo_phone_numbers(Map<String, String> mo_phone_numbers) {
        this.mo_phone_numbers = mo_phone_numbers;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getChild_key() {
        return child_key;
    }

    public void setChild_key(String child_key) {
        this.child_key = child_key;
    }


    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public MO_Entity() {}


    public MO_Entity(String name, String mo_image_url, int age, String gender, String email, Map<String, String> mo_phone_numbers, String address,String child_key,Boolean active) {
        this.name = name;
        this.mo_image_url = mo_image_url;
        this.age = age;
        this.gender = gender;
        this.email = email;
        this.mo_phone_numbers = mo_phone_numbers;
        this.address = address;
        this.child_key = child_key;
        this.active = active;
    }

}

