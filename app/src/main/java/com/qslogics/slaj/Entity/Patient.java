package com.qslogics.slaj.Entity;

import java.util.Map;

public class Patient {
    private String name;
    private String patient_image_url;
    private String child_key;
    private  Map<String, String> patient_phone_numbers;
    private String email;
    private String gender;
    private int age;
    private String address;
    private String type;
    private Map<String, String> medical_record_images;
    private Boolean isActive;
    private Boolean hasAppointment;
    private String id;
    private String dob;

    public Patient() {

    }

    public Patient(String name, String email, String gender, int age, String type, String patient_image_url, String address, Boolean isActive, Boolean hasAppointment, Map<String, String> patient_phone_numbers, Map<String, String> medical_record_images, String child_key, String id, String dob) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.age = age;
        this.type = type;
        this.patient_image_url = patient_image_url;
        this.address = address;
        this.isActive = isActive;
        this.hasAppointment = hasAppointment;
        this.patient_phone_numbers = patient_phone_numbers;
        this.medical_record_images = medical_record_images;
        this.child_key = child_key;
        this.id = id;
        this.dob = dob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatient_image_url() {
        return patient_image_url;
    }

    public void setPatient_image_url(String patient_image_url) {
        this.patient_image_url = patient_image_url;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getHasAppointment() {
        return hasAppointment;
    }

    public void setHasAppointment(Boolean hasAppointment) {
        this.hasAppointment = hasAppointment;
    }

    public Map<String, String> getPatient_phone_numbers() {
        return patient_phone_numbers;
    }

    public void setPatient_phone_numbers(Map<String, String> patient_phone_numbers) {
        this.patient_phone_numbers = patient_phone_numbers;
    }

    public Map<String, String> getMedical_record_images() {
        return medical_record_images;
    }

    public void setMedical_record_images(Map<String, String> medical_record_images) {
        this.medical_record_images = medical_record_images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChild_key() {
        return child_key;
    }

    public void setChild_key(String child_key) {
        this.child_key = child_key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
