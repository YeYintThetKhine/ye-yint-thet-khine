package com.qslogics.slaj.Entity;

import java.util.Map;

public class Caregiver {
    private String name;
    private String child_key;
    private String caregiver_image_url;
    private String caregiver_image_url_name;
    private Map<String,String> caregiver_phone_numbers;
    private String email;
    private String gender;
    private int age;
    private String type;
    private String address;
    private String support_role;
    private Boolean isActive;

    public Caregiver() {

    }

    public Caregiver(String name, String email, String gender, int age, String type, String address, String support_role, String caregiver_image_url,Boolean isActive, Map<String,String> caregiver_phone_numbers, String child_key) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.age = age;
        this.type = type;
        this.address = address;
        this.support_role =support_role;
        this.caregiver_image_url = caregiver_image_url;
        this.caregiver_image_url_name = caregiver_image_url_name;
        this.isActive = isActive;
        this.caregiver_phone_numbers = caregiver_phone_numbers;
        this.child_key = child_key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaregiver_image_url() {
        return caregiver_image_url;
    }

    public void setCaregiver_image_url(String caregiver_image_url) {
        this.caregiver_image_url = caregiver_image_url;
    }

    public String getCaregiver_image_url_name() {
        return caregiver_image_url_name;
    }

    public void setCaregiver_image_url_name(String caregiver_image_url_name) {
        this.caregiver_image_url_name = caregiver_image_url_name;
    }

    public Map<String, String> getCaregiver_phone_numbers() {
        return caregiver_phone_numbers;
    }

    public void setCaregiver_phone_numbers(Map<String, String> caregiver_phone_numbers) {
        this.caregiver_phone_numbers = caregiver_phone_numbers;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSupport_role() {
        return support_role;
    }

    public void setSupport_role(String support_role) {
        this.support_role = support_role;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChild_key() {
        return child_key;
    }

    public void setChild_key(String child_key) {
        this.child_key = child_key;
    }
}
