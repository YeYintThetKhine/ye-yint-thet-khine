package com.qslogics.slaj.Entity;

public class Prescription_Detail_Entity {
    String afternoon;
    String bedtime;
    String days_to_take;
    int dose;
    String med_name;
    String morning;
    String unit;
    String remark;

    public String getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(String afternoon) {
        this.afternoon = afternoon;
    }

    public String getBedtime() {
        return bedtime;
    }

    public void setBedtime(String bedtime) {
        this.bedtime = bedtime;
    }

    public String getDays_to_take() {
        return days_to_take;
    }

    public void setDays_to_take(String days_to_take) {
        this.days_to_take = days_to_take;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }
    public String getMed_name() {
        return med_name;
    }

    public void setMed_name(String med_name) {
        this.med_name = med_name;
    }

    public String getMorning() {
        return morning;
    }

    public void setMorning(String morning) {
        this.morning = morning;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public Prescription_Detail_Entity() {

    }

    public Prescription_Detail_Entity(String afternoon, String bedtime, String days_to_take, int dose, String med_name, String morning, String unit,String remark){
        this.afternoon = afternoon;
        this.bedtime = bedtime;
        this.days_to_take = days_to_take;
        this.dose = dose;
        this.med_name = med_name;
        this.morning = morning;
        this.unit = unit;
        this.remark = remark;
    }

}
