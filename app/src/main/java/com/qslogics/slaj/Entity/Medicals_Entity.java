package com.qslogics.slaj.Entity;

import java.util.HashMap;

public class Medicals_Entity {

    public boolean active;
    public long age;
    public String child_key;
    public HashMap<String, String > available_time;
    public String email;
    public String experience;
    public String gender;
    public String name;
    public String pro_image_url;
    public HashMap<String, String > pro_phone_numbers;
    public String qualifications;
    public String sm_no;
    public boolean sm_verified;
    public HashMap<String, String > specializations;
    public String type;
    public HashMap<String, String > verfied_documents;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public HashMap<String, String> getAvailable_time() {
        return available_time;
    }

    public void setAvailable_time(HashMap<String, String> available_time) {
        this.available_time = available_time;
    }

    public String getChild_key() {
        return child_key;
    }

    public void setChild_key(String child_key) {
        this.child_key = child_key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPro_image_url() {
        return pro_image_url;
    }

    public void setPro_image_url(String pro_image_url) {
        this.pro_image_url = pro_image_url;
    }

    public HashMap<String, String> getPro_phone_numbers() {
        return pro_phone_numbers;
    }

    public void setPro_phone_numbers(HashMap<String, String> pro_phone_numbers) {
        this.pro_phone_numbers = pro_phone_numbers;
    }

    public String getQualifications() {
        return qualifications;
    }

    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }

    public String getSm_no() {
        return sm_no;
    }

    public void setSm_no(String sm_no) {
        this.sm_no = sm_no;
    }

    public boolean isSm_verified() {
        return sm_verified;
    }

    public void setSm_verified(boolean sm_verified) {
        this.sm_verified = sm_verified;
    }

    public HashMap<String, String> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(HashMap<String, String> specializations) {
        this.specializations = specializations;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HashMap<String, String> getVerfied_documents() {
        return verfied_documents;
    }

    public void setVerfied_documents(HashMap<String, String> verfied_documents) {
        this.verfied_documents = verfied_documents;
    }

    public Medicals_Entity() {}

    public Medicals_Entity(boolean active,long age,HashMap<String, String > available_time,String child_key,String email,String experience,String gender,String name,String pro_image_url,HashMap<String, String > pro_phone_numbers,String qualifications,String sm_no,boolean sm_verified,HashMap<String, String > specializations,String type,HashMap<String, String > verfied_documents) {
        this.active = active;
        this.age = age;
        this.available_time = available_time;
        this.child_key = child_key;
        this.email = email;
        this.experience = experience;
        this.gender = gender;
        this.name = name;
        this.pro_image_url = pro_image_url;
        this.pro_phone_numbers = pro_phone_numbers;
        this.qualifications = qualifications;
        this.sm_no = sm_no;
        this.sm_verified = sm_verified;
        this.specializations = specializations;
        this.type = type;
        this.verfied_documents = verfied_documents;
    }
}
