package com.qslogics.slaj.Entity;

public class Other_Pro_Entity {

    public long age;
    public String email;
    public String fullname;
    public String gender;
    public String phnumber;
    public String img_url;

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhnumber() {
        return phnumber;
    }

    public void setPhnumber(String phnumber) {
        this.phnumber = phnumber;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Other_Pro_Entity() {}


    public Other_Pro_Entity(long age,String email,String img_url,String fullname,String gender,String phnumber) {
        this.age = age;
        this.email = email;
        this.img_url = img_url;
        this.fullname = fullname;
        this.gender = gender;
        this.phnumber = phnumber;

    }

}
