package com.qslogics.slaj.Entity;

public class User {
    public String fullname;
    public String usertype;
    public String gender;
    public int age;
    public String email;
    public String phnumber;
    public Boolean verified;
    public String timestamp;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhnumber() {
        return phnumber;
    }

    public void setPhnumber(String phnumber) {
        this.phnumber = phnumber;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    public User() {

    }

    public User(String fullname, String usertype, String gender, int age, String email, String phnumber, Boolean verified, String timestamp) {
        this.fullname = fullname;
        this.usertype = usertype;
        this.gender = gender;
        this.age= age;
        this.email = email;
        this.phnumber = phnumber;
        this.verified = verified;
        this.timestamp = timestamp;
    }


}
