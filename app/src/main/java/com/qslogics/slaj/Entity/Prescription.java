package com.qslogics.slaj.Entity;

public class Prescription {

    private String child_key;
    private String date;
    private String doctor_name;
    private String patient_name;
    private String prescription;
    private String time;

    public String getChild_key() {
        return child_key;
    }

    public void setChild_key(String child_key) {
        this.child_key = child_key;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public Prescription() {

    }

    public Prescription(String child_key,String date, String doctor_name, String patient_name, String prescription, String time) {
        this.child_key = child_key;
        this.date = date;
        this.doctor_name = doctor_name;
        this.patient_name = patient_name;
        this.prescription= prescription;
        this.time = time;
    }

}
