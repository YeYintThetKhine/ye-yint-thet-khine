package com.qslogics.slaj.Entity;

import java.io.Serializable;
import java.util.Date;

public class MedicalRecordImage implements Serializable {
    public String rec_image_name;
    public String rec_image_url;
    private Date rec_date;

    public String getRec_image_name() {
        return rec_image_name;
    }

    public void setRec_image_name(String rec_image_name) {
        this.rec_image_name = rec_image_name;
    }

    public String getRec_image_url() {
        return rec_image_url;
    }

    public void setRec_image_url(String rec_image_url) {
        this.rec_image_url = rec_image_url;
    }

    public Date getRec_date() {
        return rec_date;
    }

    public void setRec_date(Date rec_date) {
        this.rec_date = rec_date;
    }


    public MedicalRecordImage() {

    }

    public MedicalRecordImage(String name, String url) {
        this.rec_image_name = name;
        this.rec_image_url = url;
    }


}
