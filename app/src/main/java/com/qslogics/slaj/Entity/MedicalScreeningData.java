package com.qslogics.slaj.Entity;

import java.io.Serializable;
import java.util.ArrayList;

public class MedicalScreeningData implements Serializable {
    private String patientKey;
    private String professionalKey;
    private String professionalName;
    private String Appointed_professionalKey;
    private String moKey;
    private String type;

    private String maritalStatus;
    private String address;
    private String temperature;
    private String medicalNotes;

    private boolean haveCough;
    private boolean haveTroubleBreathing;
    private boolean haveTravelHistory;

    private boolean checked;
    private String screeningDate;
    private String checkedDate;
    private String appointmentDate;
    private String appointmentTime;
    private String patientName;

    private ArrayList<MedicalRecordImage> medicalRecords;

    private ArrayList<MedicalRecordImage> medicalReports;

    public MedicalScreeningData() {

    }


    public MedicalScreeningData(String patientKey, String patientName,String professionalKey, String professionalName, String Appointed_professionalKey, String moKey, String maritalStatus, String address, String temperature, String medicalNotes, boolean haveCough, boolean haveTroubleBreathing, boolean haveTravelHistory, boolean checked, String screeningDate, String checkedDate, ArrayList<MedicalRecordImage> medicalRecords, String type, String appointmentDate, String appointmentTime, ArrayList<MedicalRecordImage> medicalReports) {

        this.patientKey = patientKey;
        this.professionalKey = professionalKey;
        this.professionalName = professionalName;
        this.Appointed_professionalKey = Appointed_professionalKey;
        this.moKey = moKey;
        this.maritalStatus = maritalStatus;
        this.address = address;
        this.temperature = temperature;
        this.medicalNotes = medicalNotes;
        this.haveCough = haveCough;
        this.haveTroubleBreathing = haveTroubleBreathing;
        this.haveTravelHistory = haveTravelHistory;
        this.checked = checked;
        this.screeningDate = screeningDate;
        this.checkedDate = checkedDate;
        this.medicalRecords = medicalRecords;
        this.type = type;
        this.appointmentDate = appointmentDate;
        this.appointmentTime = appointmentTime;
        this.medicalReports = medicalReports;
        this.patientName = patientName;
        this.professionalName = professionalName;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getProfessionalKey() {
        return professionalKey;
    }

    public void setProfessionalKey(String professionalKey) {
        this.professionalKey = professionalKey;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName;
    }

    public String getAppointed_professionalKey() {
        return Appointed_professionalKey;
    }

    public void setAppointed_professionalKey(String appointed_professionalKey) {
        Appointed_professionalKey = appointed_professionalKey;
    }

    public String getMoKey() {
        return moKey;
    }

    public void setMoKey(String moKey) {
        this.moKey = moKey;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getMedicalNotes() {
        return medicalNotes;
    }

    public void setMedicalNotes(String medicalNotes) {
        this.medicalNotes = medicalNotes;
    }

    public boolean isHaveCough() {
        return haveCough;
    }

    public void setHaveCough(boolean haveCough) {
        this.haveCough = haveCough;
    }

    public boolean isHaveTroubleBreathing() {
        return haveTroubleBreathing;
    }

    public void setHaveTroubleBreathing(boolean haveTroubleBreathing) {
        this.haveTroubleBreathing = haveTroubleBreathing;
    }

    public boolean isHaveTravelHistory() {
        return haveTravelHistory;
    }

    public void setHaveTravelHistory(boolean haveTravelHistory) {
        this.haveTravelHistory = haveTravelHistory;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getScreeningDate() {
        return screeningDate;
    }

    public void setScreeningDate(String screeningDate) {
        this.screeningDate = screeningDate;
    }

    public String getCheckedDate() {
        return checkedDate;
    }

    public void setCheckedDate(String checkedDate) {
        this.checkedDate = checkedDate;
    }

    public ArrayList<MedicalRecordImage> getMedicalRecords() {
        return medicalRecords;
    }

    public void setMedicalRecords(ArrayList<MedicalRecordImage> medicalRecords) {
        this.medicalRecords = medicalRecords;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public ArrayList<MedicalRecordImage> getMedicalReports() {
        return medicalReports;
    }

    public void setMedicalReports(ArrayList<MedicalRecordImage> medicalReports) {
        this.medicalReports = medicalReports;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
}
