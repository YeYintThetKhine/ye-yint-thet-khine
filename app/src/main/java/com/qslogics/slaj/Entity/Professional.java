package com.qslogics.slaj.Entity;

import java.util.ArrayList;
import java.util.Map;

public class Professional {
    private String name;
    private String child_key;
    public String pro_image_url;
    private Map<String, String> pro_phone_numbers;
    private String email;
    private String gender;
    private int age;
    private String type;
    private ArrayList<String> available_time;
    private String sm_no;
    private Map<String, String> verfied_documents;
    private String qualifications;
    private String specializations;
    private String experience;
    private Boolean isActive;

    public Professional() {

    }

    public Professional(String name, String email, String gender, int age, String type, String sm_no, String qualifications, String experience, String pro_image_url, Boolean isActive, Map<String, String> pro_phone_numbers, ArrayList<String> available_time, Map<String, String> verfied_documents, String specializations, String child_key) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.age = age;
        this.type = type;
        this.sm_no = sm_no;
        this.qualifications = qualifications;
        this.experience = experience;
        this.pro_image_url = pro_image_url;
        this.isActive = isActive;
        this.pro_phone_numbers = pro_phone_numbers;
        this.available_time = available_time;
        this.verfied_documents = verfied_documents;
        this.specializations =specializations ;
        this.child_key = child_key;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPro_image_url() {
        return pro_image_url;
    }

    public void setPro_image_url(String pro_image_url) {
        this.pro_image_url = pro_image_url;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSm_no() {
        return sm_no;
    }

    public void setSm_no(String sm_no) {
        this.sm_no = sm_no;
    }

    public Map<String, String> getVerfied_documents() {
        return verfied_documents;
    }

    public void setVerfied_documents(Map<String, String> verfied_documents) {
        this.verfied_documents = verfied_documents;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<String> getAvailable_time() {
        return available_time;
    }

    public void setAvailable_time(ArrayList<String> available_time) {
        this.available_time = available_time;
    }

    public Map<String, String> getPro_phone_numbers() {
        return pro_phone_numbers;
    }

    public void setPro_phone_numbers(Map<String, String> pro_phone_numbers) {
        this.pro_phone_numbers = pro_phone_numbers;
    }

    public String getQualifications() {
        return qualifications;
    }

    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }

    public String getSpecializations() {
        return specializations;
    }

    public void setSpecializations(String specializations) {
        this.specializations = specializations;
    }

    public String getChild_key() {
        return child_key;
    }

    public void setChild_key(String child_key) {
        this.child_key = child_key;
    }
}
