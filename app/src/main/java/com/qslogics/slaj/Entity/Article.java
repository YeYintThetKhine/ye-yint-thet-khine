package com.qslogics.slaj.Entity;

public class Article {

    public String article_name;
    public String article_img_url;
    public String article_detail;
    public String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getArticle_detail() {
        return article_detail;
    }

    public void setArticle_detail(String article_detail) {
        this.article_detail = article_detail;
    }

    public String getArticle_name() {
        return article_name;
    }

    public void setArticle_name(String article_name) {
        this.article_name = article_name;
    }

    public Article() {}

    public Article(String article_name,String article_img_url,String article_detail,String timestamp) {
        this.article_detail = article_detail;
        this.article_img_url = article_img_url;
        this.article_name = article_name;
        this.timestamp = timestamp;
    }



}

