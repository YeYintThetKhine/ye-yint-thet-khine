package com.qslogics.slaj.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Appointments_Entity {
    private String address;
    private boolean checked;
    private String checkedDate;
    private String childkey;
    private String date;
    private boolean haveCough;
    private boolean haveTravelHistory;
    private boolean haveTroubleBreathing;
    private String maritalStatus;
    private String medicalNotes;
    private ArrayList<MedicalRecordImage> medicalRecords;
    private String moKey;
    private String patientKey;
    private String professionalKey;
    private String screeningDate;
    private String temperature;
    private String time;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getCheckedDate() {
        return checkedDate;
    }

    public void setCheckedDate(String checkedDate) {
        this.checkedDate = checkedDate;
    }

    public String getChildkey() {
        return childkey;
    }

    public void setChildkey(String childkey) {
        this.childkey = childkey;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isHaveCough() {
        return haveCough;
    }

    public void setHaveCough(boolean haveCough) {
        this.haveCough = haveCough;
    }

    public boolean isHaveTravelHistory() {
        return haveTravelHistory;
    }

    public void setHaveTravelHistory(boolean haveTravelHistory) {
        this.haveTravelHistory = haveTravelHistory;
    }

    public boolean isHaveTroubleBreathing() {
        return haveTroubleBreathing;
    }

    public void setHaveTroubleBreathing(boolean haveTroubleBreathing) {
        this.haveTroubleBreathing = haveTroubleBreathing;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMedicalNotes() {
        return medicalNotes;
    }

    public void setMedicalNotes(String medicalNotes) {
        this.medicalNotes = medicalNotes;
    }

    public ArrayList<MedicalRecordImage> getMedicalRecords() {
        return medicalRecords;
    }

    public void setMedicalRecords(ArrayList<MedicalRecordImage> medicalRecords) {
        this.medicalRecords = medicalRecords;
    }

    public String getMoKey() {
        return moKey;
    }

    public void setMoKey(String moKey) {
        this.moKey = moKey;
    }


    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getProfessionalKey() {
        return professionalKey;
    }

    public void setProfessionalKey(String professionalKey) {
        this.professionalKey = professionalKey;
    }

    public String getScreeningDate() {
        return screeningDate;
    }

    public void setScreeningDate(String screeningDate) {
        this.screeningDate = screeningDate;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Appointments_Entity() {

    }


    public Appointments_Entity(String address,boolean checked,String checkedDate,String childkey,String date,boolean haveCough,
                               boolean haveTravelHistory,boolean haveTroubleBreathing,String maritalStatus,String medicalNotes,ArrayList<MedicalRecordImage> medicalRecords,
                                String moKey,String patientKey,String professionalKey,String screeningDate,String temperature,String time) {
        this.address=address;
        this.checked=checked;
        this.checkedDate=checkedDate;
        this.childkey=childkey;
        this.date=date;
        this.haveCough=haveCough;
        this.haveTravelHistory=haveTravelHistory;
        this.haveTroubleBreathing=haveTroubleBreathing;
        this.maritalStatus=maritalStatus;
        this.medicalNotes=medicalNotes;
        this.medicalRecords=medicalRecords;
        this.moKey=moKey;
        this.patientKey=patientKey;
        this.professionalKey=professionalKey;
        this.screeningDate=screeningDate;
        this.temperature=temperature;
        this.time=time;
    }
}
