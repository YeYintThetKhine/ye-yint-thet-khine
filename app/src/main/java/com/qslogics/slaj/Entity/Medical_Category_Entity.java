package com.qslogics.slaj.Entity;

public class Medical_Category_Entity {
    String burShort;
    String burmeseVer;
    String category_image_url;
    String engShort;
    String englishVer;
    String timestamp;

    public String getBurShort() {
        return burShort;
    }

    public void setBurShort(String burShort) {
        this.burShort = burShort;
    }

    public String getBurmeseVer() {
        return burmeseVer;
    }

    public void setBurmeseVer(String burmeseVer) {
        this.burmeseVer = burmeseVer;
    }

    public String getCategory_image_url() {
        return category_image_url;
    }

    public void setCategory_image_url(String category_image_url) {
        this.category_image_url = category_image_url;
    }

    public String getEngShort() {
        return engShort;
    }

    public void setEngShort(String engShort) {
        this.engShort = engShort;
    }

    public String getEnglishVer() {
        return englishVer;
    }

    public void setEnglishVer(String englishVer) {
        this.englishVer = englishVer;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Medical_Category_Entity() {

    }

    public Medical_Category_Entity(    String burShort,String burmeseVer,String category_image_url,String engShort,String englishVer,String timestamp) {
        this.burShort = burShort;
        this.burmeseVer = burmeseVer;
        this.category_image_url = category_image_url;
        this.engShort = engShort;
        this.englishVer = englishVer;
        this.timestamp = timestamp;
    }
}
