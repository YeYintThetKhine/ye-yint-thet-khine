package com.qslogics.slaj.Medicals;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class Medical_Activity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<Professional> list=new ArrayList<Professional>();

    private DatabaseReference mDatabase;

    Query qurey;

    String doctor_category_key;
    ImageView imgBackArrow;

    Intent intent = getIntent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_);
        getSupportActionBar().setDisplayOptions( ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Log.d("specialist",getIntent().getStringExtra("child_key"));

        doctor_category_key = getIntent().getStringExtra("child_key");

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Professionals");

        qurey = mDatabase.orderByChild("specializations").equalTo(doctor_category_key);

        SearchView searchValue = findViewById(R.id.search_doctors_activity_component);

        qurey.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Professional medicals_Entity = postSnapshot.getValue(Professional.class);
                    list.add(medicals_Entity);
                }
                recyclerView = findViewById(R.id._medicalRecyclerView);

                final ImageAdapter imageAdapter = new ImageAdapter(Medical_Activity.this,list);
                recyclerView.setAdapter(imageAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(Medical_Activity.this));
                recyclerView.setFocusable(false);
                imageAdapter.filter("");

                searchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        String text = searchValue.getQuery().toString().toLowerCase(Locale.getDefault());
                        imageAdapter.filter(text);
                        return false;
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}