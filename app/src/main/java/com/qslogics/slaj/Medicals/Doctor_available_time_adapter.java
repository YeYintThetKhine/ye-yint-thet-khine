package com.qslogics.slaj.Medicals;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.qslogics.slaj.Profile.AvailabletimeDoctor_adapter;
import com.qslogics.slaj.Profile.Professional_Profile;
import com.qslogics.slaj.R;

import java.util.ArrayList;

public class Doctor_available_time_adapter extends RecyclerView.Adapter<Doctor_available_time_adapter.AvailableValue> {

    ArrayList<String> mainlist=new ArrayList<String>();
    Boolean edit_check;
    Context context;
    Professional_Profile professional_profile_main;

    public Doctor_available_time_adapter(Context ct, ArrayList<String> list){
        context = ct;
        mainlist = list;
    }

    public Doctor_available_time_adapter() {

    }

    @NonNull
    @Override
    public Doctor_available_time_adapter.AvailableValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_doctor_available_time_adapter,parent,false);
        return new Doctor_available_time_adapter.AvailableValue(view);
    }



    @Override
    public void onBindViewHolder(@NonNull Doctor_available_time_adapter.AvailableValue holder, final int position) {
        holder.available_time_text.setText(mainlist.get(position));
        holder.available_time_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_access_time_black_24dp, 0, 0, 0);
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class AvailableValue extends RecyclerView.ViewHolder {

        TextView available_time_text;
        ImageButton time_remove_btn;

        public AvailableValue(@NonNull View itemView) {
            super(itemView);
            available_time_text= itemView.findViewById(R.id.available_time_text);
            time_remove_btn = itemView.findViewById(R.id.time_remove_btn);
        }
    }
}