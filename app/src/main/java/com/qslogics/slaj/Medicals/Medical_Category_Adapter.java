package com.qslogics.slaj.Medicals;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Appointment.Compeleted_Appointment_Detail;
import com.qslogics.slaj.Entity.Medical_Category_Entity;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Medical_Category_Adapter extends RecyclerView.Adapter<Medical_Category_Adapter.MedicalsCategoryValue> {

    ArrayList<Medical_Category_Entity> mainlist=new ArrayList<Medical_Category_Entity>();
    ArrayList<String> keylist=new ArrayList<String>();
    Context context;
    private DatabaseReference mDatabase;
    Intent intent;
    String title_value;

    public Medical_Category_Adapter(Context ct, ArrayList<Medical_Category_Entity> list, ArrayList<String> listkey,String title_value_get){
        context = ct;
        mainlist = list;
        keylist = listkey;
        title_value = title_value_get;
    }

    @NonNull
    @Override
    public Medical_Category_Adapter.MedicalsCategoryValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_medical__category__adapter,parent,false);
        return new Medical_Category_Adapter.MedicalsCategoryValue(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicalsCategoryValue holder, int position) {
        if(title_value.equals("Please select a Doctor category")){
            holder.doctor_category.setText(mainlist.get(position).getEnglishVer());
        }
        else{
            holder.doctor_category.setText(mainlist.get(position).getBurmeseVer());
        }

        holder.doctor_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putString("medical_category_key", keylist.get(position));
//                AppCompatActivity activity = (AppCompatActivity) v.getContext();
//                Fragment myFragment = new Medicals();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, myFragment).addToBackStack(null).commit();
//                myFragment.setArguments(bundle);


                intent = new Intent(context, Medical_Activity.class);
                intent.putExtra("child_key",keylist.get(position));
                context.startActivity(intent);
            }
        });
        holder.med_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putString("medical_category_key", keylist.get(position));
//                AppCompatActivity activity = (AppCompatActivity) v.getContext();
//                Fragment myFragment = new Medicals();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, myFragment).addToBackStack(null).commit();
//                myFragment.setArguments(bundle);


                intent = new Intent(context, Medical_Activity.class);
                intent.putExtra("child_key",keylist.get(position));
                context.startActivity(intent);
            }
        });

//        mDatabase = FirebaseDatabase.getInstance().getReference().child("Categories").child(keylist.get(position));
//        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                holder.doctor_category.setText(mainlist.get(position));
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class MedicalsCategoryValue extends RecyclerView.ViewHolder {

        Button doctor_category;
        ImageView med_icon;

        public MedicalsCategoryValue(@NonNull View itemView) {
            super(itemView);
            doctor_category= itemView.findViewById(R.id.doctor_category);
            med_icon= itemView.findViewById(R.id.med_icon);
        }
    }
}