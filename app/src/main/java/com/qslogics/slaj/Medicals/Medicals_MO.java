package com.qslogics.slaj.Medicals;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class Medicals_MO extends Fragment {

    RecyclerView recyclerView;

    ArrayList<Professional> list=new ArrayList<Professional>();

    private DatabaseReference mDatabase;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Professionals");

        final View rootView = inflater.inflate(R.layout.fragment_medicals,container,false);

        final SearchView searchValue = rootView.findViewById(R.id.search_doctors);


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Professional medicals_Entity = postSnapshot.getValue(Professional.class);
                    list.add(medicals_Entity);
                }
                recyclerView = rootView.findViewById(R.id._medicalRecyclerView);

                final ImageAdapter_MO ImageAdapter_MO = new ImageAdapter_MO (getContext(),list);
                recyclerView.setAdapter(ImageAdapter_MO);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setFocusable(false);
                ImageAdapter_MO.filter("");

                searchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        String text = searchValue.getQuery().toString().toLowerCase(Locale.getDefault());
                        ImageAdapter_MO.filter(text);
                        return false;
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        list.add( new Medicals_Entity("https://www.anime-planet.com/images/anime/screenshots/darling-in-the-franxx-8969-1.jpg",
//                "Dr.Michelle Seymour","michelle@gmail.com", "+95123456789"));




        return rootView;
    }


}
