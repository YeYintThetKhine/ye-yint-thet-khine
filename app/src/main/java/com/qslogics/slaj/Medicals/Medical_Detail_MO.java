package com.qslogics.slaj.Medicals;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Medical_Detail_MO extends AppCompatActivity {

    ImageView imgBackArrow;
    private DatabaseReference mDatabase;
    String child_key;
    String pro_image_url_extra;
    String name_extra;
    String experience_extra;
    String email_extra;
    /* HashMap<String, String > pro_phone_numbers_extra; */
    String qualifications_extra;
    HashMap<String, String > available_time_extra;
    HashMap<String, String > specializations_extra;

    String specializations_main;
    String specializations_plus;

    ArrayList<String> available_list=new ArrayList<String>();

    ArrayList<String> mainlist=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical__detail_mo);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);
        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onStart() {
        super.onStart();

        if(getIntent().hasExtra("name")){
            child_key = getIntent().getStringExtra("child_key");
            mDatabase = FirebaseDatabase.getInstance().getReference().child("Professionals");
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        final ImageView pro_image_url;
                        TextView name;
                        TextView experience;
                        TextView email;
                         TextView phone1;
                         TextView phone2;
                         TextView phone3;
                        RecyclerView available_time_recycler;
                        TextView qualification;
                        TextView specialize;


                        pro_image_url = findViewById(R.id.circularImageView_professional_profile);
                        name = findViewById(R.id.doc_name);
                        experience = findViewById(R.id.text_experience);
                        email = findViewById(R.id.email_text);
                         phone1 = findViewById(R.id.phoneno_1);
                         phone2 = findViewById(R.id.phoneno_2);
                         phone3 = findViewById(R.id.phoneno_3);
                        available_time_recycler = findViewById(R.id.available_time_recycler);
                        qualification = findViewById(R.id.qualification_detail);
                        specialize = findViewById(R.id.specialize_detail_text);

                        available_time_extra = (HashMap<String, String>) dataSnapshot.child(child_key).child("available_time").getValue();
                        specializations_extra = (HashMap<String, String>) dataSnapshot.child(child_key).child("specializations").getValue();

                        for (Map.Entry<String, String> entry : specializations_extra.entrySet()) {
                            specializations_plus=entry.getValue()+",";
                            if(specializations_main == null){
                                specializations_main=specializations_plus;
                            }
                            else{
                                specializations_main=specializations_main+specializations_plus;
                            }
                        }
                        for (Map.Entry<String, String> entry : available_time_extra.entrySet()) {
                            available_list.add(entry.getValue());
                        }

                        Picasso.get().load(dataSnapshot.child(child_key).child("pro_image_url").getValue().toString()).into(pro_image_url);
                        name.setText(dataSnapshot.child(child_key).child("name").getValue().toString());
                        experience.setText(dataSnapshot.child(child_key).child("experience").getValue().toString());
                        email.setText(dataSnapshot.child(child_key).child("email").getValue().toString());

                         phone1.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_1").getValue().toString());
                        if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").exists()){
                            phone2.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_2").getValue().toString());
                        }
                        else{
                            phone2.setText("");
                        }
                        if(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").exists()){
                            phone3.setText(dataSnapshot.child(child_key).child("pro_phone_numbers").child("ph_3").getValue().toString());
                        }
                        else{
                            phone3.setText("");
                        }

                        qualification.setText(dataSnapshot.child(child_key).child("qualifications").getValue().toString());
                        specialize.setText(specializations_main);

                        final Specialize_detail_adapter specialize_detail_adapter = new Specialize_detail_adapter( Medical_Detail_MO.this,available_list,false);
                        available_time_recycler.setAdapter(specialize_detail_adapter);
                        available_time_recycler.setHasFixedSize(true);
                        available_time_recycler.setLayoutManager(new LinearLayoutManager( Medical_Detail_MO.this));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }
}
