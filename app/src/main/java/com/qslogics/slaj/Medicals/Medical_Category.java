package com.qslogics.slaj.Medicals;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Article.Image;
import com.qslogics.slaj.Entity.Medical_Category_Entity;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Locale;

public class Medical_Category extends Fragment {

    ArrayList<String> doc_category_key=new ArrayList<String>();

    ArrayList<Medical_Category_Entity> doc_category_info=new ArrayList<Medical_Category_Entity>();

    private DatabaseReference mDatabase;

    RecyclerView recyclerView;

    TextView supportRoleTitle;

    public Medical_Category() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Categories");

        final View rootView = inflater.inflate(R.layout.fragment_medical__category,container,false);

        supportRoleTitle = rootView.findViewById(R.id.supportRoleTitle);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                doc_category_key.clear();
                doc_category_info.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Medical_Category_Entity medical_category_data = postSnapshot.getValue(Medical_Category_Entity.class);
                    doc_category_info.add(medical_category_data);
                    doc_category_key.add(postSnapshot.getKey());
                }
                recyclerView = rootView.findViewById(R.id.doc_category_recyclerview);

                final Medical_Category_Adapter medicalCategoryAdapter = new Medical_Category_Adapter(getContext(),doc_category_info,doc_category_key,supportRoleTitle.getText().toString());
                recyclerView.setAdapter(medicalCategoryAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
                recyclerView.setFocusable(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }
}
