package com.qslogics.slaj.Medicals;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qslogics.slaj.Appointment.Pending_Appointment_Detail;
import com.qslogics.slaj.Profile.Professional_Profile;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class Specialize_detail_adapter extends RecyclerView.Adapter<Specialize_detail_adapter.SpecializeValue> {


    ArrayList<String> mainlist=new ArrayList<String>();
    Boolean edit_check;
    Context context;

    public Specialize_detail_adapter(Context ct, ArrayList<String> list,Boolean edit_check_received){
        context = ct;
        mainlist = list;
        edit_check = edit_check_received;
    }

    public Specialize_detail_adapter() {

    }

    @NonNull
    @Override
    public SpecializeValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_specialize_detail_adapter,parent,false);
        return new SpecializeValue(view);
    }



    @Override
    public void onBindViewHolder(@NonNull SpecializeValue holder, final int position) {
        holder.available_time_text.setText(mainlist.get(position));
        holder.available_time_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_access_time_black_24dp, 0, 0, 0);
    }

    public static interface AdapterCallback {
        void onMethodCallback_two();
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class SpecializeValue extends RecyclerView.ViewHolder {

        TextView available_time_text;
        ImageButton close_btn;

        public SpecializeValue(@NonNull View itemView) {
            super(itemView);
            available_time_text= itemView.findViewById(R.id.available_time_text);
            close_btn = itemView.findViewById(R.id.close_btn);
            if(edit_check == false){
                close_btn.setVisibility(View.GONE);
            }
            else{
                close_btn.setVisibility(View.VISIBLE);
            }
        }
    }
}
