/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.qslogics.slaj.VideoCall;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.twilio.video.quickstart";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String TWILIO_ACCESS_TOKEN = "TWILIO_ACCESS_TOKEN";
  public static final String TWILIO_ACCESS_TOKEN_SERVER = "http://f78171c77335.ngrok.io";
  public static final boolean USE_TOKEN_SERVER = true;
}
