package com.qslogics.slaj.Article;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArticleFragment extends Fragment {

    RecyclerView recyclerView;
    List<Image> articleList;
    ArticleAdapter articleAdapter;
  //  @Override
  //  public void onCreate(Bundle savedInstanceState) {
  //      super.onCreate(savedInstanceState);
 //       List<Image> imageList = new ArrayList<>();
//        imageList.add(new Image(R.drawable.img1,
//                "Image 1",
//                "Once a Demon Slayer has fully settled into the organization, they are classified as a member of the lowest rank: Mizunoto. They must complete a variety of missions to ultimately climb the ranks and reach the highest Demon Slayer position: Kinoe. Within the Demon Slayer Corps, there is a group of elites, known as The Nine Hashira. This group consists of the nine most powerful combatants in the organization.   "
//        ));
//        imageList.add(new Image(R.drawable.img2,
//                "Image 2",
//                "Once a Demon Slayer has fully settled into the organization, they are classified as a member of the lowest rank: Mizunoto. They must complete a variety of missions to ultimately climb the ranks and reach the highest Demon Slayer position: Kinoe. Within the Demon Slayer Corps, there is a group of elites, known as The Nine Hashira. This group consists of the nine most powerful combatants in the organization. "
//        ));
//        imageList.add(new Image(R.drawable.img3, "Image 3", "Once a Demon Slayer has fully settled into the organization, they are classified as a member of the lowest rank: Mizunoto. They must complete a variety of missions to ultimately climb the ranks and reach the highest Demon Slayer position: Kinoe. Within the Demon Slayer Corps, there is a group of elites, known as The Nine Hashira. This group consists of the nine most powerful combatants in the organization. "));
//        imageList.add(new Image(R.drawable.img4, "Image 4", "Once a Demon Slayer has fully settled into the organization, they are classified as a member of the lowest rank: Mizunoto. They must complete a variety of missions to ultimately climb the ranks and reach the highest Demon Slayer position: Kinoe. Within the Demon Slayer Corps, there is a group of elites, known as The Nine Hashira. This group consists of the nine most powerful combatants in the organization. "));
//        imageList.add(new Image(R.drawable.img5, "Image 5", "Once a Demon Slayer has fully settled into the organization, they are classified as a member of the lowest rank: Mizunoto. They must complete a variety of missions to ultimately climb the ranks and reach the highest Demon Slayer position: Kinoe. Within the Demon Slayer Corps, there is a group of elites, known as The Nine Hashira. This group consists of the nine most powerful combatants in the organization. "));
    //}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        recyclerView = view.findViewById(R.id._imageRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setFocusable(false);
        articleList=new ArrayList<>();
        final DatabaseReference nm= FirebaseDatabase.getInstance().getReference("Articles");
        nm.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot npsnapshot : dataSnapshot.getChildren()){
                        Image l=npsnapshot.getValue(Image.class);
                        articleList.add(l);

                    }

                    Collections.reverse(articleList);

                     articleAdapter = new ArticleAdapter(getContext(),articleList);

                    recyclerView.setAdapter(articleAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));

//                    articleAdapter=new ArticleAdapter(getContext().getApplicationContext(),articleList);
//                    recyclerView.setAdapter(articleAdapter);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        // Inflate the layout for this fragment
        return view;
    }


    public void detailData(Image image) {
        Intent intent =  new Intent(getContext(), Detail_Article.class);
       intent.putExtra("OBJECT_INTENT", image);
        startActivity(intent);
   }




}
