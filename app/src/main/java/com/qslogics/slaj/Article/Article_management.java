package com.qslogics.slaj.Article;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.qslogics.slaj.Entity.Article;
import com.qslogics.slaj.R;

import java.util.Date;

public class Article_management extends AppCompatActivity {

    TextView article_name;
    TextView article_detail;
    DatabaseReference Database;
    String Article_name;
    String Article_detail;
    String timestamp;
    Article article;
    boolean overwritten;
    boolean completed;

    ImageView img;
    public Uri imguri;
    StorageReference mStorageRef;
    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    Uri downloadUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_management);

        mStorageRef = FirebaseStorage.getInstance().getReference("Images");
        img = (ImageView)findViewById(R.id.article_image);

        article_name = findViewById(R.id.article_nameText);
        article_detail = findViewById(R.id.article_detailText);

        Database = FirebaseDatabase.getInstance().getReference();

        findViewById(R.id.creat_art_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overwritten = false;
                Date date= new Date();
                long time = date.getTime();
                timestamp = String.valueOf(time);
                Database= FirebaseDatabase.getInstance().getReference().child("Articles").child(article_name.getText().toString().trim());
                Database.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists() && overwritten == false){
                            new AlertDialog.Builder(Article_management.this)
                                    .setTitle("Article with same name already existed!")
                                    .setMessage(article_name.getText().toString().trim() + " is already saved in the articles list. Do you want to overwrite the old article with the new one?")
                                    .setCancelable(false)
                                    .setNegativeButton("Don't", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .setPositiveButton("Overwrite", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            overwritten = true;
                                            Date date= new Date();
                                            long time = date.getTime();
                                            timestamp = String.valueOf(time);

                                            if(uploadTask != null && uploadTask.isInProgress()){
                                                Toast.makeText(Article_management.this,"Upload in progress",Toast.LENGTH_LONG).show();
                                            }
                                            else{
                                                final StorageReference Ref = mStorageRef.child(timestamp+"."+getExtension(imguri));
                                                uploadTask = Ref.putFile(imguri);
                                                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                                    @Override
                                                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                        if (!task.isSuccessful()) {
                                                            throw task.getException();
                                                        }

                                                        // Continue with the task to get the download URL
                                                        return Ref.getDownloadUrl();
                                                    }
                                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Uri> task) {
                                                        if (task.isSuccessful()) {
                                                            downloadUri = task.getResult();

                                                            Toast.makeText(Article_management.this,"Image Uploaded successfully",Toast.LENGTH_LONG).show();

                                                            if(completed == false){
                                                                completed = true;
                                                                Article_name=article_name.getText().toString().trim();
                                                                Article_detail=article_detail.getText().toString();

                                                                article = new Article(Article_name,downloadUri.toString(),Article_detail, timestamp);

                                                                FirebaseDatabase.getInstance().getReference().child("Articles").child(Article_name).setValue(article);
                                                            }
                                                            else{

                                                            }
                                                        } else {
                                                            // Handle failures
                                                            // ...
                                                        }
                                                    }
                                                });


                                            }
                                        }
                                    }).show();
                            Log.d("no data","There is no data");
                        }
                        else{
                            overwritten = true;

                            if(uploadTask != null && uploadTask.isInProgress()){
                                Toast.makeText(Article_management.this,"Upload in progress",Toast.LENGTH_LONG).show();
                            }
                            else{
                                final StorageReference Ref = mStorageRef.child(timestamp+"."+getExtension(imguri));
                                uploadTask = Ref.putFile(imguri);
                                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                    @Override
                                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                        if (!task.isSuccessful()) {
                                            throw task.getException();
                                        }

                                        // Continue with the task to get the download URL
                                        return Ref.getDownloadUrl();
                                    }
                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {

                                            downloadUri = task.getResult();
                                            Toast.makeText(Article_management.this,"Image Uploaded successfully",Toast.LENGTH_LONG).show();

                                            if(completed == false){
                                                completed = true;
                                                Article_name=article_name.getText().toString().trim();
                                                Article_detail=article_detail.getText().toString();

                                                article = new Article(Article_name,downloadUri.toString(),Article_detail, timestamp);

                                                FirebaseDatabase.getInstance().getReference().child("Articles").child(Article_name).setValue(article);
                                            }
                                            else{

                                            }

                                        } else {
                                            // Handle failures
                                            // ...
                                        }
                                    }
                                });

                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("databaseError",databaseError.getMessage());
                    }

                });
            }
        });

        findViewById(R.id.article_listbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Article_management.this, ArticleFragment.class);
                startActivity(intent);
            }
        });
    }

    public void imagePicker(View view) {
        Intent intent = new Intent();
        intent.setType("image/'");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK && data!=null && data.getData()!=null){
            imguri=data.getData();
            img.setImageURI(imguri);
        }
    }

    private String getExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

}
