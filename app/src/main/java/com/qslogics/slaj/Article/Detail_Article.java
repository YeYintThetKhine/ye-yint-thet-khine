package com.qslogics.slaj.Article;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

public class Detail_Article extends AppCompatActivity {

    String imgSrc_extra;
    String imgTitle_extra;
    String imgDesc_extra;
    ImageView imgBackArrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail__article);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);

        imgBackArrow = getSupportActionBar().getCustomView().findViewById(R.id.backbutton_toolbar);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }

    protected void onStart() {
        super.onStart();

        if(getIntent().hasExtra("imgSrc") || getIntent().hasExtra("imgTitle") || getIntent().hasExtra("imgDesc")){
            imgSrc_extra = getIntent().getStringExtra("imgSrc");
            imgTitle_extra = getIntent().getStringExtra("imgTitle");
            imgDesc_extra = getIntent().getStringExtra("imgDesc");
            Log.d("imgSrc",imgSrc_extra);
            Log.d("imgTitle",imgTitle_extra);
            Log.d("imgDesc",imgDesc_extra);

            ImageView imgSrc;
            TextView imgTitle;
            TextView imgDesc;

            imgSrc = findViewById(R.id._articleimg);
            imgTitle = findViewById(R.id._imageTitle);
            imgDesc = findViewById(R.id._imageDesc);

            Picasso.get().load(imgSrc_extra).into(imgSrc);
            imgTitle.setText(imgTitle_extra);
            imgDesc.setText(imgDesc_extra);
        }
    }

}
