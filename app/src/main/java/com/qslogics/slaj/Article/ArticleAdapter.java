package com.qslogics.slaj.Article;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {

    List<Image> articleFragmentListData;
    ImageView imageView;
    Context context;

    public ArticleAdapter(Context ct,List<Image> articleFragmentList) {
        context = ct;
        this.articleFragmentListData = articleFragmentList;
    }

    @NonNull
    @Override
    public ArticleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_image_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleAdapter.ViewHolder holder, final int position) {
        holder.title.setText(articleFragmentListData.get(position).getArticle_name());
        Picasso.get().load(articleFragmentListData.get(position).getArticle_img_url()).fit().centerCrop().into(holder.image);

        holder.article_card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Detail_Article.class);
                intent.putExtra("imgSrc",articleFragmentListData.get(position).getArticle_img_url());
                intent.putExtra("imgTitle",articleFragmentListData.get(position).getArticle_name());
                intent.putExtra("imgDesc",articleFragmentListData.get(position).getArticle_detail());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return articleFragmentListData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView title;
        RelativeLayout article_card;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id._image);
            title = itemView.findViewById(R.id._title);
            article_card = itemView.findViewById(R.id._article_card);
        }
    }


}
