package com.qslogics.slaj.Article

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Image(
        val article_name : String = "",
        val article_img_url : String = "",
        val article_detail : String = "",
        var timestamp: String = ""
) : Parcelable