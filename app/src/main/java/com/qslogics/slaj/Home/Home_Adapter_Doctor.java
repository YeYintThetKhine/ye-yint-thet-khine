package com.qslogics.slaj.Home;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Entity.Medical_Category_Entity;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.Medicals.Medical_Activity;
import com.qslogics.slaj.Medicals.Medical_Detail;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home_Adapter_Doctor extends RecyclerView.Adapter<Home_Adapter_Doctor.MedicalsValue> {

    ArrayList<Medical_Category_Entity> mainlist=new ArrayList<Medical_Category_Entity>();
    ArrayList<String> mainlist_key = new ArrayList<String>();
    Context context;
    String text_check;

    public Home_Adapter_Doctor(Context ct, ArrayList<Medical_Category_Entity> list, ArrayList<String> key_mainlist,String get_text_check){
        context = ct;
        mainlist = list;
        mainlist_key = key_mainlist;
        text_check = get_text_check;

    }
    @NonNull
    @Override
    public Home_Adapter_Doctor.MedicalsValue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_home__adapter__doctor,parent,false);
        return new Home_Adapter_Doctor.MedicalsValue(view);
    }
    public Home_Adapter_Doctor() {

    }

    @Override
    public void onBindViewHolder(@NonNull Home_Adapter_Doctor.MedicalsValue holder, final int position) {
        Picasso.get().load(mainlist.get(position).getCategory_image_url()).into(holder.img_url);
        if(text_check.equals("Available Professionals")){
            holder.specialize_name.setText(mainlist.get(position).getEngShort());
        }
        else{
            holder.specialize_name.setText(mainlist.get(position).getBurShort());
        }

        holder.img_url.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Medical_Activity.class);
                intent.putExtra("child_key",mainlist_key.get(position));
                context.startActivity(intent);
            }
        });
        holder.specialize_name.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Medical_Activity.class);
                intent.putExtra("child_key",mainlist_key.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }


    public class MedicalsValue extends RecyclerView.ViewHolder {

        ImageView img_url;
        TextView specialize_name;

        public MedicalsValue(@NonNull View itemView) {
            super(itemView);
            img_url = itemView.findViewById(R.id.doctor_img_circle);
            specialize_name = itemView.findViewById(R.id.specialize_name);
        }
    }
}

