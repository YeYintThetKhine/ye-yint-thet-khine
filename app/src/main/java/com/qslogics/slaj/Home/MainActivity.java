package com.qslogics.slaj.Home;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.qslogics.slaj.Appointment.MO_Appointments;
import com.qslogics.slaj.Appointment.Patient_Appointments;
import com.qslogics.slaj.Appointment.Patient_Pending_Appointment_Adapter;
import com.qslogics.slaj.Appointment_Notification.ReminderBroadcast;
import com.qslogics.slaj.Article.ArticleFragment;
import com.qslogics.slaj.Caregivers.SelectCaregiverSupportRole;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.Login.Login_main;
import com.qslogics.slaj.MedicalScreening.MedicalScreening;
import com.qslogics.slaj.Medicals.Medical_Category;
import com.qslogics.slaj.Medicals.Medicals;
import com.qslogics.slaj.Medicals.Medicals_MO;
import com.qslogics.slaj.Patient.Patient_List;
import com.qslogics.slaj.Prescriptions.Patient_Prescriptions;
import com.qslogics.slaj.Prescriptions.Professional_Prescriptions;
import com.qslogics.slaj.Profile.Caregiver_Profile;
import com.qslogics.slaj.Profile.MO_Profile;
import com.qslogics.slaj.Profile.Patient_Profile;
import com.qslogics.slaj.Profile.Professional_Profile;
import com.qslogics.slaj.R;
import com.qslogics.slaj.Settings.Help;
import com.qslogics.slaj.Settings.Settings;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    TextView nav_name;
    TextView nav_type;
    ImageView nav_user_image;
    String child_key;
    DatabaseReference rootRef;
    boolean hasAppointment;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabase_noti_timer;
    private FirebaseAuth firebaseAuth;
    long final_final_time_diff;
    long current_time_parse_time;
    long appointment_time_parse_time;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ///super.onCreate(savedInstanceState);///
        ///setContentView(R.layout.activity_main);///
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();

        if (config.smallestScreenWidthDp >= 600)
        {
            setContentView(R.layout.activity_main_tablet);
        }
        else
        {
            setContentView(R.layout.activity_main);
        }
        final SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        LoadLanguage();
/*
        // Default Language Function
        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        // Create a new configuration object
        Configuration configs = new Configuration();
        // Set the locale of the new configuration
        configs.locale = locale;
        // Update the configuration of the Accplication context
        getResources().updateConfiguration(
                configs,
                getResources().getDisplayMetrics()
        ); */

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
//        getUserAppointmentStatus();
        //Bundle extras = getIntent().getExtras();
        Intent i = getIntent();
        if(i != null) {
            boolean fromMedicalScreening = i.getBooleanExtra("fromMedicalScreening", false);
            boolean fromCompleteScreening = i.getBooleanExtra("fromCompleteScreening", false);
            System.out.println(fromCompleteScreening);
            System.out.println("wefjkefbwkejfbkejfbwekjfbewkjfbwkejfbkjewfbjkwebfejwbfek");

            if(fromMedicalScreening) {
                System.out.println("fromMedicalScreening");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Appointments()).commit();
            }
            if(fromCompleteScreening) {
                System.out.println("fromCompleteScreening");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Appointments()).commit();
            }
        }
        /*if (extras != null) {
            boolean fromMedicalScreening = extras.getBoolean("fromMedicalScreening", false);
            boolean fromCompleteScreening = extras.getBoolean("fromCompleteScreening", false);
            System.out.println("Patient appointment is here");
            if (fromMedicalScreening || fromCompleteScreening) {

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Appointments()).commit();
            }
        }*/

        //set home as default nav item depending on user type
        if(savedInstanceState == null) {
            switch(settings.getString("type", "User_type")) {
                case "Patient":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Home()).commit();
                    navigationView.setCheckedItem(R.id.nav_home);
                    break;
                case "Professional":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Professional_Home()).commit();
                    navigationView.setCheckedItem(R.id.nav_professional_home);
                    break;
                case "Caregiver":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Caregiver_Home()).commit();
                    navigationView.setCheckedItem(R.id.nav_caregiver_home);
                    break;
                case "MO":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MO_Home()).commit();
                    navigationView.setCheckedItem(R.id.nav_mo_home);
                    break;
            }
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Home()).commit();
            //navigationView.setCheckedItem(R.id.nav_home);
        }

        setSupportActionBar(toolbar);
        //drawer toggle
        navigationView.bringToFront();
        switch(settings.getString("type", "User_type")) {
            case "Patient":
                navigationView.inflateMenu(R.menu.main_menu);
                break;
            case "Professional":
                navigationView.inflateMenu(R.menu.main_menu_professional);
                break;
            case "Caregiver":
                navigationView.inflateMenu(R.menu.main_menu_caregiver);
                break;
            case "MO":
                navigationView.inflateMenu(R.menu.main_menu_mo);
                break;
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_drawer_open, R.string.nav_drawer_close);
        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        nav_name = headerView.findViewById(R.id.nav_menu_name);
        if(settings != null) {
            /// Log.d("pname",getString(R.string.patient));
            Log.d("TAGTAG", settings.getString("type", "User_type"));
            nav_type = headerView.findViewById(R.id.nav_menu_type);
            nav_user_image = headerView.findViewById(R.id.nav_user_image);
            if(settings.getString("type", "User_type").equals("MO")){
                nav_type.setText(getString(R.string.mo));
            }
            else if(settings.getString("type", "User_type").equals("Patient")){
                nav_type.setText(getString(R.string.patient_singular));
            }
            else if(settings.getString("type", "User_type").equals("Professional")){
                nav_type.setText(getString(R.string.professional));
            }
            else if(settings.getString("type", "User_type").equals("Caregiver")){
                nav_type.setText(getString(R.string.caregivers));
            }
        }


        if(settings != null) {
            String img_url;
            Log.d("TAGTAG", settings.getString("type", "User_type"));
            if(settings.getString("type", "User_type").equals("MO")){
                mDatabase = FirebaseDatabase.getInstance().getReference().child("MO");
            }
            if(settings.getString("type", "User_type").equals("Patient")){
                mDatabase = FirebaseDatabase.getInstance().getReference().child("Patients");
            }
            if(settings.getString("type", "User_type").equals("Professional")){
                mDatabase = FirebaseDatabase.getInstance().getReference().child("Professionals");
            }
            if(settings.getString("type", "User_type").equals("Caregiver")){
                mDatabase = FirebaseDatabase.getInstance().getReference().child("Caregivers");
            }

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("name", (String) dataSnapshot.child(settings.getString("key", "child_key")).child("name").getValue());

                        if(settings.getString("type", "User_type").equals("MO")){
                            editor.putString("profile_img_url", dataSnapshot.child(settings.getString("key", "child_key")).child("mo_image_url").getValue().toString());
                        }
                        if(settings.getString("type", "User_type").equals("Patient")){
                            editor.putString("profile_img_url", dataSnapshot.child(settings.getString("key", "child_key")).child("patient_image_url").getValue().toString());
                        }
                        if(settings.getString("type", "User_type").equals("Professional")){
                            editor.putString("profile_img_url", dataSnapshot.child(settings.getString("key", "child_key")).child("pro_image_url").getValue().toString());
                        }
                        if(settings.getString("type", "User_type").equals("Caregiver")){
                            editor.putString("profile_img_url", dataSnapshot.child(settings.getString("key", "child_key")).child("caregiver_image_url").getValue().toString());
                        }
                        editor.commit();
//                        img_url = dataSnapshot.child(child_key).child("mo_image_url").getValue().toString();
//
//                        Picasso.get().load(dataSnapshot.child(child_key).child("mo_image_url").getValue().toString()).into(mo_image_url);

                        nav_name.setText(settings.getString("name", "Username"));
                        /// nav_type.setText(settings.getString("type", "User_type"));
                        if(settings.getString("type", "User_type").equals("MO")){
                            nav_type.setText(getString(R.string.mo));
                        }
                        else if(settings.getString("type", "User_type").equals("Patient")){
                            nav_type.setText(getString(R.string.patient_singular));
                        }
                        else if(settings.getString("type", "User_type").equals("Professional")){
                            nav_type.setText(getString(R.string.professional));
                        }
                        else if(settings.getString("type", "User_type").equals("Caregiver")){
                            nav_type.setText(getString(R.string.caregivers));
                        }

                        Picasso.get().load(settings.getString("profile_img_url", "User_profile_img_url")).into(nav_user_image);
                        System.out.println(settings.getString("type", "User_type"));
                    }

                    if(settings.getString("type", "User_type").equals("Patient") || settings.getString("type", "User_type").equals("Professional")){
//                        RequestQueue mRequestQue;
//                        String URL = "https://fcm.googleapis.com/fcm/send";
//
//                        mRequestQue = Volley.newRequestQueue(MainActivity.this);

                        String firebase_topic =settings.getString("key", "child_key").replace("+","");
                        FirebaseMessaging.getInstance().subscribeToTopic(firebase_topic);

                        System.out.println(firebase_topic);
                        System.out.println("topic_assigned");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            if(settings.getString("type", "User_type").equals("Patient")){
                createNotificationChannel();

                mDatabase_noti_timer = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");

                String key = settings.getString("key", "key");
                Query query1 = mDatabase_noti_timer.orderByChild("patientKey").equalTo(key);

                query1.addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            MedicalScreeningData medicalScreeningData = postSnapshot.getValue(MedicalScreeningData.class);
                            if(postSnapshot.child("checked").getValue().equals(false)){

                                Intent intent = new Intent(MainActivity.this, ReminderBroadcast.class);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this,0,intent,0);
                                try {
                                    DateFormat df = new SimpleDateFormat("hh:mm:ss_yyyy.MM.dd");
                                    DateFormat df_two = new SimpleDateFormat("MM/dd/yy");

                                    String now = df.format(new Date());
                                    Date today_date = new java.util.Date();
//                            Log.d("hi1",today_date.toString());
                                    String today_date_string = df.format(today_date);
                                    Date today_parse = df.parse(today_date_string);
                                    Log.d("today_date_string",today_date_string);
                                    Log.d("today_parse",today_parse.toString());
                                    String appointment_date_string = postSnapshot.child("appointmentDate").getValue().toString();
                                    System.out.println(appointment_date_string);
                                    Date appointment_date_parse = df_two.parse(appointment_date_string);
                                    String appointment_date_parse_string = df.format(appointment_date_parse);
                                    if(now.equals(appointment_date_parse_string)){
                                        Date appointment_date_parse_new = df.parse(appointment_date_parse_string);
                                        long today_parse_time = today_parse.getTime();
                                        long appointment_date_parse_new_time = appointment_date_parse_new.getTime();
                                        long time_diff = appointment_date_parse_new_time - today_parse_time;
                                        System.out.println(appointment_date_parse_new);
                                        System.out.println(today_parse_time);
                                        System.out.println(appointment_date_parse_new_time);
                                        Log.d("appointment_date_parse",String.valueOf(time_diff));

                                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                        String current_time = sdf.format(new Date());
                                        Date current_time_parse = sdf.parse(current_time);
                                        String appointment_time = postSnapshot.child("appointmentTime").getValue().toString();
                                        Date appointment_time_parse = sdf.parse(appointment_time);
                                        current_time_parse_time = current_time_parse.getTime();
                                        appointment_time_parse_time = appointment_time_parse.getTime();
                                        long final_time_diff = appointment_time_parse_time - current_time_parse_time;

                                        final_final_time_diff = time_diff + final_time_diff;
                                    }
                                    else{
                                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                        String current_time = sdf.format(new Date());
                                        Date current_time_parse = sdf.parse(current_time);
                                        String appointment_time = postSnapshot.child("appointmentTime").getValue().toString();
                                        Date appointment_time_parse = sdf.parse(appointment_time);
                                        current_time_parse_time = current_time_parse.getTime();
                                        appointment_time_parse_time = appointment_time_parse.getTime();
                                        final_final_time_diff = appointment_time_parse_time - current_time_parse_time;
                                        Log.d("final_time_diff",String.valueOf(final_final_time_diff));
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

//                        long diff = date2.getTime() - date1.getTime();
//
//                        System.out.println(diff);

                                if(appointment_time_parse_time > current_time_parse_time){
                                    AlarmManager alarmManager =(AlarmManager) MainActivity.this.getSystemService(MainActivity.this.ALARM_SERVICE);

                                    long timeAtButtonClick = System.currentTimeMillis();

                                    alarmManager.set(AlarmManager.RTC_WAKEUP,timeAtButtonClick + final_final_time_diff,pendingIntent);

                                    Log.d("alarm_manager_time",String.valueOf(final_final_time_diff));
                                    Toast.makeText(MainActivity.this, "Reminder Set!" + String.valueOf(final_final_time_diff), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

//            nav_name.setText(settings.getString("name", "Username"));
//            nav_type.setText(settings.getString("type", "User_type"));
        }

    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "LemubitReminderChannel";
            String description = "Channel for Lemubit Reminder";
            int important = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("notifyLemubit",name,important);
            channel.setDescription(description);

            NotificationManager notificationManager = MainActivity.this.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void LoadLanguage(){
        SharedPreferences shp = getSharedPreferences(
                "pref",Context.MODE_PRIVATE);
        String language = shp.getString("USER_LANGUAGE","");
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    public void setMenuData() {
        setContentView(R.layout.menu_header);
        nav_name =  findViewById(R.id.nav_menu_name);
        nav_type = findViewById(R.id.nav_menu_type);

        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        if(settings != null) {
            nav_name.setText(settings.getString("name", "Username"));
            if(settings.getString("type", "User_type").equals("MO")){
                nav_type.setText(getString(R.string.mo));
            }
            if(settings.getString("type", "User_type").equals("Patient")){
                nav_type.setText(getString(R.string.patient_singular));
            }
            if(settings.getString("type", "User_type").equals("Professional")){
                nav_type.setText(getString(R.string.professional));
            }
            if(settings.getString("type", "User_type").equals("Caregiver")){
                nav_type.setText(getString(R.string.caregivers));
            }
        }
    }

    public void logout(View view) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), Login_main.class));
        finish();
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
        new Handler ().postDelayed( new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void navLogout() {
        //set active to false
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        rootRef = FirebaseDatabase.getInstance().getReference();
        String key = settings.getString("key", "key");
        switch(settings.getString("type", "User_type")) {
            case "Patient":
                rootRef.child("Patients").child(key).child("active").setValue(false);
                removePreferences();
                break;
            case "Professional":
                rootRef.child("Professionals").child(key).child("active").setValue(false);
                removePreferences();
                break;
            case "Caregiver":
                rootRef.child("Caregivers").child(key).child("active").setValue(false);
                removePreferences();
                break;
            case "MO":
                rootRef.child("MO").child(key).child("active").setValue(false);
                removePreferences();
                break;
        }

        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), Login_main.class));
        //to unsubscribe the firebase push notification topics after log out

//        FirebaseMessaging.getInstance().unsubscribeFromTopic("aungphonekyaw247gmailcom");

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    System.out.println("deleted_topics");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        AlarmManager alarmManager =(AlarmManager) MainActivity.this.getSystemService(MainActivity.this.ALARM_SERVICE);
        Intent intent = new Intent(MainActivity.this, ReminderBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this,0,intent,0);

        try {
            alarmManager.cancel(pendingIntent);
        } catch (Exception e) {
            Log.e("alarmManager", "AlarmManager update was not canceled. " + e.toString());
        }

        finish();
    }

    public void removePreferences() {
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.nav_home:
//                getUserAppointmentStatus();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Home()).commit();
                break;
            case R.id.nav_professional_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Professional_Home()).commit();
                break;
            case R.id.nav_professional_patients:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_List()).commit();
                break;
            case R.id.nav_caregiver_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Caregiver_Home()).commit();
                break;
            case R.id.nav_professionals:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Medical_Category()).commit();
                break;
            case R.id.nav_caregivers:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SelectCaregiverSupportRole()).commit();
                break;
            case R.id.nav_caregiver_patients:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_List()).commit();
                break;
            case R.id.nav_patient_medicalscreening:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MedicalScreening()).commit();
                break;
            case R.id.nav_patient_appointments:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Appointments()).commit();
                break;
//            case R.id.nav_patient_appointments:
//                getUserAppointmentStatus();
//                if(hasAppointment) {
//                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Appointments()).commit();
//                }
//                if (!hasAppointment) {
//                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MedicalScreening()).commit();
//                }
//                break;
            case R.id.nav_professional_appointments:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MO_Appointments()).commit();
                break;
            case R.id.nav_articles:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ArticleFragment()).commit();
                break;
            case R.id.nav_patient_prescriptions:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Prescriptions()).commit();
                break;
            case R.id.nav_professional_prescriptions:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Professional_Prescriptions()).commit();
                break;
            case R.id.nav_patient_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_Profile()).commit();
                break;
            case R.id.nav_caregiver_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Caregiver_Profile()).commit();
                break;
            case R.id.nav_professional_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Professional_Profile()).commit();
                break;
            case R.id.nav_mo_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MO_Home()).commit();
                break;
            case R.id.nav_mo_professionals:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Medicals_MO ()).commit();
                break;
            case R.id.nav_mo_caregivers:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SelectCaregiverSupportRole()).commit();
                break;
            case R.id.nav_mo_patients:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Patient_List()).commit();
                break;
            case R.id.nav_mo_appointments:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MO_Appointments()).commit();
                break;
            case R.id.nav_mo_articles:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ArticleFragment()).commit();
                break;
            case R.id.nav_mo_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MO_Profile()).commit();
                break;
            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Settings()).commit();
                break;
            case R.id.nav_help:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Help()).commit();
                break;
            case R.id.nav_logout:
                navLogout();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getUserAppointmentStatus() {
        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        String key = settings.getString("key", "key");

        rootRef = FirebaseDatabase.getInstance().getReference();

        switch(settings.getString("type", "User_type")) {
            case "Patient":
                rootRef.child("Patients").child(key).child("hasAppointment").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        hasAppointment = (boolean) dataSnapshot.getValue();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                break;
        }
    }
}