package com.qslogics.slaj.Home;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.BaseDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Article.Image;
import com.qslogics.slaj.R;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;

public class Professional_Home extends Fragment {

    RecyclerView recyclerView;
    private DatabaseReference mDatabaseLatestArticle;
    private DatabaseReference mDatabase_chartdata;
    Query mDatabaseLatestArticle_query;
    ArrayList<Image> list_latest_new=new ArrayList<Image>();

    float totalcase_value_week1 = 0;
    float totalcase_value_week2 = 0;
    float totalcase_value_week3 = 0;
    float totalcase_value_week4 = 0;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_professional_home, container, false);

        BarChart chart = view.findViewById(R.id.patient_home_chart);

//        BarData data = new BarData(getXAxisValues(), getDataSet());
//        chart.setData(data);
//        chart.setDescription("");
//        chart.animateXY(2000, 2000);
//        chart.invalidate();


        //////////////////////////////for chart data(start)//////////////////////////////////////////////////////////////////////////////
        LocalDate currentdate = LocalDate.now();
        Month currentMonth = currentdate.getMonth();
        String currentMonth_string_lowercase = currentMonth.toString().toLowerCase();
        String currentMonth_string_uppercase_firstchar =  currentMonth_string_lowercase.substring(0, 1).toUpperCase() + currentMonth_string_lowercase.substring(1);
        int currentYear = currentdate.getYear();
        String currentYear_string = String.valueOf(currentYear);


        mDatabase_chartdata = FirebaseDatabase.getInstance().getReference().child("ChartData").child(currentMonth_string_uppercase_firstchar+currentYear_string);
        mDatabase_chartdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    totalcase_value_week1 = 0;
                    totalcase_value_week2 = 0;
                    totalcase_value_week3 = 0;
                    totalcase_value_week4 = 0;
                    if(dataSnapshot.child("Week1").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week1").getChildren()) {
                            totalcase_value_week1 = totalcase_value_week1 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    if(dataSnapshot.child("Week2").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week2").getChildren()) {
                            totalcase_value_week2 = totalcase_value_week2 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    if(dataSnapshot.child("Week3").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week3").getChildren()) {
                            totalcase_value_week3 = totalcase_value_week3 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    if(dataSnapshot.child("Week4").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week4").getChildren()) {
                            totalcase_value_week4 = totalcase_value_week4 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    BarData data = new BarData(getXAxisValues(), getDataSet());
                    chart.setData(data);
                    chart.setDescription("");
                    chart.animateXY(2000, 2000);
                    chart.invalidate();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //////////////////////////////for chart data(end)//////////////////////////////////////////////////////////////////////////////

        mDatabaseLatestArticle = FirebaseDatabase.getInstance().getReference().child("Articles");
        mDatabaseLatestArticle_query = mDatabaseLatestArticle.orderByKey().limitToLast(5);

        mDatabaseLatestArticle_query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_latest_new.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if(list_latest_new.size() <= 4){
                        Image image = postSnapshot.getValue(Image.class);
                        list_latest_new.add(image);
                    }
                    else{
                        break;
                    }
                }
                Collections.reverse(list_latest_new);

                recyclerView = view.findViewById(R.id.home_recyclerView_latest_article);

                Home_Adapter_Latest_Article home_adapter_latest_Article = new Home_Adapter_Latest_Article(getContext(),list_latest_new);
                recyclerView.setAdapter(home_adapter_latest_Article);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerView.setFocusable(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

    private ArrayList getDataSet() {
        ArrayList dataSets = null;

        ArrayList valueSet2 = new ArrayList();
        BarEntry v2e1 = new BarEntry(totalcase_value_week1, 0); // Jan
        valueSet2.add(v2e1);
        BarEntry v2e2 = new BarEntry(totalcase_value_week2, 1); // Feb
        valueSet2.add(v2e2);
        BarEntry v2e3 = new BarEntry(totalcase_value_week3, 2); // Mar
        valueSet2.add(v2e3);
        BarEntry v2e4 = new BarEntry(totalcase_value_week4, 3); // Apr
        valueSet2.add(v2e4);

//        ArrayList valueSet2 = new ArrayList();
//        BarEntry v2e1 = new BarEntry(150.000f, 0); // Jan
//        valueSet2.add(v2e1);
//        BarEntry v2e2 = new BarEntry(90.000f, 1); // Feb
//        valueSet2.add(v2e2);
//        BarEntry v2e3 = new BarEntry(120.000f, 2); // Mar
//        valueSet2.add(v2e3);
//        BarEntry v2e4 = new BarEntry(60.000f, 3); // Apr
//        valueSet2.add(v2e4);
//        BarEntry v2e5 = new BarEntry(20.000f, 4); // May
//        valueSet2.add(v2e5);
//        BarEntry v2e6 = new BarEntry(80.000f, 5); // Jun
//        valueSet2.add(v2e6);


//        barDataSet1.setColor(Color.rgb(0, 255, 0));
//
//        barDataSet2.setColor(Color.rgb(255, 255, 0));

        BaseDataSet barDataSet3 = new BarDataSet(valueSet2, "Infected");
        barDataSet3.setColor(Color.rgb(255, 0, 0));

        dataSets = new ArrayList();

        dataSets.add(barDataSet3);
        return dataSets;
    }

    private ArrayList getXAxisValues() {
        ArrayList xAxis = new ArrayList();
        xAxis.add("1st Week");
        xAxis.add("2nd Week");
        xAxis.add("3rd Week");
        xAxis.add("4th Week");
//        xAxis.add("5th Week");
//        xAxis.add("6th Week");
        return xAxis;
    }
}
