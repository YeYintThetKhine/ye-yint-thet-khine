package com.qslogics.slaj.Home;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.BaseDataSet;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.qslogics.slaj.Appointment_Notification.ReminderBroadcast;
import com.qslogics.slaj.Article.Image;
import com.qslogics.slaj.Caregivers.CaregiversActivity;
import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.Entity.MedicalScreeningData;
import com.qslogics.slaj.Entity.Medical_Category_Entity;
import com.qslogics.slaj.Entity.Professional;
import com.qslogics.slaj.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class Patient_Home extends Fragment {

    RecyclerView recyclerView;

    ArrayList<Medical_Category_Entity> list=new ArrayList<Medical_Category_Entity>();

    ArrayList<Caregiver> list_other_pro=new ArrayList<Caregiver>();

    ArrayList<Image> list_latest_new=new ArrayList<Image>();

    ArrayList<String> list_key = new ArrayList<String>();

    ArrayList valueSet2 = new ArrayList();

    private DatabaseReference mDatabase;

    private DatabaseReference mDatabaseOtherPro;
    private DatabaseReference mDatabaseLatestArticle;
    private DatabaseReference mDatabase_noti_timer;
    private DatabaseReference mDatabase_chartdata;
    Query mDatabaseLatestArticle_query;
    private FirebaseAuth firebaseAuth;
    long final_final_time_diff;
    long current_time_parse_time;
    long appointment_time_parse_time;
    AlarmManager alarmManager;
    float totalcase_value_week1 = 0;
    float totalcase_value_week2 = 0;
    float totalcase_value_week3 = 0;
    float totalcase_value_week4 = 0;

    CircleImageView support1;
    CircleImageView support2;
    CircleImageView support3;
    CircleImageView support4;
    CircleImageView support5;
    CircleImageView support6;
    CircleImageView support7;
    CircleImageView support8;
    CircleImageView support9;
    TextView support_test1;
    TextView support_test2;
    TextView support_test3;
    TextView support_test4;
    TextView support_test5;
    TextView support_test6;
    TextView support_test7;
    TextView support_test8;
    TextView support_test9;
    TextView available_doctor;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Categories");

        mDatabaseOtherPro = FirebaseDatabase.getInstance().getReference().child("Caregivers");

        mDatabaseLatestArticle = FirebaseDatabase.getInstance().getReference().child("Articles");

        mDatabaseLatestArticle_query = mDatabaseLatestArticle.orderByKey().limitToLast(5);

        final View rootView = inflater.inflate(R.layout.fragment_patient_home,container,false);

        support1 = rootView.findViewById(R.id.support1_img);
        support2 = rootView.findViewById(R.id.support2_img);
        support3 = rootView.findViewById(R.id.support3_img);
        support4 = rootView.findViewById(R.id.support4_img);
        support5 = rootView.findViewById(R.id.support5_img);
        support6 = rootView.findViewById(R.id.support6_img);
        support7 = rootView.findViewById(R.id.support7_img);
        support8 = rootView.findViewById(R.id.support8_img);
        support9 = rootView.findViewById(R.id.support9_img);
        support_test1 = rootView.findViewById(R.id.support1);
        support_test2 = rootView.findViewById(R.id.support2);
        support_test3 = rootView.findViewById(R.id.support3);
        support_test4 = rootView.findViewById(R.id.support4);
        support_test5 = rootView.findViewById(R.id.support5);
        support_test6 = rootView.findViewById(R.id.support6);
        support_test7 = rootView.findViewById(R.id.support7);
        support_test8 = rootView.findViewById(R.id.support8);
        support_test9 = rootView.findViewById(R.id.support9);

        available_doctor = rootView.findViewById(R.id.text_doctor);

        BarChart chart = rootView.findViewById(R.id.patient_home_chart);

        //////////////////////////////for chart data(start)//////////////////////////////////////////////////////////////////////////////
        LocalDate currentdate = LocalDate.now();
        Month currentMonth = currentdate.getMonth();
        String currentMonth_string_lowercase = currentMonth.toString().toLowerCase();
        String currentMonth_string_uppercase_firstchar =  currentMonth_string_lowercase.substring(0, 1).toUpperCase() + currentMonth_string_lowercase.substring(1);
        int currentYear = currentdate.getYear();
        String currentYear_string = String.valueOf(currentYear);


        mDatabase_chartdata = FirebaseDatabase.getInstance().getReference().child("ChartData").child(currentMonth_string_uppercase_firstchar+currentYear_string);
        mDatabase_chartdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    totalcase_value_week1 = 0;
                    totalcase_value_week2 = 0;
                    totalcase_value_week3 = 0;
                    totalcase_value_week4 = 0;
                    if(dataSnapshot.child("Week1").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week1").getChildren()) {
                            totalcase_value_week1 = totalcase_value_week1 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    if(dataSnapshot.child("Week2").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week2").getChildren()) {
                            totalcase_value_week2 = totalcase_value_week2 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    if(dataSnapshot.child("Week3").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week3").getChildren()) {
                            totalcase_value_week3 = totalcase_value_week3 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    if(dataSnapshot.child("Week4").exists()){
                        for (DataSnapshot postSnapshot : dataSnapshot.child("Week4").getChildren()) {
                            totalcase_value_week4 = totalcase_value_week4 + (long) postSnapshot.child("confirm_case").getValue();
                        }
                    }
                    BarData data = new BarData(getXAxisValues(), getDataSet());
                    chart.setData(data);
                    chart.setDescription("");
                    chart.animateXY(2000, 2000);
                    chart.invalidate();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //////////////////////////////for chart data(end)//////////////////////////////////////////////////////////////////////////////

        //////////////////////////////for reminder notification////////////////////////////////////////////////////////////////////////
        SharedPreferences settings = getContext().getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        String key = settings.getString("key", "key");

        alarmManager =(AlarmManager) getContext().getSystemService(getContext().ALARM_SERVICE);

        createNotificationChannel();
        Intent intent = new Intent(getContext(), ReminderBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(),0,intent,0);
            mDatabase_noti_timer = FirebaseDatabase.getInstance().getReference().child("MedicalRecords_Completed");

            Query query1 = mDatabase_noti_timer.orderByChild("patientKey").equalTo(key);

            query1.addValueEventListener(new ValueEventListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        MedicalScreeningData medicalScreeningData = postSnapshot.getValue(MedicalScreeningData.class);
                        if(postSnapshot.child("checked").getValue().equals(false)){


                            try {
                                DateFormat df = new SimpleDateFormat("hh:mm:ss_yyyy.MM.dd");
                                DateFormat df_two = new SimpleDateFormat("MM/dd/yy");

                                String now = df.format(new Date());
                                Date today_date = new java.util.Date();
//                            Log.d("hi1",today_date.toString());
                                String today_date_string = df.format(today_date);
                                Date today_parse = df.parse(today_date_string);
                                Log.d("today_date_string",today_date_string);
                                Log.d("today_parse",today_parse.toString());
                                String appointment_date_string = postSnapshot.child("appointmentDate").getValue().toString();
                                System.out.println(appointment_date_string);
                                Date appointment_date_parse = df_two.parse(appointment_date_string);
                                String appointment_date_parse_string = df.format(appointment_date_parse);
                                if(now.equals(appointment_date_parse_string)){
                                    Date appointment_date_parse_new = df.parse(appointment_date_parse_string);
                                    long today_parse_time = today_parse.getTime();
                                    long appointment_date_parse_new_time = appointment_date_parse_new.getTime();
                                    long time_diff = appointment_date_parse_new_time - today_parse_time;
                                    System.out.println(appointment_date_parse_new);
                                    System.out.println(today_parse_time);
                                    System.out.println(appointment_date_parse_new_time);
                                    Log.d("appointment_date_parse",String.valueOf(time_diff));

                                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                    String current_time = sdf.format(new Date());
                                    Date current_time_parse = sdf.parse(current_time);
                                    String appointment_time = postSnapshot.child("appointmentTime").getValue().toString();
                                    Date appointment_time_parse = sdf.parse(appointment_time);
                                    current_time_parse_time = current_time_parse.getTime();
                                    appointment_time_parse_time = appointment_time_parse.getTime();
                                    long final_time_diff = appointment_time_parse_time - current_time_parse_time;

                                    final_final_time_diff = time_diff + final_time_diff;
                                }
                                else{
                                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                    String current_time = sdf.format(new Date());
                                    Date current_time_parse = sdf.parse(current_time);
                                    String appointment_time = postSnapshot.child("appointmentTime").getValue().toString();
                                    Date appointment_time_parse = sdf.parse(appointment_time);
                                    current_time_parse_time = current_time_parse.getTime();
                                    appointment_time_parse_time = appointment_time_parse.getTime();
                                    final_final_time_diff = appointment_time_parse_time - current_time_parse_time;
                                    Log.d("final_time_diff",String.valueOf(final_final_time_diff));
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

//                        long diff = date2.getTime() - date1.getTime();
//
//                        System.out.println(diff);
                            Log.d("notiiiiiiii_two","notiiiiiiii_two");
                            if(appointment_time_parse_time > current_time_parse_time){
//                                AlarmManager alarmManager =(AlarmManager) getContext().getSystemService(getContext().ALARM_SERVICE);

                                long timeAtButtonClick = System.currentTimeMillis();

                                alarmManager.set(AlarmManager.RTC_WAKEUP,timeAtButtonClick + final_final_time_diff,pendingIntent);

                                Log.d("alarm_manager_time",String.valueOf(final_final_time_diff));
//                                Toast.makeText(getContext(), "Reminder Set!" + String.valueOf(final_final_time_diff), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                list_key.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Medical_Category_Entity medical_category_Entity = postSnapshot.getValue(Medical_Category_Entity.class);
                    list.add(medical_category_Entity);
                    list_key.add(postSnapshot.getKey());
                }
                 recyclerView = rootView.findViewById(R.id.home_recyclerView_doctor);

                 Home_Adapter_Doctor home_adapter_Doctor = new Home_Adapter_Doctor(getContext(),list,list_key,available_doctor.getText().toString());
                 recyclerView.setAdapter(home_adapter_Doctor);
                 recyclerView.setHasFixedSize(true);
                 recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /*mDatabaseOtherPro.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_other_pro.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Caregiver other_pro_Entity = postSnapshot.getValue(Caregiver.class);
                    list_other_pro.add(other_pro_Entity);
                }
                recyclerView = rootView.findViewById(R.id.home_recyclerView_other);

                Home_Adapter_Other home_adapter_Other = new Home_Adapter_Other(getContext(),list_other_pro);
                recyclerView.setAdapter(home_adapter_Other);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

        mDatabaseLatestArticle_query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list_latest_new.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if(list_latest_new.size() <= 4){
                        Image image = postSnapshot.getValue(Image.class);
                        list_latest_new.add(image);
                    }
                    else{
                        break;
                    }
                }
                Collections.reverse(list_latest_new);

                recyclerView = rootView.findViewById(R.id.home_recyclerView_latest_article);

                Home_Adapter_Latest_Article home_adapter_latest_Article = new Home_Adapter_Latest_Article(getContext(),list_latest_new);
                recyclerView.setAdapter(home_adapter_latest_Article);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        support1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support1));
                startActivity(intent);
            }
        });

        support2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support2));
                startActivity(intent);
            }
        });

        support3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support3));
                startActivity(intent);
            }
        });

        support4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support4));
                startActivity(intent);
            }
        });

        support5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support5));
                startActivity(intent);
            }
        });

        support6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support6));
                startActivity(intent);
            }
        });

        support7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support7));
                startActivity(intent);
            }
        });

        support8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support8));
                startActivity(intent);
            }
        });

        support9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support9));
                startActivity(intent);
            }
        });
        support_test1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support1));
                startActivity(intent);
            }
        });

        support_test2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support2));
                startActivity(intent);
            }
        });

        support_test3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support3));
                startActivity(intent);
            }
        });

        support_test4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support4));
                startActivity(intent);
            }
        });

        support_test5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support5));
                startActivity(intent);
            }
        });

        support_test6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support6));
                startActivity(intent);
            }
        });

        support_test7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support7));
                startActivity(intent);
            }
        });

        support_test8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support8));
                startActivity(intent);
            }
        });

        support_test9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CaregiversActivity.class);
                intent.putExtra("query", getResources().getString(R.string.support9));
                startActivity(intent);
            }
        });

        return rootView;
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        if (!mIsVisibleToUser && getUserVisibleHint()) {
//            mIsVisibleToUser = true;
//            show();
//        }
//    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d("notiiiiiiii","notiiiiiiiii_check");
            CharSequence name = "LemubitReminderChannel";
            String description = "Channel for Lemubit Reminder";
            int important = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("notifyLemubit",name,important);
            channel.setDescription(description);

            NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private ArrayList getDataSet() {
        ArrayList dataSets = null;


        ArrayList valueSet2 = new ArrayList();
        BarEntry v2e1 = new BarEntry(totalcase_value_week1, 0); // Jan
        valueSet2.add(v2e1);
        BarEntry v2e2 = new BarEntry(totalcase_value_week2, 1); // Feb
        valueSet2.add(v2e2);
        BarEntry v2e3 = new BarEntry(totalcase_value_week3, 2); // Mar
        valueSet2.add(v2e3);
        BarEntry v2e4 = new BarEntry(totalcase_value_week4, 3); // Apr
        valueSet2.add(v2e4);
//        BarEntry v2e5 = new BarEntry(20.000f, 4); // May
//        valueSet2.add(v2e5);
//        BarEntry v2e6 = new BarEntry(80.000f, 5); // Jun
//        valueSet2.add(v2e6);


        BaseDataSet barDataSet3 = new BarDataSet(valueSet2, "Infected");
        barDataSet3.setColor(Color.rgb(255, 0, 0));

        dataSets = new ArrayList();

        dataSets.add(barDataSet3);
        return dataSets;
    }

    private ArrayList getXAxisValues() {
        ArrayList xAxis = new ArrayList();
        xAxis.add("1st Week");
        xAxis.add("2nd Week");
        xAxis.add("3rd Week");
        xAxis.add("4th Week");
//        xAxis.add("5th Week");
//        xAxis.add("6th Week");
        return xAxis;
    }
}
