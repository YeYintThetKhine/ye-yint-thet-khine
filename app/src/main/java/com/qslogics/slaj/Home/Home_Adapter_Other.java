package com.qslogics.slaj.Home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qslogics.slaj.Caregivers.Caregiver_Detail;
import com.qslogics.slaj.Entity.Caregiver;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home_Adapter_Other extends RecyclerView.Adapter<Home_Adapter_Other.Other_Professional_Value> {

    ArrayList<Caregiver> mainlist=new ArrayList<Caregiver>();
    Context context;

    public Home_Adapter_Other(Context ct, ArrayList<Caregiver> list){
        context = ct;
        mainlist = list;
    }

    @Override
    public Home_Adapter_Other.Other_Professional_Value onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_home__adapter__other,parent,false);
        return new Home_Adapter_Other.Other_Professional_Value(view);
    }

    public Home_Adapter_Other() {

    }

    @Override
    public void onBindViewHolder(@NonNull Home_Adapter_Other.Other_Professional_Value holder, final int position) {

        Picasso.get().load(mainlist.get(position).getCaregiver_image_url()).into(holder.img_url);
        holder.img_url.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Caregiver_Detail.class);
                intent.putExtra("child_key",mainlist.get(position).getChild_key());
                intent.putExtra("caregiver_image_url",mainlist.get(position).getCaregiver_image_url());
                intent.putExtra("name",mainlist.get(position).getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class Other_Professional_Value extends RecyclerView.ViewHolder {

        ImageView img_url;

        public Other_Professional_Value(@NonNull View itemView) {
            super(itemView);
            img_url = itemView.findViewById(R.id.other_pro_img_circle);
        }
    }
}
