package com.qslogics.slaj.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qslogics.slaj.Article.Detail_Article;
import com.qslogics.slaj.Article.Image;
import com.qslogics.slaj.Entity.Other_Pro_Entity;
import com.qslogics.slaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home_Adapter_Latest_Article extends RecyclerView.Adapter<Home_Adapter_Latest_Article.Latest_Article_Value>  {

    ArrayList<Image> mainlist=new ArrayList<Image>();
    Context context;

    public Home_Adapter_Latest_Article(Context ct, ArrayList<Image> list){
        context = ct;
        mainlist = list;
    }

    @Override
    public Home_Adapter_Latest_Article.Latest_Article_Value onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_home__adapter__latest__article,parent,false);
        return new Home_Adapter_Latest_Article.Latest_Article_Value(view);
    }

    public Home_Adapter_Latest_Article() {

    }

    @Override
    public void onBindViewHolder(@NonNull Home_Adapter_Latest_Article.Latest_Article_Value holder, final int position) {
        Log.d("checklist",String.valueOf(position));
        holder.title.setText(mainlist.get(position).getArticle_name());
        Picasso.get().load(mainlist.get(position).getArticle_img_url()).into(holder.image);

        holder.article_card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Detail_Article.class);
                intent.putExtra("imgSrc",mainlist.get(position).getArticle_img_url());
                intent.putExtra("imgTitle",mainlist.get(position).getArticle_name());
                intent.putExtra("imgDesc",mainlist.get(position).getArticle_detail());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    public class Latest_Article_Value extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title;
        RelativeLayout article_card;

        public Latest_Article_Value(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id._image);
            title = itemView.findViewById(R.id._title);
            article_card = itemView.findViewById(R.id._article_card);
        }
    }

}
